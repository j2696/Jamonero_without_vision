# WCC-CLAAS

Zephyr RTOS based application for Maximatecc.WCC

# Build requirements

Reference development machine is GNU Debian Bullseye

* ssh
* git
* python3
* pip3
* gcc-arm-none-eabi (arm cross toolchain)
* openvpn (for K-LAGAN vpn connection)
* openocd (current master + STM32G0B1 support) (package pending)
* gdb-multiarch (suggested)

```sh
sudo apt install ssh git python3-pip gcc-arm-none-eabi openvpn gdb-multiarch
```

# Getting started

## Environment setup

This section is a one time setup. This step is only needed to prepared the development system for being able to build the application.

These are the [zephyr dependencies:][https://docs.zephyrproject.org/latest/getting_started/index.html]

* cmake
* ninja-build or make
* west (installed from pip3)
* device-tree-compiler
* python3-pyelftools
* ccache (suggested)

```sh
sudo apt install cmake ninja-build device-tree-compiler ccache python3-pyelftools
```

west tool installation

* Optional: debian python dependencies

```sh
sudo apt install python3-colorama python3-docopt python3-packaging python3-pykwalify python3-pyparsing python3-dateutil python3-ruamel.yaml python3-pyelftools
```

* Pip3 for west tool

```sh
pip3 install --user -U west
echo 'export PATH=~/.local/bin:"$PATH"' >> ~/.bashrc
source ~/.bashrc
```
Bring up K-LAGAN vpn

```sh
west init -m ssh://git@10.10.2.5/maximatecc/wcc-claas.git wcc-claas
cd wcc-claas/wcc-claas.git
west update
```

## Application build

Inside the wcc-claas.git directory:

```sh
ZEPHYR_TOOLCHAIN_VARIANT=cross-compile CROSS_COMPILE="/usr/bin/arm-none-eabi-" west build -b nucleo_g0b1re app
west flash  -r openocd --config openocd.cfg
```

Note when custom openocd is used, add the following flags to the `west flash` command: `--openocd /home/raul-klg/debs/openocd/openocd/src/openocd --openocd-search /home/raul-klg/debs/openocd/openocd/tcl/ `

## Attach gdb to running application

```sh
west debug  -r openocd --config openocd.cfg
```

# Testing

## Runtime dependencies

```sh
sudo apt install python3-tabulate python3-psutil python3-ply
pip3 install --user anytree
```

## Run twister

```sh
ZEPHYR_TOOLCHAIN_VARIANT="zephyr" ZEPHYR_TOOLCHAIN_DIR="/usr/" ../zephyr/scripts/twister -a posix -p native_posix -v -T tests/ --coverage
```

