find_package(Git QUIET)
find_package(Hg QUIET)
if(NOT BUILD_VERSION AND (GIT_FOUND OR HG_FOUND))
  if(GIT_FOUND)
    execute_process(
        COMMAND ${GIT_EXECUTABLE} describe --abbrev=12
        WORKING_DIRECTORY                ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE                  BUILD_VERSION_GIT
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_STRIP_TRAILING_WHITESPACE
        ERROR_VARIABLE                   stderr_git
        RESULT_VARIABLE                  return_code_git
    )
  endif()
  if(HG_FOUND)
    execute_process(
        COMMAND ${HG_EXECUTABLE} log -r . --template "{gitnode|short}"
        WORKING_DIRECTORY                ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE                  BUILD_VERSION_HG
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_STRIP_TRAILING_WHITESPACE
        ERROR_VARIABLE                   stderr_hg
        RESULT_VARIABLE                  return_code_hg
    )
  endif()
  if(return_code_hg AND return_code_git)
    message(WARNING "Version retrieval failed: git ${stderr_git} hg ${stderr_hg};
   BUILD_VERSION is left undefined")
  elseif(CMAKE_VERBOSE_MAKEFILE)
    message(STATUS "git describe stderr: ${stderr_git}")
    message(STATUS "hg describe stderr: ${stderr_hg}")
  endif()
  if(BUILD_VERSION_GIT)
    set(BUILD_VERSION ${BUILD_VERSION_GIT})
  elseif(BUILD_VERSION_HG)
    set(BUILD_VERSION ${BUILD_VERSION_HG})
  endif()
  message(STATUS "build version in ${CMAKE_SOURCE_DIR} ${BUILD_VERSION}")
endif()
