/**
 * @file   debounce.c
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for debounce helper.
 */

/* Header of project specific types
-----------------------------------------------------------------------------*/
#include "debounce.h"
#include "assert.h"
#include "util.h"

/* Constant definitions
---------------------------------------------------------------------------*/
/** @brief Released and pressed threshold definition for ADC keyboard input sampling  */
#define KEYB_RELEASED_THRSHLD_MAX   3000U
#define KEYB_RELEASED_THRSHLD_MIN   2500U
#define KEYB_PRESSED_THRSHLD_MAX    1250U
#define KEYB_PRESSED_THRSHLD_MIN    750U
/** @brief Released and pressed threshold definition for analogic ignition input sampling  */
#define IGNA_RELEASED_THRSHLD_MAX   1250U
#define IGNA_RELEASED_THRSHLD_MIN   750U
#define IGNA_PRESSED_THRSHLD_MAX    3000U
#define IGNA_PRESSED_THRSHLD_MIN    2500U
#define MAX_DEBOUNCE_COUNT          1000U

/** @brief Debouncing threshold definition. i.e. DEB_THRSHLD_IGN, DEB_THRSHLD_KEY.
 * The debouncing threshold could be defined ranging from 1 to MAX_DEBOUNCE_COUNT-1
 * The meaning is how many consecutive active detections (or pressed detections when 
 * signals are mapped to buttons) will be needed for triggering a status change in the
 * debounce filter output (.detection in the S_debounce_st structure) 
 */
#define DEB_THRSHLD_PINKEY      4U /* threshold for keyboard module with non multiplexed adc inputs */
#define DEB_THRSHLD_MUXKEY      1U /* threshold for keyboard module with multiplexed adc inputs */
#define DEB_THRSHLD_IGN         3U /* threshold for ignition module */

/* Private macro
---------------------------------------------------------------------------*/
BUILD_ASSERT(0U < DEB_THRSHLD_PINKEY, "Invalid debounce threshold");
BUILD_ASSERT(MAX_DEBOUNCE_COUNT > DEB_THRSHLD_PINKEY, "Invalid debounce threshold");
BUILD_ASSERT(0U < DEB_THRSHLD_MUXKEY, "Invalid debounce threshold");
BUILD_ASSERT(MAX_DEBOUNCE_COUNT > DEB_THRSHLD_MUXKEY, "Invalid debounce threshold");
BUILD_ASSERT(0U < DEB_THRSHLD_IGN, "Invalid debounce threshold");
BUILD_ASSERT(MAX_DEBOUNCE_COUNT > DEB_THRSHLD_IGN, "Invalid debounce threshold");
#define S_DEBOUNCE_ST_DEF(rthr_max, rthr_min, pthr_max, pthr_min, dthr_) \
{ \
    .failure = false, \
    .detection = 0U, \
    .repetitions = 0U, \
    .debounce_threshold = dthr_, \
    .released_threshold_max = rthr_max, \
    .released_threshold_min = rthr_min, \
    .pressed_threshold_max = pthr_max, \
    .pressed_threshold_min = pthr_min, \
    .time_spent_pressed = 0U, \
    .time_stamp_pressed = 0U, \
    .button_pressed = false, \
}

/* Project specific types
---------------------------------------------------------------------------*/
typedef struct S_debounce_st_t
{
    bool failure;
    button_status detection;
    uint16_t repetitions;
    uint16_t debounce_threshold;
    uint16_t released_threshold_max;
    uint16_t released_threshold_min;
    uint16_t pressed_threshold_max;
    uint16_t pressed_threshold_min;
    int64_t time_spent_pressed;
    int64_t time_stamp_pressed;
    bool button_pressed;

} S_debounce_st;

/* Local variables
-----------------------------------------------------------------------------*/
static S_debounce_st deb_filter[E_DEBOUNCE_ID_MAX] = {

    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch1*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch2*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch3*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch4*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch5*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch6*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch7*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /*keyboard Ch8*/
    S_DEBOUNCE_ST_DEF(IGNA_RELEASED_THRSHLD_MAX, IGNA_RELEASED_THRSHLD_MIN, IGNA_PRESSED_THRSHLD_MAX, IGNA_PRESSED_THRSHLD_MIN, DEB_THRSHLD_IGN),  /*ignition analogic*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /* diag wash 1*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /* diag wash 2*/
    S_DEBOUNCE_ST_DEF(KEYB_RELEASED_THRSHLD_MAX, KEYB_RELEASED_THRSHLD_MIN, KEYB_PRESSED_THRSHLD_MAX, KEYB_PRESSED_THRSHLD_MIN, DEB_THRSHLD_MUXKEY), /* diag wash 3*/
};

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Step for helper debounce
 */
void debounce_refresh(E_debounce_channel deb_ch, RETStatus ret_st, int value)
{
    S_ASSERT(deb_ch < E_DEBOUNCE_ID_MAX, ASSERT_BOUNDS, "Invalid debounce channel");
    if (ret_st == STATUS_OK)
    {
        deb_filter[deb_ch].failure = false;

        if ((value < deb_filter[deb_ch].released_threshold_max) && (value > deb_filter[deb_ch].released_threshold_min)) { /* value above threshold means released */
            if (deb_filter[deb_ch].released_threshold_max > deb_filter[deb_ch].pressed_threshold_max) { /* Decremental threshold */
                deb_filter[deb_ch].detection = BUTTON_RELEASED;
                deb_filter[deb_ch].repetitions = 0U;
                deb_filter[deb_ch].button_pressed = false;
                deb_filter[deb_ch].time_spent_pressed = 0;
                deb_filter[deb_ch].time_stamp_pressed = 0;
            } else {  /* Incremental threshold */
                deb_filter[deb_ch].repetitions++;
                if (deb_filter[deb_ch].repetitions > MAX_DEBOUNCE_COUNT)
                    deb_filter[deb_ch].repetitions = MAX_DEBOUNCE_COUNT;

                deb_filter[deb_ch].time_spent_pressed = k_uptime_get();
                deb_filter[deb_ch].time_spent_pressed = deb_filter[deb_ch].time_spent_pressed - deb_filter[deb_ch].time_stamp_pressed;
                if(deb_filter[deb_ch].repetitions > deb_filter[deb_ch].debounce_threshold){
                    deb_filter[deb_ch].detection = BUTTON_PRESSED;
                    deb_filter[deb_ch].time_stamp_pressed = k_uptime_get();
                }else if (deb_filter[deb_ch].time_spent_pressed > config_get_reset_time())
                    deb_filter[deb_ch].detection = BUTTON_LONG_PRESSED;
                else
                    deb_filter[deb_ch].detection = BUTTON_RELEASED;
                
            }
        }
        else  if ((value > deb_filter[deb_ch].pressed_threshold_min) && (value < deb_filter[deb_ch].pressed_threshold_max)) {
            if (deb_filter[deb_ch].released_threshold_max > deb_filter[deb_ch].pressed_threshold_max) { /* Decremental threshold */
                deb_filter[deb_ch].repetitions++;
                if (deb_filter[deb_ch].repetitions > MAX_DEBOUNCE_COUNT)
                    deb_filter[deb_ch].repetitions = MAX_DEBOUNCE_COUNT;

                if(deb_filter[deb_ch].button_pressed){
                    deb_filter[deb_ch].time_spent_pressed = k_uptime_get();
                    deb_filter[deb_ch].time_spent_pressed = deb_filter[deb_ch].time_spent_pressed - deb_filter[deb_ch].time_stamp_pressed;
                }
                if(deb_filter[deb_ch].repetitions > deb_filter[deb_ch].debounce_threshold){
                    if (deb_filter[deb_ch].time_spent_pressed > config_get_reset_time()) deb_filter[deb_ch].detection = BUTTON_LONG_PRESSED;
                    else deb_filter[deb_ch].detection = BUTTON_PRESSED;
                    if(!deb_filter[deb_ch].button_pressed) deb_filter[deb_ch].time_stamp_pressed = k_uptime_get();
                    deb_filter[deb_ch].button_pressed = true;
                } else{
                    deb_filter[deb_ch].detection = BUTTON_RELEASED;
                    deb_filter[deb_ch].time_spent_pressed = 0;
                    deb_filter[deb_ch].time_stamp_pressed = 0;
                }

            } else { /* Incremental threshold */
                deb_filter[deb_ch].detection = BUTTON_RELEASED;
                deb_filter[deb_ch].repetitions = 0U;
            }
        }else{}
    }
    else /* When ret_st != STATUS_OK channel detection is forced to false and filter restarted */
    {
        deb_filter[deb_ch].failure = true;
        deb_filter[deb_ch].detection = BUTTON_RELEASED;
        deb_filter[deb_ch].repetitions = 0U;
        deb_filter[deb_ch].button_pressed = false;
        deb_filter[deb_ch].time_spent_pressed = 0;
        deb_filter[deb_ch].time_stamp_pressed = 0;
    }
}    

/** @brief Get detection status for one debounce channel
 */
RETStatus debounce_get(E_debounce_channel deb_ch, button_status *detection)
{
    S_ASSERT(deb_ch < E_DEBOUNCE_ID_MAX, ASSERT_BOUNDS, "Invalid debounce channel");
    //S_ASSERT(detection != NULL, ASSERT_BOUNDS, "Invalid pointer");
    
    RETStatus retval = STATUS_ERROR;
    if (deb_filter[deb_ch].failure == false)
    {
        *detection = deb_filter[deb_ch].detection;
        retval = STATUS_OK;
    }
    return retval;
}
