/**
 * @file   main.c
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for CAN UDS module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "application.h"

/* Global functions
---------------------------------------------------------------------------*/
void main(void)
{
    APP_Initialize();

    for (;;) {
        APP_Update();
    }
}
