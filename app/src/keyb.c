/**
 * @file   keyb.c
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for KEYB module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "adc.h"
#include "util.h"
#include "keyb.h"
#include "config.h"
#include "assert.h"
#include "can_claas.h"
#include "debounce.h"
#include "led.h"

/* Constant definitions
---------------------------------------------------------------------------*/

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(keyb);
#define S_KEYB_STR_DEF(ID_) \
{ \
    .WiperCtrlSel = ID_, \
    .WiperCtrlSet = 0U, \
    .WiperCV_X = 0U, \
    .WiperCV_X_old = 0U, \
    .WiperCtrlState = 0U, \
    .WiperCI_X = 0U, \
    .ActivationByCAN = false, \
    .ActivationByCANinit = false, \
    .status = BUTTON_RELEASED, \
    .status_old = BUTTON_RELEASED, \
    .time_stamp_cyclic = 0U, \
    .time_spent_cyclic = 0U, \
    .time_spent_since_last_change = 255, \
    .time_stamp_since_last_change = 255, \
    .AutoWipingActive = 0U, \
    .ParkPosSelected = 0U, \
    .Status = 0U, \
    .time_spent_pressed = 0U, \
    .time_stamp_pressed = 0U, \
    .activation_timer = 0U, \
    .max_time_pump_on = false, \
    .pump_activated = false, \
    .first_activation = true, \
    .time_spent_unpressed = 1U, \
    .time_stamp_unpressed = 0U, \
    .deactivation_timer = 0U, \
    .skipNextReleaseEvent = false, \
}

/* Project specific types
---------------------------------------------------------------------------*/
typedef enum WiperCtrlState_str_t
{
    ResponsePending = 0x4,
    RequestAccepted = 0x5,
    RequestRejected = 0x6,
    LockedOut = 0x7,
    LastFrame = 0x8,
    Error_state = 0xE,
    NotAvailable_state = 0xF,
    WiperCtrlStateMax,
} WiperCtrlState_str;

typedef enum WiperCI_X_str_t
{
    Normal_operating = 0x0,
    NotAvailable_CI = 0xF,
    WiperCI_XMax,
} WiperCI_X_str;

typedef enum Status_str_t
{
	STATUS_OFF = 0b00,
	STATUS_ON = 0b01,
} Status_str;

typedef struct wiper_str_t
{
    WiperCtrlSel_str WiperCtrlSel;
    WiperCtrlSet_str WiperCtrlSet;
    WiperCV_X_str WiperCV_X;          //indicates mode
    WiperCV_X_str WiperCV_X_old;      //TODO: remove if unused
    WiperCtrlState_str WiperCtrlState;
    WiperCI_X_str WiperCI_X;
    bool ActivationByCAN;
    bool ActivationByCANinit;
    button_status status;             ///< button status
    button_status status_old;         ///< button status old
    int64_t time_stamp_cyclic;          ///< initial time for cyclic frame in ms
    int64_t time_spent_cyclic;          ///< current time for cyclic frame in ms
    int64_t time_spent_since_last_change;          ///< initial time for last event change in ms
    int64_t time_stamp_since_last_change;          ///< current time for last event change in ms
    AutoWipingActive_str AutoWipingActive;
    ParkPosSelected_str ParkPosSelected;
    Status_str Status;
    int64_t time_spent_pressed;
    int64_t time_stamp_pressed;
    int64_t activation_timer;
    bool max_time_pump_on;
    bool pump_activated;
    bool first_activation;
    int64_t remainig_time;
    int64_t time_spent_unpressed;
    int64_t time_stamp_unpressed;
    int64_t deactivation_timer;
    bool skipNextReleaseEvent;
} Wiper_str;

/* Local variables
-----------------------------------------------------------------------------*/
int Keyb_voltage[KEYB_MAX];

/* Internal struct holding parameters for buttons configuration */
Wiper_str keyb_str[KEYB_MAX] = {
    S_KEYB_STR_DEF(LeftWiperButton), S_KEYB_STR_DEF(RearWiperButton), S_KEYB_STR_DEF(RightWiperButton),
    S_KEYB_STR_DEF(AutoButton), S_KEYB_STR_DEF(ParkPosButton), S_KEYB_STR_DEF(LeftPumpButton),
    S_KEYB_STR_DEF(RearPumpButton), S_KEYB_STR_DEF(RightPumpButton)
};

/* Internal struct holding parameters for buttons measures */
static const E_adc_channel keyb2adc[KEYB_MAX] = {
    MUXED_ADC_CHKEY4, MUXED_ADC_CHKEY6, MUXED_ADC_CHKEY8,
    MUXED_ADC_CHKEY2, MUXED_ADC_CHKEY1, MUXED_ADC_CHKEY3,
    MUXED_ADC_CHKEY5, MUXED_ADC_CHKEY7
};

/* Internal struct holding parameters for buttons status */
static const E_debounce_channel keyb2deb[KEYB_MAX] = {
    DEB_KEYB_4, DEB_KEYB_6, DEB_KEYB_8, DEB_KEYB_2,
    DEB_KEYB_1, DEB_KEYB_3, DEB_KEYB_5, DEB_KEYB_7
};

/* Local functions
---------------------------------------------------------------------------*/
static void set_RightWiper(uint8_t wiperMode);
static void set_LeftWiper(uint8_t wiperMode);

WiperCV_X_str keyb_get_wipercv_x(WiperCtrlSel_str button)
{
    return keyb_str[button - BUTTON_ID_OFFSET].WiperCV_X;
}

/** @brief: Configure the left wiper and LED parameters to set the outputs for the selected mode
 *  @param wiperMode: wiper function to set
 * */
static void set_LeftWiper(uint8_t wiperMode){
    keyb_str[LeftWiperButton].WiperCV_X = wiperMode;
    if (get_AutoModeSelected() != Off) {
        //Turn off auto mode
        set_FL_Auto_07(STATUS_OFF);
        set_FL_Auto_08(STATUS_OFF);
        keyb_str[AutoButton].WiperCV_X = Off;
        //check auto mode to turn on right wiper 
        if(((get_AutoWipingActive() == Right_wiper) || (get_AutoWipingActive() == Both)) && (get_AutoModeSelected() == HighSpeed))
            set_RightWiper(HighSpeed);
        else if(((get_AutoWipingActive() == Right_wiper) || (get_AutoWipingActive() == Both)) && (get_AutoModeSelected() == Intermittence))
            set_RightWiper(Intermittence);
        set_AutoWipingActive(No);
        set_AutoModeSelected(Off);
    }
    //Check requested wiper mode
    switch (wiperMode){
        case HighSpeed:
            set_ManLeftSelected(HighSpeed);
            set_FL_Left_01(STATUS_ON);
            set_FL_Left_02(STATUS_ON);
            break;
        case Intermittence:
            set_ManLeftSelected(Intermittence);
            set_FL_Left_01(STATUS_ON);
            set_FL_Left_02(STATUS_OFF);
            break;
        case Off:
            set_ManLeftSelected(Off);
            set_FL_Left_01(STATUS_OFF);
            set_FL_Left_02(STATUS_OFF);
            break;
        default:
            keyb_str[LeftWiperButton].WiperCV_X = Off;
            break;
    }
}

/** @brief: Configure the rear wiper and LED parameters to set the outputs for the selected mode
 *  @param wiperMode: wiper function to set
 * */
static void set_RearWiper(uint8_t wiperMode){
    keyb_str[RearWiperButton].WiperCV_X = wiperMode;
    switch (wiperMode){
        case HighSpeed:
            set_ManRearSelected(HighSpeed);
            set_FL_Rear_03(STATUS_ON);
            set_FL_Rear_04(STATUS_ON);
            break;
        case Intermittence:
            set_ManRearSelected(Intermittence);
            set_FL_Rear_03(STATUS_ON);
            set_FL_Rear_04(STATUS_OFF);
            break;
        case Off:
            set_ManRearSelected(Off);
            set_FL_Rear_03(STATUS_OFF);
            set_FL_Rear_04(STATUS_OFF);
            break;
        default:
            keyb_str[RearWiperButton].WiperCV_X = Off;
            break;
    }
}

/** @brief: Configure the right wiper and LED parameters to set the outputs for the selected mode
 *  @param wiperMode: wiper function to set
 * */
static void set_RightWiper(uint8_t wiperMode){
    keyb_str[RightWiperButton].WiperCV_X = wiperMode;
    if (get_AutoModeSelected() != Off) {
        //Turn off auto mode
        set_FL_Auto_07(STATUS_OFF);
        set_FL_Auto_08(STATUS_OFF);
        keyb_str[AutoButton].WiperCV_X = Off;
        //check auto mode to turn on right wiper 
        if(((get_AutoWipingActive() == Left_wiper) || (get_AutoWipingActive() == Both)) && (get_AutoModeSelected() == HighSpeed))
            set_LeftWiper(HighSpeed);
        else if(((get_AutoWipingActive() == Left_wiper) || (get_AutoWipingActive() == Both)) && (get_AutoModeSelected() == Intermittence))
            set_LeftWiper(Intermittence);
        set_AutoWipingActive(No);
        set_AutoModeSelected(Off);
    }
    //Check requested wiper mode
    switch (wiperMode){
        case HighSpeed:
            set_ManRightSelected(HighSpeed);
            set_FL_Right_05(STATUS_ON);
            set_FL_Right_06(STATUS_ON);
            break;
        case Intermittence:
            set_ManRightSelected(Intermittence);
            set_FL_Right_05(STATUS_ON);
            set_FL_Right_06(STATUS_OFF);
            break;
        case Off:
            set_ManRightSelected(Off);
            set_FL_Right_05(STATUS_OFF);
            set_FL_Right_06(STATUS_OFF);
            break;
        default:
            keyb_str[RightWiperButton].WiperCV_X = Off;
            break;
    }
}

/** @brief: Configure the auto wiping mode and LEDs to off mode
 * */
static void set_AutoModeOff (void){
    set_AutoModeSelected(Off);
    set_FL_Auto_07(STATUS_OFF);
    set_FL_Auto_08(STATUS_OFF);
    set_AutoWipingActive(No);
}

/** @brief: Configure the auto wiping and LED parameters to set the outputs for the selected mode
 *  @param wiperMode: wiper function to set
 * */
static void set_AutoWiper(uint8_t wiperMode){
    set_ManLeftSelected(Off);
    set_ManRightSelected(Off);
    set_FL_Right_05(STATUS_OFF);
    set_FL_Right_06(STATUS_OFF);
    set_FL_Left_01(STATUS_OFF);
    set_FL_Left_02(STATUS_OFF);
    keyb_str[AutoButton].WiperCV_X = wiperMode;
    keyb_str[LeftWiperButton].WiperCV_X = Off;
    keyb_str[RightWiperButton].WiperCV_X = Off;
    switch (wiperMode){
        case HighSpeed:
            set_AutoModeSelected(HighSpeed);
            set_FL_Auto_07(STATUS_ON);
            set_FL_Auto_08(STATUS_ON);
            break;
        case Intermittence:
            set_AutoModeSelected(Intermittence);
            set_FL_Auto_07(STATUS_ON);
            set_FL_Auto_08(STATUS_OFF);
            break;
        case Off:
            set_AutoModeOff();
            break;
        default:
            keyb_str[AutoButton].WiperCV_X = Off;
            break;
    }
}

/** @brief: Configure the parking position and LED parameters to set the outputs for the selected mode
 *  @param parkingMode: parking function to set
 * */
static void set_ParkingPosition(uint8_t parkingMode){
    keyb_str[ParkPosButton].WiperCV_X = parkingMode;
    switch (parkingMode){
        case Park_bottom:
            set_ParkPosSelected(Park_bottom);
            set_FL_Park_09(STATUS_ON);
            set_FL_Park_10(STATUS_OFF);
            break;
        case Park_top:
            set_ParkPosSelected(Park_top);
            set_FL_Park_09(STATUS_OFF);
            set_FL_Park_10(STATUS_ON);
            break;
        default:
            keyb_str[ParkPosButton].WiperCV_X = Off;
            break;
    }
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function for KEYB module
 */
void keyb_init(void)
{
    MODULE_SINGLE_INIT;   

    set_ParkPosSelected(Park_top);
    set_FL_Park_10(STATUS_ON);

    set_PumpLeftOutput(STATUS_OFF);
    set_PumpRearOutput(STATUS_OFF);
    set_PumpRightOutput(STATUS_OFF);
}

/** @brief Main function for sending KEYB module
 */
void keyb_step(void)
{
    int value = 0;
    static num_of_buttons keyb_channel = KEYB_1;
    
    for (uint8_t keyb_flw = 0U; keyb_flw < 5U; keyb_flw++){
        switch (keyb_flw) {
            case 0U:
                /* Get the adc values */
                if (adc_get(keyb2adc[keyb_channel], &value) == STATUS_OK)
                    Keyb_voltage[keyb_channel] = value;
                if(debounce_get(keyb2deb[keyb_channel], &keyb_str[keyb_channel].status) == STATUS_OK){ /* TODO: Remove logic if unused case*/ }
                else { keyb_str[keyb_channel].status = BUTTON_FAILURE;}
            break;
            case 1U:
                /* Time update */
                keyb_str[keyb_channel].time_spent_since_last_change = k_uptime_get();
                keyb_str[keyb_channel].time_spent_since_last_change = keyb_str[keyb_channel].time_spent_since_last_change - keyb_str[keyb_channel].time_stamp_since_last_change;
                keyb_str[keyb_channel].time_spent_cyclic = k_uptime_get();
                keyb_str[keyb_channel].time_spent_cyclic = keyb_str[keyb_channel].time_spent_cyclic - keyb_str[keyb_channel].time_stamp_cyclic;
            break;
            case 2U:
                // TODO: enable it when CAN module is enabled
                /* If time since last frame is not bigger than minimum we not send any frame */
                // time_since_send_last_frame = k_uptime_get();
                // time_since_send_last_frame = time_since_send_last_frame - time_stamp_since_send_last_frame;
                // if (time_since_send_last_frame > config_get_min_time_bet_two_emissions()){
                    /* Change of state frame */
                    if(keyb_str[keyb_channel].status != keyb_str[keyb_channel].status_old){
                        LOG_DBG("\nkey button %d changed: value %d; status %d \n", keyb_channel + 1, Keyb_voltage[keyb_channel], keyb_str[keyb_channel].status);
                        // keyb_can_send(keyb_channel);
                    }
                    set_WiperCtrlSel(keyb_channel);
                // }
            break;
            case 3U:
                // TODO: enable it when CAN module is enabled
                /* If time since last frame is not bigger than minimum we not send any frame */
                // time_since_send_last_frame = k_uptime_get();
                // time_since_send_last_frame = time_since_send_last_frame - time_stamp_since_send_last_frame;
                // if (time_since_send_last_frame > config_get_min_time_bet_two_emissions()){
                //     /* cyclic frame */
                //     if (keyb_str[keyb_channel].time_spent_cyclic > config_get_cycle_time()){
                //         LOG_DBG("\nkey button %d time_spent_cyclic %lld\n",keyb_channel + 1, keyb_str[keyb_channel].time_spent_cyclic);
                //         keyb_str[keyb_channel].time_stamp_cyclic = k_uptime_get();
                //         // keyb_can_send(keyb_channel);
                //     }
                // }
            break;
            case 4U:
                keyb_channel++;
                if(keyb_channel == KEYB_MAX) { keyb_channel = KEYB_1;}
            break;  
            default:
            break;        
        }
    }    
}

/** @brief voltage getter
 * @param button - button ID
 * @param voltage - Valid pointer to data
 */
void keyb_get_voltage(const num_of_buttons keyb_channel, int *voltage)
{
    *voltage = Keyb_voltage[keyb_channel];
}

/** @brief get keyboard button status for one keyboard channel
 * @param[in] keyb_channel specific button number/identifier
 * @param[in] status request button state
 * @return STATUS_OK if successful
 *
 * For @ref status KEYBST_RELEASED, button is not pressed
 * For @ref status KEYBST_PRESSED, button is pressed at this moment
 * For @ref status KEYBST_UNDETERMINED, it is not possible to determine de status
 * For @ref status KEYBST_FAILURE, something wrong calculating the status.
 */
RETStatus keyb_get(const num_of_buttons keyb_channel, uint8_t *status)
{
    S_ASSERT(keyb_channel < KEYB_MAX, ASSERT_BOUNDS, "Invalid keyb channel");
    S_ASSERT(status != NULL, ASSERT_BOUNDS, "Invalid pointer");

    RETStatus retval = debounce_get(keyb2deb[keyb_channel], status);

    return retval;
}

/** @brief wiper CAN getter info
 * @param data - CAN bytes
 */
void keyb_set_data(uint8_t *data)
{
    static uint8_t WiperCtrlSel = 0;
    WiperCtrlSel = data[0] - BUTTON_ID_OFFSET;
    keyb_str[WiperCtrlSel].WiperCtrlSel = data[0];
    keyb_str[WiperCtrlSel].WiperCtrlSet = data[1] >> 4;
    keyb_str[WiperCtrlSel].WiperCV_X = data[2];
    keyb_str[WiperCtrlSel].status = BUTTON_RELEASED;
    keyb_str[WiperCtrlSel].status_old = BUTTON_RELEASED;
    if(keyb_str[WiperCtrlSel].WiperCV_X == Off){ // off pumps, default park
        switch (WiperCtrlSel){
        case LeftWiperButton:
            set_LeftWiper(Off);
            break;
        case RearWiperButton:
            set_RearWiper(Off);
            break;
        case RightWiperButton:
            set_RightWiper(Off);
            break;
        case AutoButton:
            set_AutoWiper(Off);
            break;
        case ParkPosButton:
            set_ParkingPosition(Park_top);
            break;
        case LeftPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = false;
            break;
        case RearPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = false;
            break;
        case RightPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = false;
            break;
        default:
            break;
        }
    }
    else if(keyb_str[WiperCtrlSel].WiperCV_X == HighSpeed){
        switch (WiperCtrlSel){
        case LeftWiperButton:
            set_LeftWiper(HighSpeed);
            break;
        case RearWiperButton:
            set_RearWiper(HighSpeed);
            break;
        case RightWiperButton:
            set_RightWiper(HighSpeed);
            break;
        case AutoButton:
            set_AutoWiper(HighSpeed);
            break;
        case ParkPosButton:
            break;
        case LeftPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        case RearPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        case RightPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        default:
            break;
        }
    }
    else if(keyb_str[WiperCtrlSel].WiperCV_X == Intermittence){ // on pumps, alternative park
        switch (WiperCtrlSel){
        case LeftWiperButton:
            set_LeftWiper(Intermittence);
            break;
        case RearWiperButton:
            set_RearWiper(Intermittence);
            break;
        case RightWiperButton:
            set_RightWiper(Intermittence);
            break;
        case AutoButton:
            set_AutoWiper(Intermittence);
            break;
        case ParkPosButton:
            set_ParkingPosition(Park_bottom);
            break;
        case LeftPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        case RearPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        case RightPumpButton:
            keyb_str[WiperCtrlSel].ActivationByCAN = true;
            keyb_str[WiperCtrlSel].ActivationByCANinit = false;
            break;
        default:
            break;
        }
    }
    set_WiperCtrlSel(WiperCtrlSel);
}

/** @brief wiper control
 * @param keyb_channel - button ID
 */
void set_WiperCtrlSel(const num_of_buttons keyb_channel)
{
    switch ((WiperCtrlSel_str) keyb_channel){
        case LeftWiperButton:
            if((keyb_str[keyb_channel].status == BUTTON_RELEASED) && (keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (get_ManLeftSelected() == Intermittence)){
                if(!keyb_str[keyb_channel].skipNextReleaseEvent){ set_LeftWiper(HighSpeed);}
                else { keyb_str[keyb_channel].skipNextReleaseEvent = false;}
            }
            else if((keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (keyb_str[keyb_channel].status == BUTTON_LONG_PRESSED) && (get_ManLeftSelected() == Intermittence) && !keyb_str[keyb_channel].skipNextReleaseEvent)
                set_LeftWiper(Off);
            else if((keyb_str[keyb_channel].status_old == BUTTON_RELEASED) && (keyb_str[keyb_channel].status == BUTTON_PRESSED)){
                if(get_ManLeftSelected() == HighSpeed)
                    set_LeftWiper(Off);
                else if(get_ManLeftSelected() == Off){
                    if ((get_AutoModeSelected() == HighSpeed) && ((get_AutoWipingActive() == Left_wiper) || (get_AutoWipingActive() == Both)))
                        set_LeftWiper(HighSpeed);
                    else {
                        set_LeftWiper(Intermittence);
                        keyb_str[keyb_channel].skipNextReleaseEvent = true;
                    }
                }
            }
            break;
        case RearWiperButton:
            if((keyb_str[keyb_channel].status == BUTTON_RELEASED) && (keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (get_ManRearSelected() == Intermittence)){
                if(!keyb_str[keyb_channel].skipNextReleaseEvent){ set_RearWiper(HighSpeed);}
                else { keyb_str[keyb_channel].skipNextReleaseEvent = false;}
            }
            else if((keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (keyb_str[keyb_channel].status == BUTTON_LONG_PRESSED) && (get_ManRearSelected() == Intermittence) && !keyb_str[keyb_channel].skipNextReleaseEvent)
                set_RearWiper(Off);
            else if((keyb_str[keyb_channel].status_old == BUTTON_RELEASED) && (keyb_str[keyb_channel].status == BUTTON_PRESSED)){
                if(get_ManRearSelected() == HighSpeed)
                    set_RearWiper(Off);
                else if(get_ManRearSelected() == Off){
                    set_RearWiper(Intermittence);
                    keyb_str[keyb_channel].skipNextReleaseEvent = true;
                }
            }
            break;
        case RightWiperButton:
            if((keyb_str[keyb_channel].status == BUTTON_RELEASED) && (keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (get_ManRightSelected() == Intermittence)){
                if(!keyb_str[keyb_channel].skipNextReleaseEvent){ set_RightWiper(HighSpeed);}
                else { keyb_str[keyb_channel].skipNextReleaseEvent = false;}
            }
            else if((keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (keyb_str[keyb_channel].status == BUTTON_LONG_PRESSED) && (get_ManRightSelected() == Intermittence) && !keyb_str[keyb_channel].skipNextReleaseEvent)
                set_RightWiper(Off);
            else if((keyb_str[keyb_channel].status_old == BUTTON_RELEASED) && (keyb_str[keyb_channel].status == BUTTON_PRESSED)){
                if(get_ManRightSelected() == HighSpeed)
                    set_RightWiper(Off);
                else if(get_ManRightSelected() == Off){
                    if ((get_AutoModeSelected() == HighSpeed) && ((get_AutoWipingActive() == Right_wiper) || (get_AutoWipingActive() == Both)))
                        set_RightWiper(HighSpeed);
                    else {
                        set_RightWiper(Intermittence);
                        keyb_str[keyb_channel].skipNextReleaseEvent = true;
                    }
                }
            }
            break;
        case AutoButton:      
            if((keyb_str[keyb_channel].status == BUTTON_RELEASED) && (keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (get_AutoModeSelected() == Intermittence)){
                if(!keyb_str[keyb_channel].skipNextReleaseEvent){ set_AutoWiper(HighSpeed);}
                else { keyb_str[keyb_channel].skipNextReleaseEvent = false;}
            }
            else if((keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (keyb_str[keyb_channel].status == BUTTON_LONG_PRESSED) && (get_AutoModeSelected() == Intermittence) && !keyb_str[keyb_channel].skipNextReleaseEvent)
                set_AutoWiper(Off);
            else if((keyb_str[keyb_channel].status_old == BUTTON_RELEASED) && (keyb_str[keyb_channel].status == BUTTON_PRESSED)){                
                if(get_AutoModeSelected() == HighSpeed)
                    set_AutoWiper(Off);
                else if(get_AutoModeSelected() == Off){
                    set_AutoWiper(Intermittence);
                    keyb_str[keyb_channel].skipNextReleaseEvent = true;
                }
            }
            break;

        case ParkPosButton:
            if((keyb_str[keyb_channel].status == BUTTON_RELEASED) && (keyb_str[keyb_channel].status_old == BUTTON_PRESSED)){
                if(get_ParkPosSelected() == Park_top)
                    set_ParkingPosition(Park_bottom);
                else if(get_ParkPosSelected() == Park_bottom)
                    set_ParkingPosition(Park_top);
            }
            else if((keyb_str[keyb_channel].status_old == BUTTON_PRESSED) && (keyb_str[keyb_channel].status == BUTTON_LONG_PRESSED))
                set_ParkingPosition(Park_bottom);
            break;

        case LeftPumpButton:
            if(((keyb_str[LeftPumpButton].status == BUTTON_PRESSED) && (keyb_str[LeftPumpButton].status_old == BUTTON_RELEASED)) || ((keyb_str[LeftPumpButton].ActivationByCAN) && (!keyb_str[LeftPumpButton].ActivationByCANinit))){
                keyb_str[LeftPumpButton].ActivationByCANinit = true;
                keyb_str[LeftPumpButton].max_time_pump_on = false;
                keyb_str[LeftPumpButton].time_stamp_pressed = k_uptime_get();
                keyb_str[LeftPumpButton].deactivation_timer = (((config_get_max_pump_time())*config_get_pause_pump_ratio())/100);

                if((keyb_str[LeftPumpButton].time_spent_pressed) > keyb_str[LeftPumpButton].activation_timer){
                    keyb_str[LeftPumpButton].time_spent_pressed = keyb_str[LeftPumpButton].activation_timer;
                    keyb_str[LeftPumpButton].remainig_time = 0U;
                }
                else {
                    keyb_str[LeftPumpButton].remainig_time = keyb_str[LeftPumpButton].activation_timer - keyb_str[LeftPumpButton].time_spent_pressed;
                }

                if((keyb_str[LeftPumpButton].time_spent_unpressed > keyb_str[LeftPumpButton].deactivation_timer) || (keyb_str[LeftPumpButton].first_activation)){
                    keyb_str[LeftPumpButton].time_spent_unpressed = keyb_str[LeftPumpButton].deactivation_timer;
                    keyb_str[LeftPumpButton].activation_timer = config_get_max_pump_time();
                    keyb_str[LeftPumpButton].first_activation = false;
                }
                else {
                    keyb_str[LeftPumpButton].activation_timer = (((keyb_str[LeftPumpButton].time_spent_unpressed) * 100) / config_get_pause_pump_ratio()) + keyb_str[LeftPumpButton].remainig_time;
                    if(keyb_str[LeftPumpButton].activation_timer > config_get_max_pump_time()) keyb_str[LeftPumpButton].activation_timer  = config_get_max_pump_time();
                }
            }

            if((keyb_str[LeftPumpButton].status == BUTTON_PRESSED) || (keyb_str[LeftPumpButton].status == BUTTON_LONG_PRESSED) || (keyb_str[LeftPumpButton].ActivationByCAN)){
                keyb_str[LeftPumpButton].time_spent_pressed = k_uptime_get();
                keyb_str[LeftPumpButton].time_spent_pressed = keyb_str[LeftPumpButton].time_spent_pressed - keyb_str[LeftPumpButton].time_stamp_pressed;
                if((keyb_str[LeftPumpButton].time_spent_pressed < keyb_str[LeftPumpButton].activation_timer) && (keyb_str[LeftPumpButton].activation_timer > 1000U)){
                    set_PumpLeftOutput(STATUS_ON);
                    keyb_str[LeftPumpButton].WiperCV_X = Intermittence;
                    keyb_str[LeftPumpButton].pump_activated = true;
                    if(!config_get_c_output_enable()){ set_WiperLeftOutput(get_WiperLeftOutput() | WiperXOutput_Wash);}
                }
                else {
                    set_PumpLeftOutput(STATUS_OFF);
                    keyb_str[LeftPumpButton].WiperCV_X = Off;
                    if(!config_get_c_output_enable()){ set_WiperLeftOutput(get_WiperLeftOutput() & (~WiperXOutput_Wash));}
                    if (!keyb_str[LeftPumpButton].max_time_pump_on){
                        if (keyb_str[LeftPumpButton].activation_timer > 1000U) {
                            keyb_str[LeftPumpButton].time_stamp_unpressed = k_uptime_get();
                        }
                        keyb_str[LeftPumpButton].max_time_pump_on = true;
                    }
                }
            }

            if((keyb_str[LeftPumpButton].pump_activated) || ((keyb_str[LeftPumpButton].status == BUTTON_RELEASED) && ((keyb_str[LeftPumpButton].status_old == BUTTON_PRESSED) || (keyb_str[LeftPumpButton].status_old == BUTTON_LONG_PRESSED)))){
                keyb_str[LeftPumpButton].pump_activated = false;
                keyb_str[LeftPumpButton].time_stamp_unpressed = k_uptime_get();
            }

            if((keyb_str[LeftPumpButton].status == BUTTON_RELEASED) || ((keyb_str[LeftPumpButton].max_time_pump_on) && (keyb_str[LeftPumpButton].status == BUTTON_PRESSED))){
                keyb_str[LeftPumpButton].time_spent_unpressed = k_uptime_get();
                keyb_str[LeftPumpButton].time_spent_unpressed = keyb_str[LeftPumpButton].time_spent_unpressed - keyb_str[LeftPumpButton].time_stamp_unpressed;
                if ((keyb_str[LeftPumpButton].status != BUTTON_RELEASED) || (keyb_str[LeftPumpButton].max_time_pump_on) || (!keyb_str[LeftPumpButton].ActivationByCAN)){
                    set_PumpLeftOutput(STATUS_OFF);
                    if(!config_get_c_output_enable()){ set_WiperLeftOutput(get_WiperLeftOutput() & (~WiperXOutput_Wash));}
                }
            }
            break;

        case RearPumpButton:
            if(((keyb_str[RearPumpButton].status == BUTTON_PRESSED) && (keyb_str[RearPumpButton].status_old == BUTTON_RELEASED)) || ((keyb_str[RearPumpButton].ActivationByCAN) && (!keyb_str[RearPumpButton].ActivationByCANinit))){
                keyb_str[RearPumpButton].ActivationByCANinit = true;
                keyb_str[RearPumpButton].max_time_pump_on = false;
                keyb_str[RearPumpButton].time_stamp_pressed = k_uptime_get();
                keyb_str[RearPumpButton].deactivation_timer = (((config_get_max_pump_time())*config_get_pause_pump_ratio())/100);

                if((keyb_str[RearPumpButton].time_spent_pressed) > keyb_str[RearPumpButton].activation_timer){
                    keyb_str[RearPumpButton].time_spent_pressed = keyb_str[RearPumpButton].activation_timer;
                    keyb_str[RearPumpButton].remainig_time = 0U;
                }
                else {
                    keyb_str[RearPumpButton].remainig_time = keyb_str[RearPumpButton].activation_timer - keyb_str[RearPumpButton].time_spent_pressed;
                }

                if((keyb_str[RearPumpButton].time_spent_unpressed > keyb_str[RearPumpButton].deactivation_timer) || (keyb_str[RearPumpButton].first_activation)){
                    keyb_str[RearPumpButton].time_spent_unpressed = keyb_str[RearPumpButton].deactivation_timer;
                    keyb_str[RearPumpButton].activation_timer = config_get_max_pump_time();
                    keyb_str[RearPumpButton].first_activation = false;
                }
                else {
                    keyb_str[RearPumpButton].activation_timer = (((keyb_str[RearPumpButton].time_spent_unpressed) * 100) / config_get_pause_pump_ratio()) + keyb_str[RearPumpButton].remainig_time;
                    if(keyb_str[RearPumpButton].activation_timer > config_get_max_pump_time()) keyb_str[RearPumpButton].activation_timer  = config_get_max_pump_time();
                }
            }

            if((keyb_str[RearPumpButton].status == BUTTON_PRESSED) || (keyb_str[RearPumpButton].status == BUTTON_LONG_PRESSED) || (keyb_str[RearPumpButton].ActivationByCAN)){
                keyb_str[RearPumpButton].time_spent_pressed = k_uptime_get();
                keyb_str[RearPumpButton].time_spent_pressed = keyb_str[RearPumpButton].time_spent_pressed - keyb_str[RearPumpButton].time_stamp_pressed;
                if((keyb_str[RearPumpButton].time_spent_pressed < keyb_str[RearPumpButton].activation_timer) && (keyb_str[RearPumpButton].activation_timer > 1000U)){
                    set_PumpRearOutput(STATUS_ON);
                    keyb_str[RearPumpButton].WiperCV_X = Intermittence;
                    keyb_str[RearPumpButton].pump_activated = true;
                    if(!config_get_c_output_enable()){ set_WiperRearOutput(get_WiperRearOutput() | WiperXOutput_Wash);}
                }
                else {
                    set_PumpRearOutput(STATUS_OFF);
                    keyb_str[RearPumpButton].WiperCV_X = Off;
                    if(!config_get_c_output_enable()){ set_WiperRearOutput(get_WiperRearOutput() & (~WiperXOutput_Wash));}
                    if (!keyb_str[RearPumpButton].max_time_pump_on){
                        if (keyb_str[RearPumpButton].activation_timer > 1000U) {
                            keyb_str[RearPumpButton].time_stamp_unpressed = k_uptime_get();
                        }
                        keyb_str[RearPumpButton].max_time_pump_on = true;
                    }
                }
            }

            if((keyb_str[RearPumpButton].pump_activated) || ((keyb_str[RearPumpButton].status == BUTTON_RELEASED) && ((keyb_str[RearPumpButton].status_old == BUTTON_PRESSED) || (keyb_str[RearPumpButton].status_old == BUTTON_LONG_PRESSED)))){
                keyb_str[RearPumpButton].pump_activated = false;
                keyb_str[RearPumpButton].time_stamp_unpressed = k_uptime_get();
            }

            if((keyb_str[RearPumpButton].status == BUTTON_RELEASED) || ((keyb_str[RearPumpButton].max_time_pump_on) && (keyb_str[RearPumpButton].status == BUTTON_PRESSED))){
                keyb_str[RearPumpButton].time_spent_unpressed = k_uptime_get();
                keyb_str[RearPumpButton].time_spent_unpressed = keyb_str[RearPumpButton].time_spent_unpressed - keyb_str[RearPumpButton].time_stamp_unpressed;
                if ((keyb_str[RearPumpButton].status != BUTTON_RELEASED) || (keyb_str[RearPumpButton].max_time_pump_on) || (!keyb_str[RearPumpButton].ActivationByCAN)){
                    set_PumpRearOutput(STATUS_OFF);
                    if(!config_get_c_output_enable()){ set_WiperRearOutput(get_WiperRearOutput() & (~WiperXOutput_Wash));}
                }
            }
            break;

        case RightPumpButton:  
            if(((keyb_str[RightPumpButton].status == BUTTON_PRESSED) && (keyb_str[RightPumpButton].status_old == BUTTON_RELEASED)) || ((keyb_str[RightPumpButton].ActivationByCAN) && (!keyb_str[RightPumpButton].ActivationByCANinit))){
                keyb_str[RightPumpButton].ActivationByCANinit = true;
                keyb_str[RightPumpButton].max_time_pump_on = false;
                keyb_str[RightPumpButton].time_stamp_pressed = k_uptime_get();
                keyb_str[RightPumpButton].deactivation_timer = (((config_get_max_pump_time())*config_get_pause_pump_ratio())/100);

                if((keyb_str[RightPumpButton].time_spent_pressed) > keyb_str[RightPumpButton].activation_timer){
                    keyb_str[RightPumpButton].time_spent_pressed = keyb_str[RightPumpButton].activation_timer;
                    keyb_str[RightPumpButton].remainig_time = 0U;
                }
                else {
                    keyb_str[RightPumpButton].remainig_time = keyb_str[RightPumpButton].activation_timer - keyb_str[RightPumpButton].time_spent_pressed;
                }

                if((keyb_str[RightPumpButton].time_spent_unpressed > keyb_str[RightPumpButton].deactivation_timer) || (keyb_str[RightPumpButton].first_activation)){
                    keyb_str[RightPumpButton].time_spent_unpressed = keyb_str[RightPumpButton].deactivation_timer;
                    keyb_str[RightPumpButton].activation_timer = config_get_max_pump_time();
                    keyb_str[RightPumpButton].first_activation = false;
                }
                else {
                    keyb_str[RightPumpButton].activation_timer = (((keyb_str[RightPumpButton].time_spent_unpressed) * 100) / config_get_pause_pump_ratio()) + keyb_str[RightPumpButton].remainig_time;
                    if(keyb_str[RightPumpButton].activation_timer > config_get_max_pump_time()) keyb_str[RightPumpButton].activation_timer  = config_get_max_pump_time();
                }
            }

            if((keyb_str[RightPumpButton].status == BUTTON_PRESSED) || (keyb_str[RightPumpButton].status == BUTTON_LONG_PRESSED) || (keyb_str[RightPumpButton].ActivationByCAN)){
                keyb_str[RightPumpButton].time_spent_pressed = k_uptime_get();
                keyb_str[RightPumpButton].time_spent_pressed = keyb_str[RightPumpButton].time_spent_pressed - keyb_str[RightPumpButton].time_stamp_pressed;
                if((keyb_str[RightPumpButton].time_spent_pressed < keyb_str[RightPumpButton].activation_timer) && (keyb_str[RightPumpButton].activation_timer > 1000U)){
                    set_PumpRightOutput(STATUS_ON);
                    keyb_str[RightPumpButton].WiperCV_X = Intermittence;
                    keyb_str[RightPumpButton].pump_activated = true;
                    if(!config_get_c_output_enable()){ set_WiperRightOutput(get_WiperRightOutput() | WiperXOutput_Wash);}
                }
                else {
                    set_PumpRightOutput(STATUS_OFF);
                    keyb_str[RightPumpButton].WiperCV_X = Off;
                    if(!config_get_c_output_enable()){ set_WiperRightOutput(get_WiperRightOutput() & (~WiperXOutput_Wash));}
                    if (!keyb_str[RightPumpButton].max_time_pump_on){
                        if (keyb_str[RightPumpButton].activation_timer > 1000U) {
                            keyb_str[RightPumpButton].time_stamp_unpressed = k_uptime_get();
                        }
                        keyb_str[RightPumpButton].max_time_pump_on = true;
                    }
                }
            }

            if((keyb_str[RightPumpButton].pump_activated) || ((keyb_str[RightPumpButton].status == BUTTON_RELEASED) && ((keyb_str[RightPumpButton].status_old == BUTTON_PRESSED) || (keyb_str[RightPumpButton].status_old == BUTTON_LONG_PRESSED)))){
                keyb_str[RightPumpButton].pump_activated = false;
                keyb_str[RightPumpButton].time_stamp_unpressed = k_uptime_get();
            }

            if((keyb_str[RightPumpButton].status == BUTTON_RELEASED) || ((keyb_str[RightPumpButton].max_time_pump_on) && (keyb_str[RightPumpButton].status == BUTTON_PRESSED))){
                keyb_str[RightPumpButton].time_spent_unpressed = k_uptime_get();
                keyb_str[RightPumpButton].time_spent_unpressed = keyb_str[RightPumpButton].time_spent_unpressed - keyb_str[RightPumpButton].time_stamp_unpressed;
                if ((keyb_str[RightPumpButton].status != BUTTON_RELEASED) || (keyb_str[RightPumpButton].max_time_pump_on) || (!keyb_str[RightPumpButton].ActivationByCAN)){
                    set_PumpRightOutput(STATUS_OFF);
                    if(!config_get_c_output_enable()){ set_WiperRightOutput(get_WiperRightOutput() & (~WiperXOutput_Wash));}
                }
            }
            break;

        default:
            break;
    }
    //Update old status
    keyb_str[keyb_channel].status_old = keyb_str[keyb_channel].status;
    keyb_str[keyb_channel].time_stamp_cyclic = k_uptime_get();
    keyb_str[keyb_channel].time_stamp_since_last_change = k_uptime_get();
}

/** @brief: process the switch2B values to turn ON or not the side wipers
 *  @param val1_left: if 1, it enables continuous wiping in left wiper
 *  @param val2_right: if 1, it enables continuous wiping in right wiper
 * */
void keyb_hotKey(uint8_t val1_left, uint8_t val2_right){

    static uint8_t WiperLeftOutput;
    static uint8_t WiperRightOutput;
    static uint8_t AutoModeSelected;
    static uint8_t AutoWipingActive;
    static bool flag_saved_left = false;
    static bool flag_saved_right = false;

    //Get auto mode configuration
    if ((!flag_saved_left) && (!flag_saved_right)){
        AutoModeSelected = get_AutoModeSelected();
        AutoWipingActive = get_AutoWipingActive();
    }

    //Set left wiper output
    if(val1_left == 1){
        if(!flag_saved_left){
            WiperLeftOutput = get_ManLeftSelected();
            flag_saved_left = true;
        }
        set_AutoModeOff();
        set_LeftWiper(HighSpeed);
    }else if(flag_saved_left){
        set_LeftWiper(WiperLeftOutput);
        flag_saved_left = false;
    }

    //Set right wiper output
    if(val2_right == 1){
        if(!flag_saved_right){
            WiperRightOutput = get_ManRightSelected();
            flag_saved_right = true;
        }
        set_AutoModeOff();
        set_RightWiper(HighSpeed);
    }
    else if(flag_saved_right){
        set_RightWiper(WiperRightOutput);
        flag_saved_right = false;
    }

    //Set auto mode configuration
    if((val1_left == 0) && (val2_right == 0) && (AutoModeSelected != Off)){
        set_AutoWiper(AutoModeSelected);
        set_AutoWipingActive(AutoWipingActive);
    }
}
