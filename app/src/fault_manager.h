#pragma once
/**
 * @file   fault_manager.h
 * @author Abel Tarragó
 * @date   April 2022
 * @brief  Public APIs and resources for fault manager module.
 */

/* Global functions declaration
---------------------------------------------------------------------------*/
void fault_manager_step(void);
