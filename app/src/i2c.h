#pragma once
/**
 * @file   i2c.h
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for I2C module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
void i2c_init(void);
RETStatus i2c_write_func(const uint16_t addr, uint8_t *data, const uint32_t num_bytes);
RETStatus i2c_read_func(const uint16_t addr, uint8_t *data, const uint32_t num_bytes);
