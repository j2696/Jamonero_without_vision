#pragma once
/**
 * @file   crc16.c
 * @author Rubén Guijarro
 * @date   Februrary 2022
 * @brief  Public APIs and resources for CRC 16 calculation.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
/** @brief Public function
 */
uint16_t crcCalculation(const uint8_t * pBuffer, uint16_t u16StartValue, uint32_t u32Count);
