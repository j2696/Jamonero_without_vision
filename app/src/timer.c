/**
 * @file   timer.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for Timer module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "timer.h"
#include "assert.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define TIMER2_CNT          1U /* in ms */

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(timer, CONFIG_LOG_DEFAULT_LEVEL);

/* Local variables
-----------------------------------------------------------------------------*/
uint32_t T2u64totalizer = 0U;
atomic_t atom_T2_var;
static bool vflag_timer[] = { false, false };

/* Local functions
-----------------------------------------------------------------------------*/
/* work for timer 1 */
static void w_timerM1_handler(struct k_work *work)
{
    vflag_timer[E_TIMER_1] = true;
}

K_WORK_DEFINE(w_timerM1, w_timerM1_handler);

/* work for timer 2 */
static void w_timerM2_handler(struct k_work *work)
{
    vflag_timer[E_TIMER_2] = true;
    
    static bool last_state = false;
    
    if (last_state == false)
    {
        if (atomic_cas(&(atom_T2_var), 0U, 1U))
        {
            last_state = true;
        }
        else
            LOG_DBG("T2 sync error \n");
    }
    else
    {
        if (atomic_cas(&(atom_T2_var), 1U, 0U))
        {
            last_state = false;
        }
        else
            LOG_DBG("T2 sync error \n");
    }

    T2u64totalizer++;
}

K_WORK_DEFINE(w_timerM2, w_timerM2_handler);

/* Timer 1 configuration */
static void timerM1_callback(struct k_timer *timer_id)
{
    k_work_submit(&w_timerM1);
}

K_TIMER_DEFINE(timerM1, timerM1_callback, NULL);

/* Timer 2 configuration */
static void timerM2_callback(struct k_timer *timer_id)
{
    k_work_submit(&w_timerM2);
}

K_TIMER_DEFINE(timerM2, timerM2_callback, NULL);

void timer_start_count(const E_timer_inst timer_inst, uint32_t time_us)
{
    S_ASSERT(timer_inst < E_TIMER_MAX, ASSERT_BOUNDS, "Invalid timer");

    switch (timer_inst) {
        case E_TIMER_1:
            k_timer_start(&timerM1, K_USEC(time_us), K_USEC(time_us));
            break;
        case E_TIMER_2:
            k_timer_start(&timerM2, K_MSEC(time_us), K_MSEC(time_us));
            break;
        default:
            break;
    }
}

uint32_t timer_get_totalTicks(const E_timer_inst timer_inst)
{
    switch (timer_inst) {
        case E_TIMER_1:
            break;
        case E_TIMER_2:
            return T2u64totalizer;
        default:
            break;
    }   
    return 0;
}

void timer_init(void)
{
    /* Timer T2 in use for synchronization of the main appl loop */
    atomic_clear(&(atom_T2_var));
    timer_start_count(E_TIMER_2, TIMER2_CNT);
}

