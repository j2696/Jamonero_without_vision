#pragma once
/**
 * @file   debounce.h
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for debounce helper.
 */

/* header of project specific types
-----------------------------------------------------------------------------*/
#include "keyb.h"

/* project specific types
---------------------------------------------------------------------------*/
/** @brief debounce_channel enum
 * Define the index of the debounce register
 */
typedef enum E_debounce_channel_t
{    
    DEB_KEYB_1, DEB_KEYB_2, DEB_KEYB_3, DEB_KEYB_4, 
    DEB_KEYB_5, DEB_KEYB_6, DEB_KEYB_7, DEB_KEYB_8,
    DEB_IGN_ANALOG,
    DIAG_WASH_1, DIAG_WASH_2, DIAG_WASH_3, 
    E_DEBOUNCE_ID_MAX,
} E_debounce_channel;

/* Global functions declaration
---------------------------------------------------------------------------*/
void debounce_refresh(E_debounce_channel deb_ch, RETStatus ret_st, int value);
RETStatus debounce_get(E_debounce_channel deb_ch, button_status *detection);
