#pragma once
/**
 * @file   tle.h
 * @author Rubén Guijarro
 * @date   February 2022
 * @brief  Public APIs and resources for TLE module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
void tle_init(void);
void tle_step(void);
uint16_t get_tle_outputs (void);
void set_tle_outputs (uint16_t values);
uint8_t set_tle_individual_output (uint8_t value, uint8_t id);
uint16_t get_tle_status (void);
uint16_t get_tle_open_load_status (void);
uint16_t get_tle_over_load_status (void);
uint16_t get_tle_OL_OFF_status (void);
