#pragma once
/**
 * @file   adc.h
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for ADC module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Project specific types
---------------------------------------------------------------------------*/
/**
 * @brief adc_channel enum
 * Starting with ADC_ the list of hardware ADC input ids
 * These should also be declared on the dts overlay (zephyr,user.io-channels).
 * The device name list and io-channel-ids should follow the same order in the (internal) 
 * @ref channel_ids array
 */
typedef enum E_adc_channel_t
{   
    MUXED_ADC_CHKEY1 = 0U,
    MUXED_ADC_CHKEY2 = 1U,
    MUXED_ADC_CHKEY3 = 2U,
    MUXED_ADC_CHKEY4 = 3U,
    MUXED_ADC_CHKEY5 = 4U,
    MUXED_ADC_CHKEY6 = 5U,
    MUXED_ADC_CHKEY7 = 6U,
    MUXED_ADC_CHKEY8 = 7U,
    ADC_VBATT_AI,
    ADC_DIAG_WASH_LEFT,
    ADC_DIAG_WASH_RIGHT,
    ADC_DIAG_WASH_REAR,
    NUM_CHANNELS_MAX_ADC,
} E_adc_channel;

typedef enum E_adc_hw_channel_t
{
    ADC_MUX_CH = 0,
    ADC_VBATT_AI_CH,
    ADC_DIAG_WASH_LEFT_CH,
    ADC_DIAG_WASH_REAR_CH,
    ADC_DIAG_WASH_RIGHT_CH, 
} E_adc_hw_channel;

/* Global functions declaration
---------------------------------------------------------------------------*/
void adc_init(void);
void adc_step(void);
RETStatus adc_get(const E_adc_channel adc_channel, int *value);
