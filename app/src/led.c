/**
 * @file   led.c
 * @author Rubén Guijarro
 * @date   February 2022
 * @brief  Public APIs and resources for LED module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "gpio.h"
#include "spi.h"
#include "pwm.h"
#include "led.h"
#include "can_claas.h"
#include "timer.h"
#include "assert.h"
#include "application.h"

/* Constant definitions
---------------------------------------------------------------------------*/
/** @brief number of leds per button
 */
#define N_BUTTON_LEDS                    2U //(LED2 - LED1)
#define CK                      OUT_SPI_CLK
#define LE                       OUT_LED_LE
#define OE                       OUT_LED_OE
#define INVALID_REFRESH_OP_INC_STEP      5U
#define MAXCOUNT_INVALID_REFRESH_OP   2000U
#define LED_BRIGHT_PWMDUTY_DEFAULT     MAX_PWM_VALUE //100% of brightness
#define APP_LED_ERR_DETECT_EVERY10SEC  120U //Number of cycles to match the 10s delay between error detections
#define LED_PERIOD_REFRESH              80U /* in ms */

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(L_led);
BUILD_ASSERT(MAXCOUNT_INVALID_REFRESH_OP+INVALID_REFRESH_OP_INC_STEP < 0xFFFF, "Invalid led cfg");


/* Project specific types
---------------------------------------------------------------------------*/
typedef struct button_ledstr_t
{
    uint32_t timestamp;     ///< last refreshed, TBD
    uint8_t lampid;         ///< This button lamp id
    E_Led_state colour;     ///< led color or OFF
    E_LedFailureSt error;  ///< type of error
}button_ledstr;

typedef struct ledstr_t
{
    bool state;                 ///< this led position
    bool disable_led_on_fail;   ///< when true and led fail is detected output is switched off
}ledstr;

/** @struct ledcfg_errdetectfeature
 *  @brief This structure stores parameters for the periodic activation
 *  of the error detection feature.
 *  @var ledcfg_errdetectfeature::errdetect_en
 *  Member 'errdetect_en' true if feature is enabled
 *  @var ledcfg_errdetectfeature::errdetect_seq_bperiod_th
 *  Member 'errdetect_seq_bperiod_th' this is a "threshold" meaning the number of
 *  normal sequences in between a error detection+normal sequence
 *  @var ledcfg_errdetectfeature::errdetect_counter
 *  Member 'errdetect_counter' counter of consecutive normal sequences
 *  @var ledcfg_errdetectfeature::errdetect_trigger_bff
 *  Member 'errdetect_trigger_bff' bit=1 means individual led check enabled
 *  @var ledcfg_errdetectfeature::errdetect_failure_bff
 *  Member 'errdetect_failure_bff' bit=0 means individual led in fail state
 */
typedef struct ledcfg_errdetectfeature_t
{
    bool errdetect_en;
    uint16_t errdetect_seq_bperiod_th;
    uint32_t errdetect_counter;
    uint8_t errdetect_trigger_bff[LED_MAX/8];
    uint8_t errdetect_failure_bff[LED_MAX/8];
}ledcfg_errdetectfeature;

typedef struct seq_elem_t {
    E_ctrlgpio_id signal;
    bool value;
}seq_elem;

/* Local variables
-----------------------------------------------------------------------------*/
static uint32_t cnt_run = 0U;
static uint8_t led_driver_buffer[LED_MAX/8] = {0};
static uint8_t led_driver_bffaux[LED_MAX/8] = {0};
/* Internal struct holding led value and associated data*/
static ledstr LED_internal[LED_MAX] = {0};

/* Internal enum holding led failure status*/
static E_LedFailureSt LED_failstatus_internal[LED_MAX] = {0};
static button_ledstr LED_button_internal[WiperCtrlSelMax] = {0};

/* Internal struct holding parameters for led error detection */
static ledcfg_errdetectfeature LED_cfgerrdetect = {0};

/* Internal variable for bright selection */
static uint16_t led_bright_pwmduty = LED_BRIGHT_PWMDUTY_DEFAULT;

/* Local functions
-----------------------------------------------------------------------------*/
static RETStatus led_update_driver_buffer(uint8_t *bff)
{
    RETStatus status = STATUS_OK;

    gpio_switch_pwm_pb4_to_gpio_output();
    gpio_set( OUT_LED_OE, 1U);

    status = spi_led_step(bff, NULL, sizeof(led_driver_buffer));

    gpio_set( OUT_LED_LE, 1U);
    gpio_set( OUT_LED_LE, 0U);

    gpio_set( OUT_LED_OE, 0U);
    gpio_switch_gpio_output_to_pwm_pb4();
    pwm_set(led_bright_pwmduty);

    return (status);
}

static RETStatus led_refresh(void)
{
    LOG_HEXDUMP_DBG(led_driver_buffer, sizeof(led_driver_buffer), "Buffer contents ");

    return led_update_driver_buffer(led_driver_buffer);
}

static void led_gpio_seq_enter_checkleds_feature(void)
{
    seq_elem vseq_enter_c[17U] = { {CK, 0}, {LE, 0}, {OE, 1}, {CK, 1},
        {CK, 0}, {OE, 0}, {CK, 1}, {CK, 0}, {OE, 1}, {CK, 1}, {CK, 0},
        {LE, 1}, {CK, 1}, {CK, 0}, {LE, 0}, {CK, 1}, {CK, 0} };

    for (uint8_t i = 0; i < ARRAY_SIZE(vseq_enter_c); i++)
        gpio_set(vseq_enter_c[i].signal, vseq_enter_c[i].value);
}

static void led_gpio_seq_enabledetect_checkleds_feature(void)
{
    gpio_set(OUT_LED_LE, 1U);
    gpio_set(OUT_LED_LE, 0U);

    gpio_set(OUT_LED_OE, 0U);

    for (int i = 0; i < 4U; i++) {
        gpio_set(OUT_SPI_CLK, 0U);
        gpio_set(OUT_SPI_CLK, 1U);
    }

    gpio_set(OUT_LED_OE, 1U);

    gpio_set(OUT_SPI_CLK, 0U);
    gpio_set(OUT_SPI_CLK, 1U);
    gpio_set(OUT_SPI_CLK, 0U);
}

static void led_gpio_seq_resume_checkleds_feature(void)
{
    /** TODO: check if the first one should be {OE,1} instead of {LE,1} */
    seq_elem vseq_resum_c[14U] = { {LE, 1}, {LE, 0}, {CK, 1}, {CK, 0},
        {OE, 0}, {CK, 1}, {CK, 0}, {OE, 1}, {CK, 1}, {CK, 0}, {CK, 1},
        {CK, 0}, {CK, 1}, {CK, 0} };

    for (uint8_t i = 0; i < ARRAY_SIZE(vseq_resum_c); i++)
        gpio_set(vseq_resum_c[i].signal, vseq_resum_c[i].value);
}

static void led_refresh_failstatus_internal(const RETStatus valid_detection)
{
    S_ASSERT((valid_detection >= STATUS_INTEGRITY_ERROR) && (valid_detection <= STATUS_OK), ASSERT_BOUNDS, "Invalid parameter");

    if (valid_detection == STATUS_OK) {
        
        /* Goes through each bit in the received spi buffer, when the bit is zero this means that the associated led is failing */
        for (int i = 0; i < ARRAY_SIZE(LED_cfgerrdetect.errdetect_failure_bff); i++)
        {
            uint8_t auxFailureDet = LED_cfgerrdetect.errdetect_trigger_bff[i] ^ LED_cfgerrdetect.errdetect_failure_bff[i];

            for (int bit_position = 0; bit_position < 8; bit_position++)
            {
                E_LedFailureSt led_st = (auxFailureDet & BIT(bit_position)) ? LED_FAILING : LED_FAIL_ST_UNKNOWN;

                /* Regarding whether detection was triggered for this LED, let's determine if it could be LED_NOT_FAILING  */  
                bool wasDetectionTriggered = (LED_cfgerrdetect.errdetect_trigger_bff[i] & BIT(bit_position));

                led_st = ((led_st != LED_FAILING) && (wasDetectionTriggered == true))? LED_NOT_FAILING : led_st;

                LED_failstatus_internal[LED_MAX - 8*(i+1) + bit_position] = led_st;
            }
        }
        
    } else {
        
        memset(LED_failstatus_internal, 0x00, LED_MAX*sizeof(E_LedFailureSt)); /* LED_FAIL_ST_UNKNOWN */
    }        
}

static RETStatus led_launch_st_error_detection(void)
{
    RETStatus status = STATUS_OK;

    uint8_t led_bffTx[sizeof(LED_cfgerrdetect.errdetect_trigger_bff)] = {0};

    led_update_driver_buffer(led_bffTx);

    gpio_switch_pwm_pb4_to_gpio_output();

    /* Error Enter sequence */
    gpio_switch_spi_pb3_to_gpio_output();
    led_gpio_seq_enter_checkleds_feature();
    gpio_switch_gpio_output_to_spi_pb3();

    /* Sequence Post Error Enter */
    status = spi_led_step(LED_cfgerrdetect.errdetect_trigger_bff, NULL, sizeof(LED_cfgerrdetect.errdetect_trigger_bff));

    if (status != STATUS_ERROR)
    {
        /* Error Enable detection sequence. length pulse OE Low = 8us */
        gpio_switch_spi_pb3_to_gpio_output();
        led_gpio_seq_enabledetect_checkleds_feature();
        gpio_switch_gpio_output_to_spi_pb3();

        /* Error Detection sequence: LED_cfgerrdetect.errdetect_failure_bff'll store the faults as zero bits  */
        status = spi_led_step(led_bffTx, LED_cfgerrdetect.errdetect_failure_bff, sizeof(LED_cfgerrdetect.errdetect_trigger_bff));
    }

    if (status != STATUS_ERROR)  /* spi error detection transmission was successfully */
    {
        /* Error Resume sequence */
        gpio_switch_spi_pb3_to_gpio_output();
        led_gpio_seq_resume_checkleds_feature();
        gpio_switch_gpio_output_to_spi_pb3();

        gpio_switch_gpio_output_to_pwm_pb4();

        led_update_driver_buffer(led_driver_buffer);
    }

    led_refresh_failstatus_internal(status);  /* Updates the detected failures according led number */

    return (status);
}

static RETStatus led_update_from_buttons(const WiperCtrlSel_str button)
{
    RETStatus status = STATUS_OK;

    button_ledstr *selected_button = &LED_button_internal[button];
    const int base_button_led = button * N_BUTTON_LEDS;
    ledstr *base_led = &LED_internal[base_button_led];
    switch(selected_button->colour)
    {
        case OFF:
            /* Set state to false for all associated leds */
            for(int i = 0; i <= N_BUTTON_LEDS; i++)
            {
                base_led[i].state = false;
                led_set_status(base_button_led + i, false);
            }
            break;
        case LOWER_ON:
            base_led[1].state = true;
            led_set_status(base_button_led+1, true);
            base_led[0].state = false;
            led_set_status(base_button_led, false);
            break;
        case HIGHER_ON:
            base_led[1].state = false;
            led_set_status(base_button_led+1, false);
            base_led[0].state = true;
            led_set_status(base_button_led, true);
            break;
        case BOTH_ON:
            for(int i = 0; i <= N_BUTTON_LEDS; i++)
            {
                base_led[i].state = true;
                led_set_status(base_button_led + i, true);
            }
            break;
        default:
            LOG_ERR("Parsing invalid led button %d colour %d", button, selected_button->colour);
            status = STATUS_ERROR;
            break;
    }

    return status;
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function for LED module
 */
void led_init(void)
{
    MODULE_SINGLE_INIT;

    memset(led_driver_buffer, 0U, sizeof(led_driver_buffer));

    spi_led_init();
    pwm_set(led_bright_pwmduty);
    gpio_set(OUT_LED_LE, 0U);
    
    //Turn on white LEDs
    for (E_Led i=LED_W_1_2; i<=LED_W_7_8; i++)
        led_set_status(i, true);

    led_enable_detect_feature(APP_LED_ERR_DETECT_EVERY10SEC);
}

/** @brief set led state into internal data structures
 * @param[in] ledn led number/identifier which state to change
 * @param[in] state request led state
 * @return STATUS_OK if successful
 * 
 * This function adjust the internal data structures and prepares them for
 * holding the led state information and for transferring data to the led drivers though SPI
 * This function encodes the led state into the buffer feeding the led driver though SPI
 * Examples (all numbers zero based, except those on led names):
 * SPI buffer [5]/led driver 0: (b7) LED2_G LED2_R LED2_O LED2_W LED1_G LED1_R LED1_O LED1_W (b0)
 * SPI buffer [0]/led driver 5: (b7) LED12_G LED12_R LED12_O LED12_W LED11_G LED11_R LED11_O LED11_W (b0)
 * LED12_G on (47) => led driver 5, bit 7: 80 00 00 00 00 00
 * LED1_W on (0) => led driver 0, bit 0: 00 00 00 00 00 01
*/
RETStatus led_set_status(const E_Led ledn, const bool state)
{
    S_ASSERT(ledn < LED_MAX, ASSERT_INTERNAL, "Invalid led number");

    RETStatus retvalue = STATUS_OK;

    const uint8_t led_driver_n = (LED_MAX - 1 - ledn) / 8;
    const uint8_t led_driver_offset = ledn % 8;

    LOG_DBG("Setting led %d (bufpos %d-%d) to %d", ledn, led_driver_n, led_driver_offset, state);

    if ((state == true) && ((led_get_error_status_autotest(ledn) != LED_FAILING)||(LED_internal[ledn].disable_led_on_fail == false)))
    {
        WRITE_BIT(led_driver_buffer[led_driver_n], led_driver_offset, 1);
        LED_internal[ledn].state = true; //ON
    }
    else /* state == false */ /* led_get_error_status(ledn) == LED_FAILING */
    {
        WRITE_BIT(led_driver_buffer[led_driver_n], led_driver_offset, 0);
        LED_internal[ledn].state = false; //OFF
        retvalue = (state == true) ? STATUS_ERROR : retvalue;
    }

    return retvalue;
}

/** @brief Handle button asociated leds: on/off, color and blink
 * @param[in] button operates on leds associated to the specified button number/identifier
 * @param[in] state request led state
 * @return STATUS_OK if successful
 * 
 * For @ref state OFF, all leds asociated with button are powered off
 * For @ref state ON, backlight led (white) asociated with button is powered on
 * For @ref state ORANGE/RED/GREEN, the specified color led is powered on, backlight is powered on
 *   as well. If another color led was already powered on, it is powered off in favour of tne
 *   newly specified one
 * For @ref state BLINK_SLOW/BLINK_FAST, the color led which is already powered on enters blink status. If no color has already been powered on this call has no effect.
 */
RETStatus led_button_set(const WiperCtrlSel_str button, const E_Led_state state)
{
    S_ASSERT( button < WiperCtrlSelMax, ASSERT_BOUNDS, "Invalid button");
    S_ASSERT( (state >= OFF) && (state < STATE_MAX), ASSERT_BOUNDS, "Invalid led state" );

    RETStatus status = STATUS_OK;

    button_ledstr *selected_button = &LED_button_internal[button];
    switch(state)
    {
        case OFF:
            selected_button->colour = OFF;
            selected_button->error = LED_NOT_FAILING;
            break;
        /** LOWER_ON implies turning off the higher LED if it was on
         *  HIGHER_ON implies turning off the lower LED if it was on
        */
        case LOWER_ON:
        case HIGHER_ON:
        case BOTH_ON:
            selected_button->colour = state;
            selected_button->error = LED_NOT_FAILING;
            break;
        default:
            LOG_ERR("Requesting invalid led button state %d", state);
            selected_button->error = LED_ERROR;
            status = STATUS_ERROR;
            break;
    }

    if(status == STATUS_OK)
    {
        selected_button->timestamp = k_uptime_get();
        led_update_from_buttons(button);
    }

    return status;
}

/** @brief Main function for LED step by step
 */
void nled_step(void)  /* This step is called 124 times by second */
{
    if (abs(timer_get_totalTicks(E_TIMER_2) - cnt_run) >= LED_PERIOD_REFRESH)
    {
        cnt_run = timer_get_totalTicks(E_TIMER_2);
        /* led_step is called ~12.5 times by second, every 80ms*/
        led_step();
    }
}

/** @brief Main function for LED management
 */
void led_step(void)
{
    RETStatus retval = STATUS_OK;
    static uint16_t invalid_refresh_op_counter = 0U;
    
    led_set_status(LED_1_D, get_FL_Park_09());
    led_set_status(LED_1_U, get_FL_Park_10());
    led_set_status(LED_2_D, get_FL_Auto_07());
    led_set_status(LED_2_U, get_FL_Auto_08());
    led_set_status(LED_4_D, get_FL_Left_01());
    led_set_status(LED_4_U, get_FL_Left_02());
    led_set_status(LED_6_D, get_FL_Rear_03());
    led_set_status(LED_6_U, get_FL_Rear_04());
    led_set_status(LED_8_D, get_FL_Right_05());
    led_set_status(LED_8_U, get_FL_Right_06());
    // LOG_DBG("\t LEDs status : PP_U %d, PP_D %d, AM_U %d, AM_D %d, LW_U %d, LW_D %d, BW_U %d, BW_d %d, RW_U %d, RW_D %d \n", get_FL_Park_10(), get_FL_Park_09(),
        // get_FL_Auto_08(), get_FL_Auto_07(), get_FL_Left_02(), get_FL_Left_01(), get_FL_Rear_04(), get_FL_Rear_03(), get_FL_Right_06(), get_FL_Right_05());

    /* Preserve a copy of the original led buffer */
    memcpy(led_driver_bffaux, led_driver_buffer, sizeof(led_driver_buffer));

    if (LED_cfgerrdetect.errdetect_en == true)
    {
        /* In case the error detection is enabled, there will be to types of refresh sequences, led_refresh() is */
        /* the short one with a single spi transmission to update the registers for driving the leds. */
        /* The other is led_refresh_st_error_detect_feature(). It's a refresh sequence more complex and starts with a special  */
        /* sequence for entering in test mode, followed by a spi transmission for selecting the set of leds we want to test     */
        /* (0xFF means we want to test all leds) then a second special sequence with a low pulse in the signal LE_OE (related  */
        /* with the time the leds will be activated for the testing). This continues with the second spi transmission where the  */
        /* rx buffer contains the result of the test, a third special sequence for resuming the test mode, and a third spi trans-*/
        /* -mission which updates the registers for driving the leds  */

        LED_cfgerrdetect.errdetect_counter++;

        /* Each time led_step() is called errdetect_counter is increased, in case the counter is below the threshold the short */
        /* sequence led_refresh() is being executed. If errdetect_counter reaches the threshold (errdetect_seq_bperiod_th), instead */
        /* of calling the short refresh sequence, the led_refresh_st_error_detect_feature() sequence with embedded error detection */
        /* feature is executed */
        if (LED_cfgerrdetect.errdetect_counter == LED_cfgerrdetect.errdetect_seq_bperiod_th) 
        {
            /* During the cyclic activation of the leds, the error detection will be launched only for the leds that are lighting */
            retval = led_refresh_st_error_detect_feature(led_driver_buffer);

            LED_cfgerrdetect.errdetect_counter = 0U;
        }
        else
            retval = led_refresh();
    }
    else
        retval = led_refresh();

    /* Restore led_driver_buffer to the original value */
    memcpy(led_driver_buffer, led_driver_bffaux, sizeof(led_driver_buffer));

    /* Policy for repetitive led-refreshing failures */
    if (invalid_refresh_op_counter < MAXCOUNT_INVALID_REFRESH_OP)
        invalid_refresh_op_counter += (retval != STATUS_OK) ? INVALID_REFRESH_OP_INC_STEP : 0U;

    if (invalid_refresh_op_counter && (retval == STATUS_OK))
        invalid_refresh_op_counter--;

    if (invalid_refresh_op_counter > MAXCOUNT_INVALID_REFRESH_OP/2)
        LOG_DBG("\n>>> Several invalid led-refresh op detected! [%d]", invalid_refresh_op_counter);
}

/** @brief Enable LED diagnostic
 */
void led_enable_detect_feature(const uint16_t errdetect_seq_burstperiod)
{
    LED_cfgerrdetect.errdetect_seq_bperiod_th = errdetect_seq_burstperiod;
    LED_cfgerrdetect.errdetect_counter = 0U;
    LED_cfgerrdetect.errdetect_en = (errdetect_seq_burstperiod > 0U) ? true : false;
}

/** @brief Refresh LED driver
 */
RETStatus led_refresh_st_error_detect_feature(const uint8_t *trigger_bff)
{
    S_ASSERT(trigger_bff != NULL, ASSERT_BOUNDS, "Invalid pointer");

    RETStatus status = STATUS_OK;
    
    memset(LED_cfgerrdetect.errdetect_trigger_bff, 0x00, sizeof(LED_cfgerrdetect.errdetect_trigger_bff));

    for (int i = 0; i < sizeof(LED_cfgerrdetect.errdetect_trigger_bff); i++)
        LED_cfgerrdetect.errdetect_trigger_bff[i] |= trigger_bff[i]; // Select white leds when these are activated

    status = led_launch_st_error_detection();

    return (status);
}

/** @brief Error detection
 */
E_LedFailureSt led_get_error_status_autotest(const E_Led ledn)
{
    S_ASSERT(ledn < LED_MAX, ASSERT_INTERNAL, "Invalid led number");

    return (LED_failstatus_internal[ledn]);
}

E_LedFailureSt led_get_error_status_buttons(const WiperCtrlSel_str button)
{
    S_ASSERT(button < WiperCtrlSelMax, ASSERT_INTERNAL, "Invalid button number");

    return LED_button_internal[button].error;
}

/** @brief Set disable output
 */
void led_disable_output_on_fail(const E_Led ledn)
{
    S_ASSERT(ledn < LED_MAX, ASSERT_INTERNAL, "Invalid led number");

    LED_internal[ledn].disable_led_on_fail = true;
}

/** @brief Set enable output
 */
void led_enable_output_on_fail(const E_Led ledn)
{
    S_ASSERT(ledn < LED_MAX, ASSERT_INTERNAL, "Invalid led number");

    LED_internal[ledn].disable_led_on_fail = false;
}

/** @brief converts lampid value received by CAN CLAAS frame to a Led_state
 */
E_Led_state led_val_ledstate(uint8_t value)
{
    E_Led_state rv = OFF;
    const static E_Led_state value_to_state[] =
    {
        [0x00] = OFF, [0x01] = LOWER_ON, [0x02] = HIGHER_ON, [0x03] = BOTH_ON,
    };

    if(value < ARRAY_SIZE(value_to_state))
        rv = value_to_state[value];

    return rv;
}

/** @brief Getter the time stamp for each button or led
 */
void led_get_time_stamp(const WiperCtrlSel_str button, uint64_t *time_stamp)
{
    *time_stamp = LED_button_internal[button].timestamp;
}

void led_get_last_colour(const WiperCtrlSel_str button, E_Led_state *Led_colour)
{
    *Led_colour = LED_button_internal[button].colour;
}

/** @brief Getter LED brightness
 */
uint16_t led_get_bright_value(void)
{
    return led_bright_pwmduty;
}

/** @brief Setter LED brightness
 */
void led_set_bright_value(uint16_t bval)
{
    led_bright_pwmduty = bval;
}
