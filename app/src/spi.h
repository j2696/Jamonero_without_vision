#pragma once
/**
 * @file   spi.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for SPI module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
RETStatus spi_led_step(const uint8_t *txdata, uint8_t *rxdata, const uint16_t length);
RETStatus spi_tle_step(const uint8_t *txdata, uint8_t *rxdata, const uint16_t length);
void spi_led_init(void);
void spi_tle_init(void);
