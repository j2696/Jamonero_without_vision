#pragma once
/**
 * @file   uds.h
 * @author Abel Tarragó
 * @date   January 2022
 * @brief  Public APIs and resources for UDS module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
void uds_init(void);
void uds_update_shared_memory(void);
void uds_update(void);
RETStatus send_negative_response_code (uint8_t serviceId, uint8_t errorCode);
RETStatus send_positive_response_code(void);
RETStatus send_positive_response_code_for_buttons_can_id(uint32_t can_id);
RETStatus send_positive_response_code_for_buttons_info(uint8_t info);
RETStatus send_positive_response_code_for_configuration(uint16_t info);

