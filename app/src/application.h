#pragma once
/**
 * @file   application.h
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for APP module.
 */

/* Project specific types
---------------------------------------------------------------------------*/

/* Global functions declaration
---------------------------------------------------------------------------*/
void APP_Initialize(void);
void APP_Update(void);
