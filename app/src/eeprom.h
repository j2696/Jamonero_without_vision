#pragma once
/**
 * @file   eeprom.h
 * @author Rubén Guijarro
 * @date   April 2022
 * @brief  Public APIs and resources for eeprom module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* project specific types
---------------------------------------------------------------------------*/
/** @brief _eeprom_id enum
 * Define the index of parameters in the EEPROM
 */
typedef enum eeprom_t
{
    ECU_SN,
    ECU_HW_PN,
    ECU_HW_VERSION,
    ECU_MainPartNumber,
    DTC_ID_1, DTC_ID_2, DTC_ID_3, DTC_ID_4, DTC_ID_5, DTC_ID_6, DTC_ID_7, DTC_ID_8, DTC_ID_9, DTC_ID_10,
    DTC_OC_1, DTC_OC_2, DTC_OC_3, DTC_OC_4, DTC_OC_5, DTC_OC_6, DTC_OC_7, DTC_OC_8, DTC_OC_9, DTC_OC_10,
    DTC_WH_1, DTC_WH_2, DTC_WH_3, DTC_WH_4, DTC_WH_5, DTC_WH_6, DTC_WH_7, DTC_WH_8, DTC_WH_9, DTC_WH_10,
    RESET_TIME,
    MAX_PUMP_TIME,
    PAUSE_PUMP_RATIO,
    PRESENT_INTERVAL_TIME,
    MIN_INTERVAL_TIME,
    MAX_INTERVAL_TIME,
    WIPER_RUN_TIME,
    WIPER_PULSE_TIME,
    C_OUTPUT_ENABLE,
    WASHER_OL,
    WASHER_SCGND,
    PWM_MAX_PERCENTAGE,
    PWM_MIN_PERCENTAGE,
    ECU_CONFIG1,
    ECU_CONFIG2,
    ECU_CONFIG3,
    CRC_16,
    EEPROM_ID_MAX,
} eeprom_id;

/* Global functions declaration
---------------------------------------------------------------------------*/
void eeprom_init(void);
void eeprom_step(void);
RETStatus eeprom_get(eeprom_id eeprom_id, uint8_t *data);
RETStatus eeprom_read(eeprom_id eeprom_parameter, uint8_t *data);
RETStatus eeprom_write(eeprom_id eeprom_parameter, uint8_t *data);
RETStatus eeprom_erase(eeprom_id eeprom_parameter);
RETStatus eeprom_write_nowait(eeprom_id eeprom_parameter, uint8_t *data);
RETStatus eeprom_erase_nowait(eeprom_id eeprom_parameter);
bool eeprom_nowait_command_completed(void);
bool get_erase_eeprom(void);
void set_erase_eeprom(bool value);
