#pragma once
/**
 * @file   dgn.h
 * @author Abel Tarragó
 * @date   Aprli 2022
 * @brief  Public APIs and resources for Diagnostic module.
 */

/* header of project specific types
-----------------------------------------------------------------------------*/
#include "eeprom.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define ALL_DTC 0xFFFFFF

/* project specific types
---------------------------------------------------------------------------*/
/** @brief DTC_Handling enum
 * Define the status of the DTC
 */
typedef enum DTC_handling_t
{
    TRANSMIT, 
    STORAGE, 
    TRANSMIT_AND_STORAGE,
    MAX_STATE,
} DTC_Handling;

/** @brief DTC_ID_t enum
 * Define the index of the DTC in the register
 */
typedef enum E_DTC_ID_t
{
    WCM_DigitalOutLeftA_ScGnd,
    WCM_DigitalOutLeftA_OL,
    WCM_DigitalOutLeftB_ScGnd,
    WCM_DigitalOutLeftB_OL,
    WCM_DigitalOutLeftC_ScGnd,
    WCM_DigitalOutLeftC_OL,
    WCM_DigitalOutRearA_ScGnd,
    WCM_DigitalOutRearA_OL,
    WCM_DigitalOutRearB_ScGnd,
    WCM_DigitalOutRearB_OL,
    WCM_DigitalOutRearC_ScGnd,
    WCM_DigitalOutRearC_OL,
    WCM_DigitalOutRightA_ScGnd,
    WCM_DigitalOutRightA_OL,
    WCM_DigitalOutRightB_ScGnd,
    WCM_DigitalOutRightB_OL,
    WCM_DigitalOutRightC_ScGnd,
    WCM_DigitalOutRightC_OL,
    WCM_SolidStateLeftPump_ScGnd,
    WCM_SolidStateLeftPump_OL,
    WCM_SolidStateRearPump_ScGnd,
    WCM_SolidStateRearPump_OL,
    WCM_SolidStateRightPump_ScGnd,
    WCM_SolidStateRightPump_OL,
    E_MAX_DTC,
} E_DTC_ID;

/** @brief dtcstr struct
 * DTC struct definition
 */
typedef struct dtcstr_t
{
    bool sent;
    bool active;
    bool dtc_in_eeprom;
    bool id_updated;
    bool occurrence_updated;
    bool wh_updated;
    bool clear;
    bool failed;
    int stepUp;
    int stepDown;
    uint16_t ID;
    uint8_t number;
    uint8_t status;
    int occurrence;
    signed int passed;
    signed int counter;
    signed int confirmed;
    DTC_Handling state;
    uint32_t firstWorkingHour;
    uint32_t lastWorkingHour;
    uint8_t position_in_eeprom_struct;
    uint64_t time_spent_ack;
    uint64_t time_stamp_ack;
}dtcstr;

/* Global functions declaration
---------------------------------------------------------------------------*/
void dgn_init(void);
void dgn_step(void);
bool dgn_status(void);
void dgn_get_number_of_dtc(uint8_t mask, uint8_t *data, uint8_t *dlc);
void dgn_clear_dtc(uint32_t dtc_id);
void dgn_get_status_of_dtc(uint8_t mask, uint8_t *data, uint8_t *dlc);
void dgn_get_snapshoot_data_of_dtc(uint8_t *data, uint8_t *dlc, uint32_t dtc_id);
void dgn_get_supported_dtc(uint8_t *data, uint16_t *dlc);
void dgn_get_extended_data_of_dtc(uint8_t *data, uint8_t *dlc, uint32_t dtc_id);
void dgn_set_Alarm_Acknowledgement(void);
void dgn_set_alarm_monitoring_status(uint8_t *data);
