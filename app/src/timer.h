#pragma once
/**
 * @file   timer.h
 * @author Rubén Guijarro
 * @date   January 2022
 * @brief  Public APIs and resources for Timer module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Global functions declaration
---------------------------------------------------------------------------*/
/** @brief Initialization function
 */
void timer_init(void);


typedef enum E_timer_inst_t
{
    E_TIMER_1 = 0,
    E_TIMER_2,
    E_TIMER_MAX
} E_timer_inst;

typedef enum E_timer_status_t
{
    TIMER_COUNT_EXPIRED = 0,
    TIMER_COUNT_ONGOING,
    E_TIMER_STATUS_MAX
} E_timer_status;

void timer_start_count(const E_timer_inst timer_inst, uint32_t time_us);

/** @brief get total ticks of the requested timer
 *  @param timer_inst: timer structure to check
 *  @return: value of ticks
 */
uint32_t timer_get_totalTicks(const E_timer_inst timer_inst);
