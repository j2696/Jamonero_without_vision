/**
 * @file   config.c
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for CONFIG module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"
#include "eeprom.h"
#include "keyb.h"
#include "application.h"
#include "watchdog.h"

/* constant definitions
---------------------------------------------------------------------------*/
#define ECU_CONFIGURED                    1U
#define MAX_RESET_TIME                 2000U
#define MAX_MAX_PUMP_TIME             60000U
#define MAX_PAUSE_PUMP_RATIO           1000U
#define MAX_MAX_INTERVAL_TIME         30000U
#define MAX_WIPER_RUN_TIME             5000U
#define MAX_WIPER_PULSE_TIME           2500U
#define MAX_C_OUTPUT_ENABLE               1U
#define MAX_WASHER_OL                  3200U
#define MAX_WASHER_SCGND               3200U
#define MAX_PWM_PERCENTAGE              100U
#define DEFAULT_RESET_TIME              500U
#define DEFAULT_MAX_PUMP_TIME          3000U
#define DEFAULT_PAUSE_PUMP_RATIO        150U
#define DEFAULT_PRESENT_INTERVAL_TIME  5000U
#define DEFAULT_MIN_INTERVAL_TIME         0U
#define DEFAULT_MAX_INTERVAL_TIME     10000U
#define DEFAULT_WIPER_RUN_TIME         2400U
#define DEFAULT_WIPER_PULSE_TIME        800U
#define DEFAULT_C_OUTPUT_ENABLE           0U
#define DEFAULT_WASHER_OL               250U
#define DEFAULT_WASHER_SCGND           2500U
#define DEFAULT_MAX_PWM_PERCENTAGE      100U
#define DEFAULT_MIN_PWM_PERCENTAGE       50U
#define Intermittence_Min_CAN_ID        0x01
#define Intermittence_Max_CAN_ID        0x02
#define Intermittence_SetVal_CAN_ID     0x03
#define WORKING_HOURS_FACTOR           3600U

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(config);

/* local defined variables
-----------------------------------------------------------------------------*/
static uint16_t reset_time; /* in ms */
static uint16_t max_pump_time;
static uint16_t pause_pump_ratio;
static uint16_t present_interval_time, min_interval_time = DEFAULT_MIN_INTERVAL_TIME, max_interval_time = DEFAULT_MAX_INTERVAL_TIME;
static uint32_t wiper_run_time;
static uint32_t wiper_pulse_time;
static uint8_t c_output_enable;
static uint16_t washer_ol;
static uint16_t washer_scgnd;
static uint32_t working_hour;
static uint8_t ecu_config_flag1;
static uint8_t ecu_config_flag2;
static uint8_t ecu_config_flag3;
static bool ecu_configured = false;
static uint8_t ecu_hw_pn[4];
static uint8_t ecu_sn[4];
static uint8_t ecu_hw_version[2];
static uint8_t main_pn[4];
static uint8_t pwm_max_percentage;
static uint8_t pwm_min_percentage;

/* Global variables
---------------------------------------------------------------------------*/
STATUS status_str;

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialization function */
void config_init(void)
{
    uint8_t data[8];
            
    eeprom_read(ECU_CONFIG1, data);
    ecu_config_flag1 = data[0];

    eeprom_read(ECU_CONFIG2, data);
    ecu_config_flag2 = data[0];

    eeprom_read(ECU_CONFIG3, data);
    ecu_config_flag3 = data[0];

    if((ecu_config_flag1 == ECU_CONFIGURED) || (ecu_config_flag1 == ECU_CONFIGURED) || (ecu_config_flag2 == ECU_CONFIGURED))
        ecu_configured = true;
    else
        ecu_configured = false;

    if(ecu_configured){
        eeprom_read(ECU_HW_PN, data);
        memcpy(ecu_hw_pn,data,8);

        eeprom_read(ECU_SN, data);
        memcpy(ecu_sn,data,4);

        eeprom_read(ECU_HW_VERSION, data);
        memcpy(ecu_hw_version,data,2);

        //eeprom_read(ECU_MainPartNumber, data);
        //memcpy(main_pn,data,4);

        eeprom_read(RESET_TIME, data);
        if (((data[0] << 8) | data[1]) < 0U || ((data[0] << 8) | data[1]) > MAX_RESET_TIME){
            reset_time = DEFAULT_RESET_TIME;
            data[0] = reset_time >> 8;
            data[1] = reset_time;
            eeprom_write ( RESET_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid RESET_TIME %d", (data[0] << 8) | data[1]);
        }else
            reset_time = (data[0] << 8) | data[1];

        eeprom_read(MAX_PUMP_TIME, data);
        if (((data[0] << 8) | data[1]) < 0 || ((data[0] << 8) | data[1]) > MAX_MAX_PUMP_TIME) {
            max_pump_time = DEFAULT_MAX_PUMP_TIME;
            data[0] = max_pump_time >> 8;
            data[1] = max_pump_time;
            eeprom_write ( MAX_PUMP_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid MAX_PUMP_TIME %d", (data[0] << 8) | data[1]);
        }else
            max_pump_time = (data[0] << 8) | data[1];

        eeprom_read(PAUSE_PUMP_RATIO, data);
        if (((data[0] << 8) | data[1]) < 0U || ((data[0] << 8) | data[1]) > MAX_PAUSE_PUMP_RATIO) {
            pause_pump_ratio = DEFAULT_PAUSE_PUMP_RATIO;
            data[0] = pause_pump_ratio >> 8;
            data[1] = pause_pump_ratio;
            eeprom_write ( PAUSE_PUMP_RATIO , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid PAUSE_PUMP_RATIO %d", (data[0] << 8) | data[1]);
        }else
            pause_pump_ratio = (data[0] << 8) | data[1];

        eeprom_read(MAX_INTERVAL_TIME, data);
        if (((data[0] << 8) | data[1]) < 0U || (((data[0] << 8) | data[1]) > MAX_MAX_INTERVAL_TIME) || (min_interval_time > ((data[0] << 8) | data[1]))){
            max_interval_time = DEFAULT_MAX_INTERVAL_TIME;
            data[0] = max_interval_time >> 8;
            data[1] = max_interval_time;
            eeprom_write (MAX_INTERVAL_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid MAX_INTERVAL_TIME %d", (data[0] << 8) | data[1]);
        }else
            max_interval_time = (data[0] << 8) | data[1];
        set_IntermittencePausingTime(max_interval_time, Intermittence_Max_CAN_ID);

        eeprom_read(PRESENT_INTERVAL_TIME, data);
        if (((data[0] << 8) | data[1]) < min_interval_time || ((data[0] << 8) | data[1]) > max_interval_time){
            present_interval_time = DEFAULT_PRESENT_INTERVAL_TIME;
            data[0] = present_interval_time >> 8;
            data[1] = present_interval_time;
            eeprom_write ( PRESENT_INTERVAL_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid PRESENT_INTERVAL_TIME %d", (data[0] << 8) | data[1]);
        }else
            present_interval_time = (data[0] << 8) | data[1];
        set_IntermittencePausingTime(present_interval_time, Intermittence_SetVal_CAN_ID);

        eeprom_read(MIN_INTERVAL_TIME, data);
        if (((data[0] << 8) | data[1]) > max_interval_time){
            min_interval_time = DEFAULT_MIN_INTERVAL_TIME;
            data[0] = min_interval_time >> 8;
            data[1] = min_interval_time;
            eeprom_write (MIN_INTERVAL_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n\n invalid MIN_INTERVAL_TIME %d\n\n", (data[0] << 8) | data[1]);
        }else
            min_interval_time = (data[0] << 8) | data[1];
        set_IntermittencePausingTime(min_interval_time, Intermittence_Min_CAN_ID);

        eeprom_read(WIPER_RUN_TIME, data);
        if ((((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]) < 0U) ||
            (((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]) > MAX_WIPER_RUN_TIME)){
            wiper_run_time = DEFAULT_WIPER_RUN_TIME;
            data[0] = wiper_run_time >> 24;
            data[1] = wiper_run_time >> 16;
            data[2] = wiper_run_time >> 8;
            data[3] = wiper_run_time;
            eeprom_write ( WIPER_RUN_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid WIPER_RUN_TIME %d",((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]));
        }else
            wiper_run_time = ((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]);

        eeprom_read(WIPER_PULSE_TIME, data);
        if ((((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]) < 0U) ||
            (((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]) > MAX_WIPER_PULSE_TIME)){
            wiper_pulse_time = DEFAULT_WIPER_PULSE_TIME;
            data[0] = wiper_pulse_time >> 24;
            data[1] = wiper_pulse_time >> 16;
            data[2] = wiper_pulse_time >> 8;
            data[3] = wiper_pulse_time;
            eeprom_write ( WIPER_PULSE_TIME , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid WIPER_PULSE_TIME %d", ((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]));
        }else
            wiper_pulse_time = ((data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3]);

        eeprom_read(C_OUTPUT_ENABLE, data);
        if (data[0] < 0 || data[0] > MAX_C_OUTPUT_ENABLE) {
            c_output_enable = DEFAULT_C_OUTPUT_ENABLE;
            data[0] = c_output_enable;
            eeprom_write ( C_OUTPUT_ENABLE , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid C_OUTPUT_ENABLE %d", data[0]);
        }else
            c_output_enable = data[0];

        eeprom_read(WASHER_OL, data);
        if (((data[0] << 8) | data[1]) < 0U || ((data[0] << 8) | data[1]) > MAX_WASHER_OL){
            washer_ol = DEFAULT_WASHER_OL;
            data[0] = washer_ol >> 8;
            data[1] = washer_ol;
            eeprom_write (WASHER_OL , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid WASHER_OL %d", (data[0] << 8) | data[1]);
        }else
            washer_ol = (data[0] << 8) | data[1];

        eeprom_read(WASHER_SCGND, data);
        if (((data[0] << 8) | data[1]) < 0U || ((data[0] << 8) | data[1]) > MAX_WASHER_SCGND){
            washer_scgnd = DEFAULT_WASHER_SCGND;
            data[0] = washer_scgnd >> 8;
            data[1] = washer_scgnd;
            eeprom_write (WASHER_SCGND , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid WASHER_SCGND %d", (data[0] << 8) | data[1]);
        }else
            washer_scgnd = (data[0] << 8) | data[1];

        eeprom_read(PWM_MAX_PERCENTAGE, data);
        pwm_max_percentage = data[0];
        LOG_DBG("\n\n PWM_MAX_PERCENTAGE %d", pwm_max_percentage);
        eeprom_read(PWM_MIN_PERCENTAGE, data);   // It is needed to read before for comparision
        pwm_min_percentage = data[0];
        LOG_DBG("\n\n PWM_MIN_PERCENTAGE %d", pwm_min_percentage);
        if (pwm_max_percentage < 0 || pwm_max_percentage > MAX_PWM_PERCENTAGE || pwm_max_percentage < pwm_min_percentage) {
            pwm_max_percentage = MAX_PWM_PERCENTAGE;
            data[0] = pwm_max_percentage;
            eeprom_write ( PWM_MAX_PERCENTAGE , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid PWM_MAX_PERCENTAGE %d", data[0]);
        }

        if (pwm_min_percentage < 0 || pwm_min_percentage > MAX_PWM_PERCENTAGE || pwm_min_percentage > pwm_max_percentage) {
            pwm_min_percentage = 0;
            data[0] = pwm_min_percentage;
            eeprom_write ( PWM_MIN_PERCENTAGE , data);
            k_msleep(5);
            wdt_step();
            LOG_DBG("\n invalid PWM_MIN_PERCENTAGE %d", data[0]);
        }

    }else{
        ecu_configured = true;
        LOG_DBG("\n ECU NOT CONFIGURED %d", data[0]);
        data[0] = ECU_CONFIGURED;
        eeprom_write (ECU_CONFIG1 , data);
        k_msleep(5);
        wdt_step();
        eeprom_write (ECU_CONFIG2 , data);
        k_msleep(5);
        wdt_step();
        eeprom_write (ECU_CONFIG3 , data);
        k_msleep(5);
        wdt_step();

        for(int i = 0; i < 8 ; i++)
            data[i] = 0x00;

        eeprom_write(ECU_HW_PN, data);
        memcpy(ecu_hw_pn,data,4);
        k_msleep(5);
        wdt_step();

        eeprom_write(ECU_SN, data);
        memcpy(ecu_sn,data,4);
        k_msleep(5);
        wdt_step();

        eeprom_write(ECU_HW_VERSION, data);
        memcpy(ecu_hw_version,data,2);
        k_msleep(5);
        wdt_step();

        eeprom_write(ECU_MainPartNumber, data);
        memcpy(main_pn,data,4);
        k_msleep(5);
        wdt_step();

        reset_time = DEFAULT_RESET_TIME;
        data[0] = reset_time >> 8;
        data[1] = reset_time;
        eeprom_write (RESET_TIME , data);
        k_msleep(5);
        wdt_step();

        max_pump_time = DEFAULT_MAX_PUMP_TIME;
        data[0] = max_pump_time >> 8;
        data[1] = max_pump_time;
        eeprom_write (MAX_PUMP_TIME , data);
        k_msleep(5);
        wdt_step();

        pause_pump_ratio = DEFAULT_PAUSE_PUMP_RATIO;
        data[0] = pause_pump_ratio >> 8;
        data[1] = pause_pump_ratio;
        eeprom_write (PAUSE_PUMP_RATIO , data);
        k_msleep(5);
        wdt_step();

        present_interval_time = DEFAULT_PRESENT_INTERVAL_TIME;
        data[0] = present_interval_time >> 8;
        data[1] = present_interval_time;
        eeprom_write (PRESENT_INTERVAL_TIME , data);
        k_msleep(5);
        wdt_step();

        min_interval_time = DEFAULT_MIN_INTERVAL_TIME;
        data[0] = min_interval_time >> 8;
        data[1] = min_interval_time;
        eeprom_write (MIN_INTERVAL_TIME , data);
        k_msleep(5);
        wdt_step();

        max_interval_time = DEFAULT_MAX_INTERVAL_TIME;
        data[0] = max_interval_time >> 8;
        data[1] = max_interval_time;
        eeprom_write (MAX_INTERVAL_TIME , data);
        k_msleep(5);
        wdt_step();

        wiper_run_time = DEFAULT_WIPER_RUN_TIME;
        data[0] = wiper_run_time >> 24;
        data[1] = wiper_run_time >> 16;
        data[2] = wiper_run_time >> 8;
        data[3] = wiper_run_time;
        eeprom_write (WIPER_RUN_TIME , data);
        k_msleep(5);
        wdt_step();

        wiper_pulse_time = DEFAULT_WIPER_PULSE_TIME;
        data[0] = wiper_pulse_time >> 24;
        data[1] = wiper_pulse_time >> 16;
        data[2] = wiper_pulse_time >> 8;
        data[3] = wiper_pulse_time;
        eeprom_write (WIPER_PULSE_TIME , data);
        k_msleep(5);
        wdt_step();

        c_output_enable = DEFAULT_C_OUTPUT_ENABLE;
        data[0] = c_output_enable;
        eeprom_write (C_OUTPUT_ENABLE , data);
        k_msleep(5);
        wdt_step();

        washer_ol = DEFAULT_WASHER_OL;
        data[0] = washer_ol >> 8;
        data[1] = washer_ol;
        eeprom_write (WASHER_OL , data);
        k_msleep(5);
        wdt_step();

        washer_scgnd = DEFAULT_WASHER_SCGND;
        data[0] = washer_scgnd >> 8;
        data[1] = washer_scgnd;
        eeprom_write (WASHER_SCGND , data);
        k_msleep(5);
        wdt_step();

        pwm_max_percentage = DEFAULT_MAX_PWM_PERCENTAGE;
        data[0] = pwm_max_percentage;
        eeprom_write (PWM_MAX_PERCENTAGE , data);
        k_msleep(5);
        wdt_step();

        pwm_min_percentage = DEFAULT_MIN_PWM_PERCENTAGE;
        data[0] = pwm_min_percentage;
        eeprom_write (PWM_MIN_PERCENTAGE , data);
        k_msleep(5);
        wdt_step();
    }
}

void config_set_working_hour(uint8_t *data)
{
    working_hour = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
    working_hour = working_hour / WORKING_HOURS_FACTOR;
}

void config_update_working_hour(void)
{
    working_hour++;
}

uint32_t config_get_working_hour(void)
{
    return working_hour;
}

uint8_t config_get_ecu_hw_pn(uint8_t byte)
{
    return ecu_hw_pn[byte];
}

uint8_t config_get_ecu_sn(uint8_t byte)
{
    return ecu_sn[byte];
}

uint8_t config_get_ecu_hw_version(uint8_t byte)
{
    return ecu_hw_version[byte];
}

uint8_t config_get_main_pn(uint8_t byte)
{
    return main_pn[byte];
}

void config_set_main_pn(uint8_t *data)
{
    memcpy(main_pn, data, 4U);
}

uint16_t config_get_reset_time(void){return reset_time;}
uint16_t config_get_max_pump_time(void){return max_pump_time;}
uint16_t config_get_pause_pump_ratio(void){return pause_pump_ratio;}
uint16_t config_get_present_interval_time(void){return present_interval_time;}
uint16_t config_get_min_interval_time(void){return min_interval_time;}
uint16_t config_get_max_interval_time(void){return max_interval_time;}
uint32_t config_get_wiper_run_time(void){return wiper_run_time;}
uint32_t config_get_wiper_pulse_time(void){return wiper_pulse_time;}
uint8_t config_get_c_output_enable(void){return c_output_enable;}
uint16_t config_get_washer_ol(void){return washer_ol;}
uint16_t config_get_washer_scgnd(void){return washer_scgnd;}
uint8_t config_get_pwm_max_limit(void){return pwm_max_percentage;}
uint8_t config_get_pwm_min_limit(void){return pwm_min_percentage;}

uint8_t get_WiperLeftOutput(void) {return status_str.individual.WiperLeftOutput;}
uint8_t get_PumpLeftOutput(void) {return status_str.individual.PumpLeftOutput;}
uint8_t get_FL_Left_01(void) {return status_str.individual.FL_Left_01;}
uint8_t get_FL_Left_02(void) {return status_str.individual.FL_Left_02;}
uint8_t get_ManLeftSelected(void) {return status_str.individual.ManLeftSelected;}

uint8_t get_WiperRearOutput(void) {return status_str.individual.WiperRearOutput;}
uint8_t get_PumpRearOutput(void) {return status_str.individual.PumpRearOutput;}
uint8_t get_FL_Rear_03(void) {return status_str.individual.FL_Rear_03;}
uint8_t get_FL_Rear_04(void) {return status_str.individual.FL_Rear_04;}
uint8_t get_ManRearSelected(void) {return status_str.individual.ManRearSelected;}

uint8_t get_WiperRightOutput(void) {return status_str.individual.WiperRightOutput;}
uint8_t get_PumpRightOutput(void) {return status_str.individual.PumpRightOutput;}
uint8_t get_FL_Right_05(void) {return status_str.individual.FL_Right_05;}
uint8_t get_FL_Right_06(void) {return status_str.individual.FL_Right_06;}
uint8_t get_ManRightSelected(void) {return status_str.individual.ManRightSelected;}

uint8_t get_AutoModeSelected(void) {return status_str.individual.AutoModeSelected;}
uint8_t get_FL_Auto_07(void) {return status_str.individual.FL_Auto_07;}
uint8_t get_FL_Auto_08(void) {return status_str.individual.FL_Auto_08;}

uint8_t get_ParkPosSelected(void) {return status_str.individual.ParkPosSelected;}
uint8_t get_FL_Park_09(void) {return status_str.individual.FL_Park_09;}
uint8_t get_FL_Park_10(void) {return status_str.individual.FL_Park_10;}

uint8_t get_AutoWipingActive(void) {return status_str.individual.AutoWipingActive;}
uint32_t get_IntermittencePausingTime(void) {
    return  present_interval_time;
}

uint32_t get_CANIntermittencePausingTime(uint8_t id)
{
    static uint16_t ret_value;

    if (id == Intermittence_Min_CAN_ID)  ret_value = min_interval_time;
    if (id == Intermittence_Max_CAN_ID)  ret_value = max_interval_time;
    if (id == Intermittence_SetVal_CAN_ID) ret_value = present_interval_time;
    return ret_value;
}

void set_WiperLeftOutput(uint8_t value) { status_str.individual.WiperLeftOutput = value;}
void set_PumpLeftOutput(uint8_t value) { status_str.individual.PumpLeftOutput = value;}
void set_FL_Left_01(uint8_t value) { status_str.individual.FL_Left_01 = value;}
void set_FL_Left_02(uint8_t value) { status_str.individual.FL_Left_02 = value;}
void set_ManLeftSelected(uint8_t value) { status_str.individual.ManLeftSelected = value;}

void set_WiperRearOutput(uint8_t value) { status_str.individual.WiperRearOutput = value;}
void set_PumpRearOutput(uint8_t value) { status_str.individual.PumpRearOutput = value;}
void set_FL_Rear_03(uint8_t value) { status_str.individual.FL_Rear_03 = value;}
void set_FL_Rear_04(uint8_t value) { status_str.individual.FL_Rear_04 = value;}
void set_ManRearSelected(uint8_t value) {status_str.individual.ManRearSelected = value;}

void set_WiperRightOutput(uint8_t value) { status_str.individual.WiperRightOutput = value;}
void set_PumpRightOutput(uint8_t value) { status_str.individual.PumpRightOutput = value;}
void set_FL_Right_05(uint8_t value) { status_str.individual.FL_Right_05 = value;}
void set_FL_Right_06(uint8_t value) { status_str.individual.FL_Right_06 = value;}
void set_ManRightSelected(uint8_t value) { status_str.individual.ManRightSelected = value;}

void set_AutoModeSelected(uint8_t value) { status_str.individual.AutoModeSelected = value;}
void set_FL_Auto_07(uint8_t value) { status_str.individual.FL_Auto_07 = value;}
void set_FL_Auto_08(uint8_t value) { status_str.individual.FL_Auto_08 = value;}

void set_ParkPosSelected(uint8_t value) { status_str.individual.ParkPosSelected = value;}
void set_FL_Park_09(uint8_t value) { status_str.individual.FL_Park_09 = value;}
void set_FL_Park_10(uint8_t value) { status_str.individual.FL_Park_10 = value;}

void set_AutoWipingActive(uint8_t value)
{
    if((get_AutoModeSelected() != Off) || (value == Off))
        status_str.individual.AutoWipingActive = value;
}

RETStatus set_IntermittencePausingTime(uint16_t value, uint8_t id)
{
    uint8_t data[2];
    RETStatus retval = STATUS_OK;

    memset(data, 0U, sizeof(data));

    if (id == Intermittence_Min_CAN_ID){
        if((value >= 0U) && (value < max_interval_time) && (value <= present_interval_time)){min_interval_time = value;}
        else
        {
            if(value > present_interval_time) min_interval_time = present_interval_time;
            else if (value < 0 ) min_interval_time = 0;
            retval = STATUS_ERROR;
        }
        data[0] = min_interval_time >> 8;
        data[1] = min_interval_time;
        eeprom_write(MIN_INTERVAL_TIME, data);
    }
    else if (id == Intermittence_Max_CAN_ID) {
        if((value <= MAX_MAX_INTERVAL_TIME) && (value > min_interval_time) && (value >= present_interval_time)) {max_interval_time = value;}
        else
        {
          if(value < present_interval_time) max_interval_time = present_interval_time;
          else if (value > MAX_MAX_INTERVAL_TIME ) max_interval_time = MAX_MAX_INTERVAL_TIME;
          retval = STATUS_ERROR;
        }
        data[0] = max_interval_time >> 8;
        data[1] = max_interval_time;
        eeprom_write(MAX_INTERVAL_TIME, data);
    }
    else if (id == Intermittence_SetVal_CAN_ID) {
        present_interval_time = value;
        if ( present_interval_time < min_interval_time ){
            present_interval_time = min_interval_time;
            retval = STATUS_ERROR;
        }
        else if ( present_interval_time > max_interval_time ){
            present_interval_time = max_interval_time;
            retval = STATUS_ERROR;
        }
        data[0] = present_interval_time >> 8;
        data[1] = present_interval_time;
        eeprom_write(PRESENT_INTERVAL_TIME , data);
    }

    status_str.individual.IntermittencePausingTimeHigh = ((present_interval_time + 500) / 1000) >> 8; //ms to sec
    status_str.individual.IntermittencePausingTimeLow = ((present_interval_time + 500) / 1000) & 0x00FF;

    return retval;
}
