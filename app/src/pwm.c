/**
 * @file   pwm.c
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for PWM module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "watchdog.h"
#include "util.h"
#include "pwm.h"
#include "assert.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define DEFAULT_PERIOD_CYCLE     1920U
#define DEFAULT_PULSE_CYCLE       960U
#define PWMSEL_PB4                  0U
/* Default port should be adapted per board to fit the channel
 * associated to the PWM pin. For instance, for following device,
 *      pwm1: pwm {
 *              status = "okay";
 *              pinctrl-0 = <&tim1_ch3_pe13>;
 *      };
 * the following should be used:
 * #define DEFAULT_PWM_PORT 3
 */
#define DEFAULT_PWM_PORT            1

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(L_pwm);

/* Local variables
---------------------------------------------------------------------------*/
const struct device *pwm_dev0 = DEVICE_DT_GET(DT_NODELABEL(pwm3));
pwm_flags_t flags = PWM_POLARITY_NORMAL;

/* Global variables
---------------------------------------------------------------------------*/
uint16_t value_received = 0;
uint8_t pwm_max_limit;
uint8_t pwm_min_limit;

/* Global functions
---------------------------------------------------------------------------*/
/** @brief init pwm module
 * Initialize and configure pwm pin 
 */
void pwm_init(void)
{
    S_ASSERT(device_is_ready(pwm_dev0), ASSERT_INTERNAL, "Error: PWM device is not ready\n");
    pwm_max_limit = config_get_pwm_max_limit();
    pwm_min_limit = config_get_pwm_min_limit();

    pwm_set(pwm_max_limit);
    MODULE_SINGLE_INIT;
}

/** @brief set pwm value
 * @param value - percentage of brightness
 * @param sel - selector
 * @return STATUS_OK if successful
 */
RETStatus pwm_set(const uint16_t value)
{
    RETStatus status = STATUS_OK;

    value_received = value;
    
    if(value_received > MAX_PWM_VALUE) value_received = MAX_PWM_VALUE;
    /* ((value - min) / (max - min)) * 100 */
    int percentage_dev0 = ((value_received * (pwm_max_limit- pwm_min_limit)) + (MAX_PWM_VALUE * pwm_min_limit)) / MAX_PWM_VALUE;
    percentage_dev0 = 100U - percentage_dev0;

    uint32_t pulse_dev0 = (DEFAULT_PERIOD_CYCLE * percentage_dev0) / 100;

    int ret = STATUS_ERROR;
   
    ret = pwm_pin_set_cycles(pwm_dev0, DEFAULT_PWM_PORT, DEFAULT_PERIOD_CYCLE, pulse_dev0, flags);

    status = (ret != 0) ? STATUS_ERROR : STATUS_OK;
    return status;
}

