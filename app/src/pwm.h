#pragma once
/**
 * @file   pwm.h
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for PWM module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "config.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define MAX_PWM_VALUE           20000U

/* Global functions declaration
---------------------------------------------------------------------------*/
void pwm_init(void);
RETStatus pwm_set(const uint16_t value);
