/**
 * @file   i2c.c
 * @author Abel Tarragó
 * @date   March 2022
 * @brief  Public APIs and resources for I2C module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "assert.h"
#include "util.h"
#include "i2c.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define MEM_EEPROM_NUM_BYTES  4096
#define MEM_EEPROM_BASE_ADDR  0x50

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(L_i2c);

/* Project specific types
---------------------------------------------------------------------------*/
static const struct device *i2c_dev = DEVICE_DT_GET(DT_NODELABEL(i2c2));
uint32_t i2c_cfg = I2C_SPEED_SET(I2C_SPEED_FAST) | I2C_MODE_MASTER;
static struct i2c_msg msg[2U];

/* Global functions
---------------------------------------------------------------------------*/
/** @brief write API for i2c module
 * @param addr - byte address
 * @param data - pointer to data
 * @param num_bytes - valid data bytes
 * @return Status
 */
RETStatus i2c_write_func(const uint16_t addr, uint8_t *data, const uint32_t num_bytes)
{
    S_ASSERT(data != NULL, ASSERT_BOUNDS, "Invalid pointer");

    S_ASSERT(addr < MEM_EEPROM_NUM_BYTES, ASSERT_BOUNDS, "Invalid address");

    uint32_t max_bytes_to_send = (addr%32U) ? 32U-(addr%32U) : 32U;
    S_ASSERT(num_bytes <= max_bytes_to_send, ASSERT_BOUNDS, "Invalid length");

    uint8_t wr_addr[2U] = { (uint8_t) (addr >> 8U), (uint8_t) (addr & 0x00FF) };

    msg[0].buf = wr_addr;
    msg[0].len = 2U;
    msg[0].flags = I2C_MSG_WRITE;

    msg[1].buf = data;
    msg[1].len = num_bytes;
    msg[1].flags = I2C_MSG_WRITE | I2C_MSG_STOP;

    int ret = i2c_transfer(i2c_dev, msg, 2U, MEM_EEPROM_BASE_ADDR);

    return (ret ? STATUS_ERROR : STATUS_OK);
}

/** @brief read API for i2c module
 * @param addr - byte address
 * @param data - pointer to data
 * @param num_bytes - valid data bytes
 * @return Status
 */
RETStatus i2c_read_func(const uint16_t addr, uint8_t *data, const uint32_t num_bytes)
{
    S_ASSERT(data != NULL, ASSERT_BOUNDS, "Invalid pointer");

    S_ASSERT(addr < MEM_EEPROM_NUM_BYTES, ASSERT_BOUNDS, "Invalid address");

    uint8_t wr_addr[2U] = { (uint8_t) (addr >> 8U), (uint8_t) (addr & 0x00FF) };

    msg[0].buf = wr_addr;
    msg[0].len = 2U;
    msg[0].flags = I2C_MSG_WRITE;

    msg[1].buf = data;
    msg[1].len = num_bytes;
    msg[1].flags = I2C_MSG_READ | I2C_MSG_STOP | I2C_MSG_RESTART;

    int ret = i2c_transfer(i2c_dev, msg, 2U, MEM_EEPROM_BASE_ADDR);

    return (ret ? STATUS_ERROR : STATUS_OK);
}

/** @brief Initialization function for I2C module
 */
void i2c_init(void)
{
    MODULE_SINGLE_INIT;

    S_ASSERT(device_is_ready(i2c_dev), ASSERT_INTERNAL, "I2C: Device driver not found");

    int ret = i2c_configure(i2c_dev, i2c_cfg);

    S_ASSERT(ret == 0, ASSERT_BOUNDS, "I2C: Device not configured");
}
