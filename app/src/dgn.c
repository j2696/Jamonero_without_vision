/**
 * @file   dgn.c
 * @author Abel Tarragó
 * @date   April 2022
 * @brief  Public APIs and resources for Diagnostic module.
 * The main purpose of this software component is to manage all possible DTc which means,define dtc properties, status, store in eeprom, send by can, clear dtc
 */

/* Header of project specific types
-----------------------------------------------------------------------------*/
#include "util.h"
#include "fault_manager.h"
#include "assert.h"
#include "can_claas.h"
#include "can_uds.h"
#include "eeprom.h"
#include "config.h"
#include "dgn.h"
#include "watchdog.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define ENGINE_RPM_THRESHOLD 600 /* in RPM */
#define TIME_TO_START_DTC_DETECTION_SINCE_RPM_UPDATED 5000 /* in ms */
#define SIZE_1 1
#define CONFIRMED 10
#define PASSED -2
#define LENGTH_FOR_DTC_STATUS 4
#define MAX_ID_DTC_IN_EEPROM DTC_ID_1 + MAX_DTC_STORED_IN_EEPROM
#define FSM_DGN_AVOID_STATE_CHANGE_ON_EEPROM_OP_FAILURE  1U
#define Acknowledgement 0xABED2145
#define MAX_DTC_STORED_IN_EEPROM 10

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(dgn);
#define DTC_STR_DEF(num_, ID_, stepUp_, stepDown_, confirmed_, passed_, state_) \
{ \
    .sent = true, \
    .active = false, \
    .dtc_in_eeprom = false, \
    .occurrence_updated = true, \
    .id_updated = true, \
    .wh_updated = true, \
    .clear = false, \
    .ID = ID_, \
    .number = num_,\
    .status = 0, \
    .counter = 0U, \
    .occurrence = 0, \
    .state = state_, \
    .stepUp = stepUp_, \
    .passed = passed_, \
    .lastWorkingHour = 0, \
    .stepDown = stepDown_, \
    .firstWorkingHour = 0, \
    .confirmed = confirmed_, \
    .position_in_eeprom_struct = 0,\
}

/* Project specific types
---------------------------------------------------------------------------*/
typedef enum enum_fsm_dgn_clear_t
{
    FSM_DGN_CLEAR_ST0 = 0U,
    FSM_DGN_WTCLR_ST0,       /* Wait state for CLEAR_ST0  ···  1U */
    FSM_DGN_CLEAR_ST1,
    FSM_DGN_WTCLR_ST1,       /* Wait state for CLEAR_ST1  ···  3U */
    FSM_DGN_CLEAR_ST2,
    FSM_DGN_WTCLR_ST2,       /* Wait state for CLEAR_ST2  ···  5U */
    FSM_DGN_CLEAR_ST3,
    FSM_DGN_WTCLR_ST3,       /* Wait state for CLEAR_ST3  ···  7U */
    FSM_DGN_CLEAR_ST4,
    FSM_DGN_WTCLR_ST4,       /* Wait state for CLEAR_ST4  ···  9U */
    FSM_DGN_CLEAR_ST5,
    FSM_DGN_WTCLR_ST5,       /* Wait state for CLEAR_ST5  ··· 11U */
    FSM_DGN_CLEAR_STATE_MAX,
} enum_fsm_dgn_clear;

typedef enum enum_fsm_dgn_main_t
{
    FSM_DGN_MAIN_ST0 = 0U,
    FSM_DGN_WTMN_ST0,        /* Wait state for MAIN_ST0  ···  1U */    
    FSM_DGN_MAIN_ST1,
    FSM_DGN_WTMN_ST1,        /* Wait state for MAIN_ST1  ···  3U */
    FSM_DGN_MAIN_ST2,
    FSM_DGN_WTMN_ST2,        /* Wait state for MAIN_ST2  ···  5U */
    FSM_DGN_MAIN_ST3,
    FSM_DGN_WTMN_ST3,        /* Wait state for MAIN_ST3  ···  7U */
    FSM_DGN_MAIN_STATE_MAX,
} enum_fsm_dgn_main;

typedef struct dtc_eeprom_t
{
    uint16_t id;
    uint8_t number;
    uint16_t occurrence;
    uint32_t WorkingHour;
    eeprom_id position_dtc_in_eeprom;
}dtc_eeprom;

/* Local variables
-----------------------------------------------------------------------------*/
static bool stacking_dtc_from_eeprom = true;
static bool clear_all_dtc = false;
static bool cleaning = false;
static bool Acknowledgement_received = false;
static bool AlarmMonitoringStatus = false;
static bool AlarmMonitoringAckUsage = false;
static uint8_t sub_stacking_flw = 0U;
static uint8_t index_dtc_stored_eeprom = 0; /* index to know how many dtc are currently stored */
static E_DTC_ID DTC_ID = 0;
static eeprom_id dtc_id_in_eeprom = DTC_ID_1;
static dtc_eeprom dtc_eeprom_str[MAX_DTC_STORED_IN_EEPROM] = {{0xFFFF,0xFF, 0xFFFF, 0xFFFFFFFF, 0xFF}};
static bool delayed_stacking_dtc_from_eeprom = true;

dtcstr dtc_filter[E_MAX_DTC] = {
    DTC_STR_DEF(0, 0x84C, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftA_ScGnd
    DTC_STR_DEF(1, 0x84D, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftA_OL
    DTC_STR_DEF(2, 0x84E, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftB_ScGnd
    DTC_STR_DEF(3, 0x84F, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftB_OL
    DTC_STR_DEF(4, 0x85e, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftC_ScGnd
    DTC_STR_DEF(5, 0x85F, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutLeftC_OL
    DTC_STR_DEF(6, 0x87C, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRearA_ScGnd
    DTC_STR_DEF(7, 0x87D, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutlRearA_OL
    DTC_STR_DEF(8, 0x87E, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRearB_ScGnd
    DTC_STR_DEF(9, 0x87F, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRearB_OL
    DTC_STR_DEF(10, 0x880, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRearC_ScGnd
    DTC_STR_DEF(11, 0x881, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRearC_OL
    DTC_STR_DEF(12, 0x882, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightA_ScGnd
    DTC_STR_DEF(13, 0x883, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightA_OL
    DTC_STR_DEF(14, 0x884, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightB_ScGnd
    DTC_STR_DEF(15, 0x885, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightB_OL
    DTC_STR_DEF(16, 0x886, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightC_ScGnd
    DTC_STR_DEF(17, 0x887, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_DigitalOutRightC_OL
    DTC_STR_DEF(18, 0x888, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateLeftPump_ScGnd
    DTC_STR_DEF(19, 0x889, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateLeftPump_OL
    DTC_STR_DEF(20, 0x88A, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateRearPump_ScGnd
    DTC_STR_DEF(21, 0x88B, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateRearPump_OL
    DTC_STR_DEF(22, 0x88D, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateRightPump_ScGnd
    DTC_STR_DEF(23, 0x88E, SIZE_1, SIZE_1, CONFIRMED, PASSED, TRANSMIT_AND_STORAGE),     //WCM_SolidStateRightPump_OL
};

/* Local functions
-----------------------------------------------------------------------------*/
bool dgn_status(void)
{
    bool status = false;
    if((cleaning) || (stacking_dtc_from_eeprom) || (!AlarmMonitoringStatus))
        status = true;  /* inform fault manager module to stop the test run criteria */
    else
        status = false;
    return status;
}

void dgn_set_alarm_monitoring_status(uint8_t *data)
{
    AlarmMonitoringStatus = (data[0] != true) ? false : true;
    uint32_t AlarmMonitoringAck = data[2] << 24 | data[3] << 16 | data[4] << 8 | data[5];
    AlarmMonitoringAckUsage = (AlarmMonitoringAck == Acknowledgement) ? true : false;
}

void dgn_set_Alarm_Acknowledgement(void)
{
    Acknowledgement_received = true;
}

/* READ ALL DTC CONTENT IN EEPROM */
static void stack_of_dtc_in_eeprom(void)
{
    uint8_t data[4] = {0};

    delayed_stacking_dtc_from_eeprom = true;

    /* go over each position of eeprom seraching each possible DTC*/
    if(dtc_id_in_eeprom == DTC_ID_10){
        dtc_id_in_eeprom = DTC_ID_1;
        DTC_ID++;
        if(DTC_ID == E_MAX_DTC){ /* end of stacking */
            DTC_ID = 0;
            delayed_stacking_dtc_from_eeprom = false; // This will force a false value in var stacking_dtc_from_eeprom //
        }
    }else{
        dtc_id_in_eeprom++;
    }

    /* for each dtc search in eeprom if the ID is present in eeprom*/
    eeprom_read(dtc_id_in_eeprom, data);
    uint16_t id_eeprom = data[0] << 8 | data[1];

    if ((id_eeprom == dtc_filter[DTC_ID].ID) && (!dtc_filter[DTC_ID].dtc_in_eeprom)) sub_stacking_flw = 1U;
    else if (delayed_stacking_dtc_from_eeprom == false)  stacking_dtc_from_eeprom = false;
}
static void stack_of_dtc_in_eeprom_2nd(void)
{
    uint8_t data[4] = {0};

    dtc_filter[DTC_ID].dtc_in_eeprom = true;
    dtc_filter[DTC_ID].position_in_eeprom_struct = index_dtc_stored_eeprom;
    dtc_eeprom_str[index_dtc_stored_eeprom].position_dtc_in_eeprom = dtc_id_in_eeprom;
    dtc_eeprom_str[index_dtc_stored_eeprom].id = dtc_filter[DTC_ID].ID;
    dtc_eeprom_str[index_dtc_stored_eeprom].number = dtc_filter[DTC_ID].number;

    eeprom_read(DTC_OC_1 + (dtc_id_in_eeprom - DTC_ID_1), data);

    dtc_filter[DTC_ID].status = 1;
    dtc_eeprom_str[index_dtc_stored_eeprom].occurrence = data[0] << 8 | data[1];
    dtc_filter[DTC_ID].occurrence =  dtc_eeprom_str[index_dtc_stored_eeprom].occurrence;
    LOG_DBG("\n dtc %X is in eeprom %d put in position strcut %d, ocurrences = %d", dtc_filter[DTC_ID].ID,dtc_id_in_eeprom-DTC_ID_1, index_dtc_stored_eeprom, dtc_filter[DTC_ID].occurrence);

    sub_stacking_flw = 2U;
}
static void stack_of_dtc_in_eeprom_3rd(void)
{
    uint8_t data[4] = {0};

    eeprom_read(DTC_WH_1 + (dtc_id_in_eeprom - DTC_ID_1), data);

    dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
    dtc_filter[DTC_ID].firstWorkingHour = dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour;
    index_dtc_stored_eeprom++;
    index_dtc_stored_eeprom %= MAX_DTC_STORED_IN_EEPROM;
        
    sub_stacking_flw = 0U;
    
    if (delayed_stacking_dtc_from_eeprom == false) stacking_dtc_from_eeprom = false;
}
static void dtc_set_status(const E_DTC_ID error)
{
    if (dtc_filter[error].occurrence == 0){
        dtc_filter[error].status = 0;         /* DTC has not occurred yet, no entry in fault storage */
    }
    if (dtc_filter[error].dtc_in_eeprom){
        if (dtc_filter[error].active == false)
            dtc_filter[error].status = 1;     /* DTC is not active and has not been active since the ignition was switched on but is stored in the fault storage */
        else
            dtc_filter[error].status = 3;     /* DTC is active at present and is entered in the fault storage. */
    }else{
        if((dtc_filter[error].occurrence != 0) && (dtc_filter[error].active == false)){
            dtc_filter[error].status = 2;     /* DTC has been active since the ignition was switched on but is not active at present and is entered
            in the fault storage */
        }
        if((dtc_filter[error].occurrence != 0) && (dtc_filter[error].active == true))
            dtc_filter[error].status = 3;     /* DTC is active at present and is entered in the fault storage. */
    }
}
static void dtc_send_by_claas_can(const E_DTC_ID error)
{
    static uint8_t data[8] = {0};

    if( error >= E_MAX_DTC){ /* send negative response */
        memset(data, 0xFF, 8);
    }else{
        data[0] = (dtc_filter[error].ID >> 8) & 0xFF;
        data[1] = dtc_filter[error].ID & 0xFF;
        data[2] = dtc_filter[error].active;
       /* data[3] = (dtc_filter[error].occurrence >> 8) & 0xFF;
        data[4] = dtc_filter[error].occurrence & 0xFF;
        data[5] = (dtc_filter[error].counter >> 8) & 0xFF;
        data[6] = dtc_filter[error].counter & 0xFF;
        data[7] = 0;*/
    }
    can_send_frame(cWcmsVbbcAlarm, data, 8);
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief s14 CLEAR */
void dgn_clear_dtc(uint32_t dtc_id)
{
    static E_DTC_ID dtc;

    for(dtc = 0; dtc < E_MAX_DTC ; dtc++) if(dtc_filter[dtc].ID == dtc_id) break;

    if(dtc_id == ALL_DTC){
        clear_all_dtc = true;
        DTC_ID = 0;
    }
    else
        dtc_filter[dtc].clear = true;
    
    cleaning = true;
    stacking_dtc_from_eeprom = false;
    index_dtc_stored_eeprom = 0;
    dtc_id_in_eeprom = DTC_ID_1;
}
/** @brief s1901 NUMBER OF DTC */
void dgn_get_number_of_dtc(uint8_t mask, uint8_t *data, uint8_t *dlc)
{
    uint8_t cnt = 0;
    bool update = false;
    for (E_DTC_ID dtc = 0 ; dtc <  E_MAX_DTC ; dtc++){

        if (((mask & 0x01) == 0x01) && (!update)){          /* bit 1 = Test failed */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x02) == 0x02) && (!update)){           /* bit 2 = Test failed since last cycle */
            if (dtc_filter[dtc].status == 1){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x04) == 0x04) && (!update)){           /* bit 3 = Pending DTC */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x08) == 0x08) && (!update)){           /* bit 4 = Confirmed DTC */
            if (dtc_filter[dtc].active == true){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x10) == 0x10) && (!update)){           /* bit 5 = test not completed since last clear */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x20) == 0x20) && (!update)){           /* bit 6 = test failed since last clear */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x40) == 0x40) && (!update)){           /* bit 7 = test not completed this monitor cycle */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        if(((mask & 0x80) == 0x80) && (!update)){           /* bit 8 = warning indicator request */
            if (dtc_filter[dtc].counter > 0){
                cnt++;
                update = true;
            }
        }
        update = false;
    }
    
    data[0] = 0;
    data[1] = cnt;
    *dlc = 2;
}
/** @brief s1902 STATUS OF DTC */
void dgn_get_status_of_dtc(uint8_t mask, uint8_t *data, uint8_t *dlc)
{
    uint8_t cnt = 0;
    bool update = false;
    for (E_DTC_ID dtc = 0 ; dtc <  E_MAX_DTC ; dtc++){
        if (((mask & 0x01) == 0x01) && (!update)) {         /* bit 1 = Test failed */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x02) == 0x02) && (!update)){           /* bit 2 = Test failed since last cycle */
            if (dtc_filter[dtc].status == 1){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x04) == 0x04) && (!update)){           /* bit 3 = Pending DTC */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x08) == 0x08) && (!update)){           /* bit 4 = Confirmed DTC */
            if (dtc_filter[dtc].active == true){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x10) == 0x10) && (!update)){           /* bit 5 = test not completed since last clear */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x20) == 0x20) && (!update)){           /* bit 6 = test failed since last clear */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x40) == 0x40) && (!update)){           /* bit 7 = test not completed this monitor cycle */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        if(((mask & 0x80) == 0x80) && (!update)){           /* bit 8 = warning indicator request */
            if (dtc_filter[dtc].counter > 0){
                data[cnt] = 0x00;
                data[cnt + 1] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
                data[cnt + 2] = (uint8_t)dtc_filter[dtc].ID;
                data[cnt + 3] = dtc_filter[dtc].status;
                cnt += 4;
                update = true;
            }
        }
        
        update = false;
    }
    *dlc = cnt;
}
/** @brief s1904 SNAPSHOOT OF DTC */
void dgn_get_snapshoot_data_of_dtc(uint8_t *data, uint8_t *dlc, uint32_t dtc_id)
{
    uint8_t cnt = 0;
    static E_DTC_ID dtc;

    for(dtc = 0; dtc < E_MAX_DTC ; dtc++) {
        if(dtc_filter[dtc].ID == dtc_id){
            data[cnt++] = dtc_filter[dtc].status;
            data[cnt++] = 0x00; /*DTCSnapshotRecordNumber #1*/
            data[cnt++] = 0x00; /*DTCSnapshotRecordNumberOfIdentifiers #1*/
            data[cnt++] = 0x00; /*DTCRecord*/
            data[cnt++] = 0x00; /*DTCRecord*/
            data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0xFF000000) >> 24);
            data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0x00FF0000) >> 16);
            data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0x0000FF00) >> 8);
            data[cnt++] = (uint8_t)(dtc_filter[dtc].firstWorkingHour);
            *dlc = cnt;
            break;
        }
    }
}
/** @brief s190A SUPPORTED DTC */
void dgn_get_supported_dtc(uint8_t *data, uint16_t *dlc)
{
    uint16_t cnt = 0;

    data[cnt++] = 0x01; /*DTCStatusMask*/

    for (E_DTC_ID dtc = 0 ; dtc <  E_MAX_DTC ; dtc++){
        data[cnt++] = 0x00;
        data[cnt++] = (uint8_t)((dtc_filter[dtc].ID & 0xFF00) >> 8);
        data[cnt++] = (uint8_t)dtc_filter[dtc].ID;
        data[cnt++] = dtc_filter[dtc].status;
    }
    *dlc = cnt;
}
/** @brief s1906 EXTENDED DATA */
void dgn_get_extended_data_of_dtc(uint8_t *data, uint8_t *dlc, uint32_t dtc_id)
{
    uint8_t cnt = 0;
    static E_DTC_ID dtc;

    for(dtc = 0; dtc < E_MAX_DTC ; dtc++) if(dtc_filter[dtc].ID == dtc_id) break;

    data[cnt++] = dtc_filter[dtc].status;
    data[cnt++] = 0x01; /*DTCExtendedDataRecordNumber #1*/
    data[cnt++] = (uint8_t)((dtc_filter[dtc].occurrence & 0xFF00) >> 8);
    data[cnt++] = (uint8_t)dtc_filter[dtc].occurrence;
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0xFF000000) >> 24);
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0x00FF0000) >> 16);
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].firstWorkingHour) & 0x0000FF00) >> 8);
    data[cnt++] = (uint8_t)(dtc_filter[dtc].firstWorkingHour);
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].lastWorkingHour) & 0xFF000000) >> 24);
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].lastWorkingHour) & 0x00FF0000) >> 16);
    data[cnt++] = (uint8_t)(((dtc_filter[dtc].lastWorkingHour) & 0x0000FF00) >> 8);
    data[cnt++] = (uint8_t)(dtc_filter[dtc].lastWorkingHour);
    *dlc = cnt;
}
/** @brief Initialization function for DGN module
 */
void dgn_init(void)
{
    MODULE_SINGLE_INIT;
    can_claas_init();
}
/** @brief Main function for diagnostics management
 */
void dgn_step(void)
{
    static uint8_t data[4] = {0};
    static uint64_t time_spent_since_last_error = 0;          ///< initial time for last time sent error info in ms
    static uint64_t time_stamp_since_last_error = 0;          ///< current time for last time sent error info in ms

    if (stacking_dtc_from_eeprom)
    {
        switch (sub_stacking_flw) {
            case 0U:
                stack_of_dtc_in_eeprom();
                break;
            case 1U:
                stack_of_dtc_in_eeprom_2nd();
                break;
            case 2U:
                stack_of_dtc_in_eeprom_3rd();
                break;
            default:
                LOG_DBG("invalid flow when stacking dtc from eeprom \n");
                break;
        }
    }
    else if (cleaning)
    {
        static enum_fsm_dgn_clear dgnFsmClear_status = FSM_DGN_CLEAR_ST0;
        
        S_ASSERT(dgnFsmClear_status < FSM_DGN_CLEAR_STATE_MAX, ASSERT_BOUNDS, "Dgn Invalid State FSM-CLEAR");
        
        enum_fsm_dgn_clear dgnFsmClear_next_st = FSM_DGN_CLEAR_STATE_MAX;

        if (dgnFsmClear_status % 2U == 0U) /* Even states */
        {
            RETStatus ret = STATUS_ERROR;

            switch (dgnFsmClear_status) {

                case FSM_DGN_CLEAR_ST0: /* CLEAR DTC ID  (DELETED FROM EEPROM) */
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST0;

                    if ((clear_all_dtc) && (dtc_id_in_eeprom < MAX_ID_DTC_IN_EEPROM) && (dtc_id_in_eeprom > DTC_ID_1 - 1))
                    {
                        ret = eeprom_erase_nowait(dtc_id_in_eeprom);  /* ID */
                    }
                    else if ((dtc_filter[DTC_ID].dtc_in_eeprom) && (dtc_filter[DTC_ID].clear))
                    {
                        ret = eeprom_erase_nowait(dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].position_dtc_in_eeprom);
                    }
                    break;

                case FSM_DGN_CLEAR_ST1: /* CLEAR DTC OCCURRENCES (DELETED FROM EEPROM) */
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST1;

                    if((clear_all_dtc) && (dtc_id_in_eeprom < MAX_ID_DTC_IN_EEPROM) && (dtc_id_in_eeprom > DTC_ID_1 - 1))
                    {
                        ret = eeprom_erase_nowait((DTC_OC_1 - DTC_ID_1) + dtc_id_in_eeprom);  /* OCCURRENCES */
                    }
                    else if ((dtc_filter[DTC_ID].dtc_in_eeprom) && (dtc_filter[DTC_ID].clear))
                    {
                        ret = eeprom_erase_nowait(dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].position_dtc_in_eeprom + (DTC_OC_1 - DTC_ID_1));
                    }
                    break;

                case FSM_DGN_CLEAR_ST2: /* CLEAR DTC WH (DELETED FROM EEPROM) */
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST2;

                    if((clear_all_dtc) && (dtc_id_in_eeprom < MAX_ID_DTC_IN_EEPROM) && (dtc_id_in_eeprom > DTC_ID_1 - 1))
                    {
                        //ret = eeprom_erase_nowait((DTC_WH_1 - DTC_ID_1) + dtc_id_in_eeprom);  /* WH */
                    }
                    else if ((dtc_filter[DTC_ID].dtc_in_eeprom) && (dtc_filter[DTC_ID].clear))
                    {
                        ret = eeprom_erase_nowait(dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].position_dtc_in_eeprom + (DTC_WH_1 - DTC_ID_1));
                    }
                    break;

                case FSM_DGN_CLEAR_ST3: /* REMOVE DTC FROM EEPROM STRUCT IN RAM */
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST3;

                    if((clear_all_dtc) && (dtc_id_in_eeprom < MAX_ID_DTC_IN_EEPROM) && (dtc_id_in_eeprom > DTC_ID_1 - 1)){
                        dtc_eeprom_str[dtc_id_in_eeprom - DTC_ID_1].id = 0xFFFF;
                        dtc_eeprom_str[dtc_id_in_eeprom - DTC_ID_1].occurrence = 0xFFFF;
                        dtc_eeprom_str[dtc_id_in_eeprom - DTC_ID_1].WorkingHour = 0xFFFFFFFF;
                        dtc_eeprom_str[dtc_id_in_eeprom - DTC_ID_1].position_dtc_in_eeprom = 0xFF;
                    }else if((dtc_filter[DTC_ID].dtc_in_eeprom) && (dtc_filter[DTC_ID].clear)){
                        dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].id = 0xFFFF;
                        dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].occurrence = 0xFFFF;
                        dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].WorkingHour = 0xFFFFFFFF;
                        dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].position_dtc_in_eeprom = 0xFF;
                    }
                    break;

                case FSM_DGN_CLEAR_ST4: /* REMOVE DTC FROM EEPROM STRUCT IN RAM */
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST4;

                    if((clear_all_dtc) || (dtc_filter[DTC_ID].clear)){
                        dtc_filter[DTC_ID].status = 0;
                        dtc_filter[DTC_ID].sent = true;
                        dtc_filter[DTC_ID].counter = 0U;
                        dtc_filter[DTC_ID].occurrence = 0;
                        dtc_filter[DTC_ID].active = false;
                        dtc_filter[DTC_ID].id_updated = true;
                        dtc_filter[DTC_ID].wh_updated = true;
                        dtc_filter[DTC_ID].lastWorkingHour = 0;
                        dtc_filter[DTC_ID].firstWorkingHour = 0;
                        dtc_filter[DTC_ID].dtc_in_eeprom = false;
                        dtc_filter[DTC_ID].occurrence_updated = true;
                        dtc_filter[DTC_ID].position_in_eeprom_struct = 0;
                        ////LOG_DBG("\nCLEAN DTC %d ", DTC_ID);
                        if (dtc_filter[DTC_ID].clear)  /* end of individual cleaning, set the initial values */
                        {
                            dtc_filter[DTC_ID].clear = false;
                            DTC_ID = 0;
                            dtc_id_in_eeprom = DTC_ID_1;
                            
                            // cleaning exit & FSM reset //
                            cleaning = false;
                            stacking_dtc_from_eeprom = true;
                            sub_stacking_flw = 0U;
                            dgnFsmClear_next_st = FSM_DGN_CLEAR_ST0;
                        }
                    }
                    break;

                case FSM_DGN_CLEAR_ST5:
                    dgnFsmClear_next_st = FSM_DGN_WTCLR_ST5;

                    if(dtc_id_in_eeprom < MAX_ID_DTC_IN_EEPROM)
                        dtc_id_in_eeprom++;

                    DTC_ID++;
                    if(DTC_ID == E_MAX_DTC)  /* end of all cleaning, set the initial values */
                    {
                        DTC_ID = 0;
                        dtc_id_in_eeprom = DTC_ID_1;
                        
                        // cleaning exit & FSM reset //
                        cleaning = false;
                        stacking_dtc_from_eeprom = true;
                        sub_stacking_flw = 0U;
                        dgnFsmClear_next_st = FSM_DGN_CLEAR_ST0;
                        //LOG_DBG("\nCLEAN ALL");
                    }
                    break;

                default:
                    break;

            } /* END: switch (dgnFsmClear_status) */
        }

        if (dgnFsmClear_status % 2U) /* Odd states */
        {
            dgnFsmClear_next_st = dgnFsmClear_status;

            if (dgnFsmClear_status < FSM_DGN_WTCLR_ST3)
            {
                if (eeprom_nowait_command_completed() == true) {
                    dgnFsmClear_next_st++;
                }
            }
            else
                dgnFsmClear_next_st++;

            dgnFsmClear_next_st %= FSM_DGN_CLEAR_STATE_MAX;
        }

        dgnFsmClear_status = dgnFsmClear_next_st;
    }else{
        static enum_fsm_dgn_main dgnFsmMain_status = FSM_DGN_MAIN_ST0;

        S_ASSERT(dgnFsmMain_status < FSM_DGN_MAIN_STATE_MAX, ASSERT_BOUNDS, "Dgn Invalid State FSM-MAIN");

        enum_fsm_dgn_main dgnFsmMain_next_st = FSM_DGN_MAIN_STATE_MAX;

        if (AlarmMonitoringStatus)
        {
            bool bfastForward = true;
            
            if ((dtc_filter[DTC_ID].active) && ((dtc_filter[DTC_ID].state == TRANSMIT_AND_STORAGE) || (dtc_filter[DTC_ID].state == STORAGE)))
            {
                bfastForward = (dgnFsmMain_status == FSM_DGN_MAIN_ST0) ? false : true;
            }
                    
            if ((bfastForward == false) || (dgnFsmMain_status != FSM_DGN_MAIN_ST0))
            {
                bfastForward = false;

                if (dgnFsmMain_status % 2U == 0U) /* Even States */
                {
                    RETStatus ret = STATUS_ERROR;

                    switch (dgnFsmMain_status) {

                        case FSM_DGN_MAIN_ST0: /* UPDATE DTC OCCURRENCES IF IS PRESENT IN EEPROM */
                            dgnFsmMain_next_st = FSM_DGN_WTMN_ST0;

                            if ((dtc_filter[DTC_ID].dtc_in_eeprom) && (!dtc_filter[DTC_ID].occurrence_updated)) { /* increment the occurrence and keep the same id also first working hour in the same position */
                                dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].occurrence = dtc_filter[DTC_ID].occurrence;
                                dtc_filter[DTC_ID].occurrence_updated = true;
                                dtc_filter[DTC_ID].id_updated = true;
                                dtc_filter[DTC_ID].wh_updated = true;
                                data[0] = (uint8_t) ((dtc_filter[DTC_ID].occurrence & 0xFF00) >> 8);
                                data[1] = (uint8_t) dtc_filter[DTC_ID].occurrence;
                                LOG_DBG("\n update dtc %d occurrences %d in eeprom position %d", DTC_ID, dtc_filter[DTC_ID].occurrence, dtc_filter[DTC_ID].position_in_eeprom_struct);
                                ret = eeprom_write_nowait(dtc_eeprom_str[dtc_filter[DTC_ID].position_in_eeprom_struct].position_dtc_in_eeprom + (DTC_OC_1 - DTC_ID_1), data);
                            }
                            break;

                        case FSM_DGN_MAIN_ST1: // ADD DTC ID IF IS NOT PRESENT IN EEPROM
                            dgnFsmMain_next_st = FSM_DGN_WTMN_ST1;

                            if ((!dtc_filter[DTC_ID].dtc_in_eeprom) && (!dtc_filter[DTC_ID].id_updated)) {  /* add all info to a new position and increment de buffer index */
                                if(dtc_eeprom_str[index_dtc_stored_eeprom].id != 0xFFFF){
                                    //LOG_DBG("\n clean dtc %X  stored in eeprom position %d",dtc_eeprom_str[index_dtc_stored_eeprom].number, index_dtc_stored_eeprom);
                                    dtc_filter[dtc_eeprom_str[index_dtc_stored_eeprom].number].dtc_in_eeprom = false; // clean or remove previous DTC stored in this position
                                    dtc_eeprom_str[index_dtc_stored_eeprom].occurrence = 0xFFFF;
                                    dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour = 0xFFFFFFFF;
                                    dtc_eeprom_str[index_dtc_stored_eeprom].position_dtc_in_eeprom = 0xFF;
                                }
                                dtc_eeprom_str[index_dtc_stored_eeprom].id = dtc_filter[DTC_ID].ID;    // add new dtc in this position
                                dtc_filter[DTC_ID].id_updated = true;
                                data[0] = (uint8_t) ((dtc_filter[DTC_ID].ID & 0xFF00) >> 8);
                                data[1] = (uint8_t) dtc_filter[DTC_ID].ID;
                                LOG_DBG("\n add dtc %d ID in eeprom position %d", DTC_ID, index_dtc_stored_eeprom);
                                ret = eeprom_write_nowait(index_dtc_stored_eeprom + DTC_ID_1, data);
                            }
                            break;

                        case FSM_DGN_MAIN_ST2: /* ADD DTC OCURRENCES IF IS NOT PRESENT IN EEPROM  */
                            dgnFsmMain_next_st = FSM_DGN_WTMN_ST2;

                            if ((!dtc_filter[DTC_ID].dtc_in_eeprom) && (!dtc_filter[DTC_ID].occurrence_updated)) {  /* add all info to a new position and increment de buffer index */
                                dtc_eeprom_str[index_dtc_stored_eeprom].occurrence = dtc_filter[DTC_ID].occurrence;
                                dtc_filter[DTC_ID].occurrence_updated = true;
                                data[0] = (uint8_t) ((dtc_eeprom_str[index_dtc_stored_eeprom].occurrence & 0xFF00) >> 8);
                                data[1] = (uint8_t) dtc_eeprom_str[index_dtc_stored_eeprom].occurrence;
                                ret = eeprom_write_nowait(index_dtc_stored_eeprom + DTC_OC_1, data);
                            }
                            break;

                        case FSM_DGN_MAIN_ST3: /* ADD DTC WH IF IS NOT PRESENT IN EEPROM  */
                            dgnFsmMain_next_st = FSM_DGN_WTMN_ST3;

                            if ((!dtc_filter[DTC_ID].dtc_in_eeprom) && (!dtc_filter[DTC_ID].wh_updated)) {  /* add all info to a new position and increment de buffer index */
                                dtc_filter[DTC_ID].dtc_in_eeprom = true;
                                dtc_filter[DTC_ID].wh_updated = true;
                                dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour = config_get_working_hour();
                                data[0] = (uint8_t) (dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour >> 24); /* MSB (secs) */
                                data[1] = (uint8_t) ((dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour & 0x00FF0000) >> 16);
                                data[2] = (uint8_t) ((dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour & 0x0000FF00) >> 8);
                                data[3] = (uint8_t) dtc_eeprom_str[index_dtc_stored_eeprom].WorkingHour; /* LSB (secs) */
                                ret = eeprom_write_nowait(index_dtc_stored_eeprom + DTC_WH_1, data);
                                index_dtc_stored_eeprom++;
                                index_dtc_stored_eeprom %= MAX_DTC_STORED_IN_EEPROM;
                            }
                            break;

                        default:
                            break;

                    } /* END: switch (dgnFsmMain_status) */
                }
            }

            if ((!dtc_filter[DTC_ID].sent) && ((dtc_filter[DTC_ID].state == TRANSMIT_AND_STORAGE) || (dtc_filter[DTC_ID].state == TRANSMIT))) { /* SEND DTC IF IS SET OR ONCE IS NOT MORE PRESENT */
                time_spent_since_last_error = k_uptime_get();
                time_spent_since_last_error = time_spent_since_last_error - time_stamp_since_last_error;
                dtc_filter[DTC_ID].time_spent_ack = k_uptime_get();
                dtc_filter[DTC_ID].time_spent_ack =  dtc_filter[DTC_ID].time_spent_ack -  dtc_filter[DTC_ID].time_stamp_ack;
                if((time_spent_since_last_error > 10 /* ms */) && (dtc_filter[DTC_ID].time_spent_ack > 1000)) {
                    dtc_set_status(DTC_ID);
                    dtc_send_by_claas_can(DTC_ID);
                    if (!AlarmMonitoringAckUsage)  dtc_filter[DTC_ID].sent = true;
                    else if ((AlarmMonitoringAckUsage) && (Acknowledgement_received))  {
                        dtc_filter[DTC_ID].sent = true;
                        Acknowledgement_received = false;
                    }else{}
                    time_stamp_since_last_error = k_uptime_get();
                    dtc_filter[DTC_ID].time_stamp_ack = k_uptime_get();
                }
            }

            if (bfastForward == false)
            {
                if (dgnFsmMain_status % 2U) /* Odd States: waiting for completion */
                {
                    dgnFsmMain_next_st = dgnFsmMain_status;

                    if (eeprom_nowait_command_completed() == true) {
                        dgnFsmMain_next_st++;
                    }

                    dgnFsmMain_next_st %= FSM_DGN_MAIN_STATE_MAX;
                }
            }
            else
                dgnFsmMain_next_st = FSM_DGN_MAIN_ST0;

            if (dgnFsmMain_next_st == FSM_DGN_MAIN_ST0)
            {
                DTC_ID++;
                if(DTC_ID == E_MAX_DTC) { /* end of main fsm, restart */
                    DTC_ID = 0;
                }
            }

            dgnFsmMain_status = dgnFsmMain_next_st;            

        } /* END: if (engine_rpm_access && ignition_get()) */
    }
}
