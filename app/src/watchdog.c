/**
 * @file   watchdog.c
 * @author Rubeń Guijarro
 * @date   March 2022
 * @brief  Public APIs and resources for Watchdog module.
 */

/* Header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "string.h"
#include "watchdog.h"
#include "gpio.h"
#include "uds.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define WDT_NODE                     DT_INST(0, st_stm32_watchdog)
#define WDT_DEV_NAME                 DT_LABEL(WDT_NODE)
#define WDT_MAX_WINDOW               1200U // miliseconds
#define WDT_CMD_BL_CAN_UDS_SESSION   10U
#define WDT_CMD_BL_DEFAULT_DS        0x02  /* Programming Session */

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(watchdog);

/* Local variables
-----------------------------------------------------------------------------*/
volatile uint8_t *bl_command = (uint8_t *) 0x20023FF0;
int wdt_channel_id;
const struct device *wdt;

/* External variables
-----------------------------------------------------------------------------*/
extern bool gpio_toggle_external_watchdog_flag;

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Initialize watchdog
 */
void wdt_init(void)
{
    MODULE_SINGLE_INIT;

    wdt = device_get_binding(WDT_DEV_NAME);
    if (!wdt) {
        LOG_DBG("Cannot get WDT device\n");
        return;
    }

    struct wdt_timeout_cfg wdt_config = {
        /* Reset SoC when watchdog timer expires. */
        .flags = WDT_FLAG_RESET_SOC,

        /* Expire watchdog after max window */
        .window.min = 0U,
        .window.max = WDT_MAX_WINDOW,
    };

    wdt_channel_id = wdt_install_timeout(wdt, &wdt_config);
    if (wdt_channel_id == -ENOTSUP) {
        /* IWDG driver for STM32 doesn't support callback */
        LOG_DBG("Callback support rejected, continuing anyway\n");
        wdt_config.callback = NULL;
        wdt_channel_id = wdt_install_timeout(wdt, &wdt_config);
    }
    if (wdt_channel_id < 0) {
        LOG_DBG("Watchdog install error\n");
        return;
    }

    int err = wdt_setup(wdt, 0); //WDT_OPT_PAUSE_HALTED_BY_DBG
    if (err < 0) {
        LOG_DBG("Watchdog setup error\n");
        return;
    }
}

/** @brief Step for watchdog
 */
void wdt_step(void)
{
    wdt_feed(wdt, wdt_channel_id);
}

/** @brief Step for watchdog
 */
void wdext_step(void)
{
    gpio_toggle_output(WD_OUT);
}

/** @brief infinite loop for triggering reset
 */
void wdt_loop_swreset(bool bEnCommandTroughRamMem)
{
    memset((void *)bl_command, 0x00, 16U);

    if (bEnCommandTroughRamMem == true) {

        bl_command[0U] = 'B';
        bl_command[1U] = 'O';
        bl_command[2U] = 'O';
        bl_command[3U] = 'T';

        bl_command[WDT_CMD_BL_CAN_UDS_SESSION] = WDT_CMD_BL_DEFAULT_DS;

        uds_update_shared_memory();
    }
    else
    {
        bl_command[0U] = 'R';
        bl_command[1U] = 'U';
        bl_command[2U] = 'N';
        bl_command[3U] = '1';
    }

    while (1)
    {
        sys_reboot(0U);
    }
}

/** @brief inf loop for trigger reset + StandBy mode
 */
void wdt_loop_lpms(void)
{
    memset((void *)bl_command, 0x00, 16U);

    while (1)
    {
        bl_command[0U] = 'U';
        bl_command[1U] = 'S';
        bl_command[2U] = 'L';
        bl_command[3U] = 'E';

        wdext_step();
        
        while (1)
        {
            sys_reboot(0U);
        }
    }
}

/** @brief inf loop for trigger reset + StandBy mode when sleep messages is received
 */
void wdt_loop_lpms_sleep_message(void)
{
    memset((void *)bl_command, 0x00, 16U);

    bl_command[0U] = 'U';
    bl_command[1U] = 'S';
    bl_command[2U] = 'L';
    bl_command[3U] = 'E';

    wdext_step();
    
    while (1)
    {
        sys_reboot(0U);
    }
}
