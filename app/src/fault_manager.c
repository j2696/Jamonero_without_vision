/**
 * @file   fault_manager.h
 * @author Abel Tarragó
 * @date   April 2022
 * @brief  The main purpose of this software component is to check all possible DTc and set in case the detection critera is positive
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "dgn.h"
#include "fault_manager.h"
#include "adc.h"
#include "keyb.h"
#include "config.h"
#include "led.h"
#include "tle.h"
#include "crc16.h"
#include "gpio.h"
#include "application.h"
#include "can_claas.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define BUS_OFF_THRSHLD                    5000U /* in ms */
/** @brief Error threshold for keyboard error detection */
#define KEYB_SCLOW_THRSHLD                  600U /* in mv */
#define KEYB_SCHIGH_THRSHLD                2800U
#define KEYB_UNDLOW_THRSHLD                1000U
#define KEYB_UNDHIGH_THRSHLD               2000U
/** @brief Error threshold for backlight error detection */
#define BL_SCLOW_THRSHLD                     10U /* in mv */
#define BL_SCHIGH_THRSHLD                  4000U
#define MAX_OCCURRENCES                     255U
#define MAX_LED_TIMEOUT_IN_MS             10000U
#define LEDS_PER_BUTTON                       4U
#define CNT_FAULTMNG_ACTIVATION_RELOAD_VAL    6U
#define LOW_BATTERY_VOLTAGE                2000U

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(fault_manager);

/* Local variables
-----------------------------------------------------------------------------*/

/* External variables
-----------------------------------------------------------------------------*/
extern dtcstr dtc_filter[E_MAX_DTC];

/* Local functions
-----------------------------------------------------------------------------*/
void dtc_update(E_DTC_ID dtc, bool state)
{
    dtc_filter[dtc].active = state;
    dtc_filter[dtc].sent = false;
    dtc_filter[dtc].id_updated = false;
    dtc_filter[dtc].occurrence_updated = false;
    dtc_filter[dtc].wh_updated = false;
    if(dtc_filter[dtc].active){
        LOG_DBG("\nDTC true: %x", dtc_filter[dtc].ID);
        if(  dtc_filter[dtc].occurrence < MAX_OCCURRENCES)
            dtc_filter[dtc].occurrence++;
        if(dtc_filter[dtc].firstWorkingHour == 0)
            dtc_filter[dtc].firstWorkingHour = config_get_working_hour();
        dtc_filter[dtc].lastWorkingHour = config_get_working_hour();
    }else
        LOG_DBG("\nDTC false: %X", dtc);
}

static void dtc_output_over_load_status()
{
    LOG_DBG("\n over load = %X", get_tle_over_load_status());

    if((get_tle_over_load_status() & 0x0002) && (get_WiperLeftOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_top))
    {
        if(dtc_filter[WCM_DigitalOutLeftA_ScGnd].counter >= dtc_filter[WCM_DigitalOutLeftA_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutLeftA_ScGnd].active)
                dtc_update(WCM_DigitalOutLeftA_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutLeftA_ScGnd].counter += dtc_filter[WCM_DigitalOutLeftA_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0001) && (get_WiperLeftOutput() == WiperXOutput_Park) && (get_ParkPosSelected() == Park_bottom))
    {
        if(dtc_filter[WCM_DigitalOutLeftB_ScGnd].counter >= dtc_filter[WCM_DigitalOutLeftB_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutLeftB_ScGnd].active)
                dtc_update(WCM_DigitalOutLeftB_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutLeftB_ScGnd].counter += dtc_filter[WCM_DigitalOutLeftB_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0004) && (config_get_c_output_enable()) && ((get_WiperLeftOutput() == WiperXOutput_Wash) || ((get_WiperLeftOutput() == WiperXOutput_Wash + WiperXOutput_High))))
    {
        if(dtc_filter[WCM_DigitalOutLeftC_ScGnd].counter >= dtc_filter[WCM_DigitalOutLeftC_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutLeftC_ScGnd].active)
                dtc_update(WCM_DigitalOutLeftC_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutLeftC_ScGnd].counter += dtc_filter[WCM_DigitalOutLeftC_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0020) && (get_WiperRightOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_top))
    {
        if(dtc_filter[WCM_DigitalOutRightA_ScGnd].counter >= dtc_filter[WCM_DigitalOutRightA_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRightA_ScGnd].active)
                dtc_update(WCM_DigitalOutRightA_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRightA_ScGnd].counter += dtc_filter[WCM_DigitalOutRightA_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0010) && (get_WiperRightOutput() == WiperXOutput_Park) && (get_ParkPosSelected() == Park_bottom))
    {
        if(dtc_filter[WCM_DigitalOutRightB_ScGnd].counter >= dtc_filter[WCM_DigitalOutRightB_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRightB_ScGnd].active)
                dtc_update(WCM_DigitalOutRightB_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRightB_ScGnd].counter += dtc_filter[WCM_DigitalOutRightB_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0040) && (config_get_c_output_enable()) && ((get_WiperRightOutput() == WiperXOutput_Wash)|| ((get_WiperRightOutput() == WiperXOutput_Wash + WiperXOutput_High))))
    {
        if(dtc_filter[WCM_DigitalOutRightC_ScGnd].counter >= dtc_filter[WCM_DigitalOutRightC_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRightC_ScGnd].active)
                dtc_update(WCM_DigitalOutRightC_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRightC_ScGnd].counter += dtc_filter[WCM_DigitalOutRightC_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0200) && (get_WiperRearOutput() == WiperXOutput_High))
    {
        if(dtc_filter[WCM_DigitalOutRearA_ScGnd].counter >= dtc_filter[WCM_DigitalOutRearA_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRearA_ScGnd].active)
                dtc_update(WCM_DigitalOutRearA_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRearA_ScGnd].counter += dtc_filter[WCM_DigitalOutRearA_ScGnd].stepUp;
    }
    if((get_tle_over_load_status() & 0x0100) && (get_WiperRearOutput() == WiperXOutput_Park))
    {
        if(dtc_filter[WCM_DigitalOutRearB_ScGnd].counter >= dtc_filter[WCM_DigitalOutRearB_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRearB_ScGnd].active)
                dtc_update(WCM_DigitalOutRearB_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRearB_ScGnd].counter += dtc_filter[WCM_DigitalOutRearB_ScGnd].stepUp;
    }

    if((get_tle_over_load_status() & 0x0400) && (config_get_c_output_enable()) && ((get_WiperRearOutput() == WiperXOutput_Wash) || ((get_WiperRearOutput() == WiperXOutput_Wash + WiperXOutput_High))))
    {
        if(dtc_filter[WCM_DigitalOutRearC_ScGnd].counter >= dtc_filter[WCM_DigitalOutRearC_ScGnd].confirmed){
            if(!dtc_filter[WCM_DigitalOutRearC_ScGnd].active)
                dtc_update(WCM_DigitalOutRearC_ScGnd, true);
        }else
            dtc_filter[WCM_DigitalOutRearC_ScGnd].counter += dtc_filter[WCM_DigitalOutRearC_ScGnd].stepUp;
    }
}

static void dtc_output_open_load_status(void)
{
    LOG_DBG("\n open load = %X", get_tle_open_load_status());

    if((get_WiperLeftOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_top)) {
        if(get_tle_open_load_status() & 0x0002){
            if(dtc_filter[WCM_DigitalOutLeftA_OL].counter >= dtc_filter[WCM_DigitalOutLeftA_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutLeftA_OL].active)
                    dtc_update(WCM_DigitalOutLeftA_OL, true);
            }else
                dtc_filter[WCM_DigitalOutLeftA_OL].counter += dtc_filter[WCM_DigitalOutLeftA_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutLeftA_OL].counter <= dtc_filter[WCM_DigitalOutLeftA_OL].passed){
                if(dtc_filter[WCM_DigitalOutLeftA_OL].active){ dtc_update(WCM_DigitalOutLeftA_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutLeftA_OL].counter -= dtc_filter[WCM_DigitalOutLeftA_OL].stepDown;
            }
        }
    }
    if((get_WiperLeftOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_bottom)){
        if(get_tle_open_load_status() & 0x0001){
            if(dtc_filter[WCM_DigitalOutLeftB_OL].counter >= dtc_filter[WCM_DigitalOutLeftB_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutLeftB_OL].active)
                    dtc_update(WCM_DigitalOutLeftB_OL, true);
            }else
                dtc_filter[WCM_DigitalOutLeftB_OL].counter += dtc_filter[WCM_DigitalOutLeftB_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutLeftB_OL].counter <= dtc_filter[WCM_DigitalOutLeftB_OL].passed){
                if(dtc_filter[WCM_DigitalOutLeftB_OL].active){ dtc_update(WCM_DigitalOutLeftB_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutLeftB_OL].counter -= dtc_filter[WCM_DigitalOutLeftB_OL].stepDown;
            }
        }
    }
    if((config_get_c_output_enable()) && ((get_WiperLeftOutput() == WiperXOutput_Wash) || ((get_WiperLeftOutput() == WiperXOutput_Wash + WiperXOutput_High)))){
        if(get_tle_open_load_status() & 0x0004){
            if(dtc_filter[WCM_DigitalOutLeftC_OL].counter >= dtc_filter[WCM_DigitalOutLeftC_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutLeftC_OL].active)
                    dtc_update(WCM_DigitalOutLeftC_OL, true);
            }else
                dtc_filter[WCM_DigitalOutLeftC_OL].counter += dtc_filter[WCM_DigitalOutLeftC_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutLeftC_OL].counter <= dtc_filter[WCM_DigitalOutLeftC_OL].passed){
                if(dtc_filter[WCM_DigitalOutLeftC_OL].active){ dtc_update(WCM_DigitalOutLeftC_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutLeftC_OL].counter -= dtc_filter[WCM_DigitalOutLeftC_OL].stepDown;
            }
        }
    }
    if((get_WiperRightOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_top)){
        if(get_tle_open_load_status() & 0x0020){
            if(dtc_filter[WCM_DigitalOutRightA_OL].counter >= dtc_filter[WCM_DigitalOutRightA_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutRightA_OL].active)
                    dtc_update(WCM_DigitalOutRightA_OL, true);
            }else
                dtc_filter[WCM_DigitalOutRightA_OL].counter += dtc_filter[WCM_DigitalOutRightA_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutRightA_OL].counter <= dtc_filter[WCM_DigitalOutRightA_OL].passed){
                if(dtc_filter[WCM_DigitalOutRightA_OL].active){ dtc_update(WCM_DigitalOutRightA_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutRightA_OL].counter -= dtc_filter[WCM_DigitalOutRightA_OL].stepDown;
            }
        }
    }
    if((get_WiperRightOutput() == WiperXOutput_High) && (get_ParkPosSelected() == Park_bottom)){
        if(get_tle_open_load_status() & 0x0010){
            if(dtc_filter[WCM_DigitalOutRightB_OL].counter >= dtc_filter[WCM_DigitalOutRightB_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutRightB_OL].active)
                    dtc_update(WCM_DigitalOutRightB_OL, true);
            }else
                dtc_filter[WCM_DigitalOutRightB_OL].counter += dtc_filter[WCM_DigitalOutRightB_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutRightB_OL].counter <= dtc_filter[WCM_DigitalOutRightB_OL].passed){
                if(dtc_filter[WCM_DigitalOutRightB_OL].active){ dtc_update(WCM_DigitalOutRightB_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutRightB_OL].counter -= dtc_filter[WCM_DigitalOutRightB_OL].stepDown;
            }
        }
    }
    if((config_get_c_output_enable()) && ((get_WiperRightOutput() == WiperXOutput_Wash)|| ((get_WiperRightOutput() == WiperXOutput_Wash + WiperXOutput_High)))){
        if(get_tle_open_load_status() & 0x0040){
            if(dtc_filter[WCM_DigitalOutRightC_OL].counter >= dtc_filter[WCM_DigitalOutRightC_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutRightC_OL].active)
                    dtc_update(WCM_DigitalOutRightC_OL, true);
            }else
                dtc_filter[WCM_DigitalOutRightC_OL].counter += dtc_filter[WCM_DigitalOutRightC_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutRightC_OL].counter <= dtc_filter[WCM_DigitalOutRightC_OL].passed){
                if(dtc_filter[WCM_DigitalOutRightC_OL].active){ dtc_update(WCM_DigitalOutRightC_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutRightC_OL].counter -= dtc_filter[WCM_DigitalOutRightC_OL].stepDown;
            }
        }
    }

    if((get_WiperRearOutput() == WiperXOutput_High)){
        if(get_tle_open_load_status() & 0x0200){
            if(dtc_filter[WCM_DigitalOutRearA_OL].counter >= dtc_filter[WCM_DigitalOutRearA_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutRearA_OL].active)
                    dtc_update(WCM_DigitalOutRearA_OL, true);
            }else
                dtc_filter[WCM_DigitalOutRearA_OL].counter += dtc_filter[WCM_DigitalOutRearA_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutRearA_OL].counter <= dtc_filter[WCM_DigitalOutRearA_OL].passed){
                if(dtc_filter[WCM_DigitalOutRearA_OL].active){ dtc_update(WCM_DigitalOutRearA_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutRearA_OL].counter -= dtc_filter[WCM_DigitalOutRearA_OL].stepDown;
            }
        }
    }
    if((config_get_c_output_enable()) && ((get_WiperRearOutput() == WiperXOutput_Wash) || ((get_WiperRearOutput() == WiperXOutput_Wash + WiperXOutput_High)))){
        if(get_tle_open_load_status() & 0x0400){
            if(dtc_filter[WCM_DigitalOutRearC_OL].counter >= dtc_filter[WCM_DigitalOutRearC_OL].confirmed){
                if(!dtc_filter[WCM_DigitalOutRearC_OL].active)
                    dtc_update(WCM_DigitalOutRearC_OL, true);
            }else
                dtc_filter[WCM_DigitalOutRearC_OL].counter += dtc_filter[WCM_DigitalOutRearC_OL].stepUp;
        }
        else{
            if(dtc_filter[WCM_DigitalOutRearC_OL].counter <= dtc_filter[WCM_DigitalOutRearC_OL].passed){
                if(dtc_filter[WCM_DigitalOutRearC_OL].active){ dtc_update(WCM_DigitalOutRearC_OL, false);}
            }else{
                dtc_filter[WCM_DigitalOutRearC_OL].counter -= dtc_filter[WCM_DigitalOutRearC_OL].stepDown;
            }
        }
    }

}

static void dtc_washer_left(void)
{
    static int voltage;
    
    if(get_PumpLeftOutput()){
        adc_get(ADC_DIAG_WASH_LEFT, &voltage);
        LOG_DBG("\n ADC_DIAG_WASH_LEFT = %d", voltage);
        if(voltage >= config_get_washer_scgnd()){
            if(dtc_filter[WCM_SolidStateLeftPump_ScGnd].counter >= dtc_filter[WCM_SolidStateLeftPump_ScGnd].confirmed){
                if(!dtc_filter[WCM_SolidStateLeftPump_ScGnd].active)
                    dtc_update(WCM_SolidStateLeftPump_ScGnd, true);
            }else
                dtc_filter[WCM_SolidStateLeftPump_ScGnd].counter += dtc_filter[WCM_SolidStateLeftPump_ScGnd].stepUp;

        }else if (voltage < config_get_washer_ol()){
            if(dtc_filter[WCM_SolidStateLeftPump_OL].counter >= dtc_filter[WCM_SolidStateLeftPump_OL].confirmed){
                if(!dtc_filter[WCM_SolidStateLeftPump_OL].active)
                    dtc_update(WCM_SolidStateLeftPump_OL, true);
            }else
                dtc_filter[WCM_SolidStateLeftPump_OL].counter += dtc_filter[WCM_SolidStateLeftPump_OL].stepUp;

        }else{
            if(dtc_filter[WCM_SolidStateLeftPump_ScGnd].counter <= dtc_filter[WCM_SolidStateLeftPump_ScGnd].passed){
                if(dtc_filter[WCM_SolidStateLeftPump_ScGnd].active)
                    dtc_update(WCM_SolidStateLeftPump_ScGnd, false);
            }else
                dtc_filter[WCM_SolidStateLeftPump_ScGnd].counter -= dtc_filter[WCM_SolidStateLeftPump_ScGnd].stepDown;

            if(dtc_filter[WCM_SolidStateLeftPump_OL].counter <= dtc_filter[WCM_SolidStateLeftPump_OL].passed){
                if(dtc_filter[WCM_SolidStateLeftPump_OL].active)
                    dtc_update(WCM_SolidStateLeftPump_OL, false);
            }else
                dtc_filter[WCM_SolidStateLeftPump_OL].counter -= dtc_filter[WCM_SolidStateLeftPump_OL].stepDown;
        }
    }
}

static void dtc_washer_rear(void)
{
    static int voltage;
    if(get_PumpRearOutput()){
        adc_get(ADC_DIAG_WASH_REAR, &voltage);
        LOG_DBG("\n ADC_DIAG_WASH_REAR %d", voltage);
        if(voltage >= config_get_washer_scgnd()){
            if(dtc_filter[WCM_SolidStateRearPump_ScGnd].counter >= dtc_filter[WCM_SolidStateRearPump_ScGnd].confirmed){
                if(!dtc_filter[WCM_SolidStateRearPump_ScGnd].active)
                    dtc_update(WCM_SolidStateRearPump_ScGnd, true);
            }else
                dtc_filter[WCM_SolidStateRearPump_ScGnd].counter += dtc_filter[WCM_SolidStateRearPump_ScGnd].stepUp;

        }else if (voltage < config_get_washer_ol()){
            if(dtc_filter[WCM_SolidStateRearPump_OL].counter >= dtc_filter[WCM_SolidStateRearPump_OL].confirmed){
                if(!dtc_filter[WCM_SolidStateRearPump_OL].active)
                    dtc_update(WCM_SolidStateRearPump_OL, true);
            }else
                dtc_filter[WCM_SolidStateRearPump_OL].counter += dtc_filter[WCM_SolidStateRearPump_OL].stepUp;

        }else{
            if(dtc_filter[WCM_SolidStateRearPump_ScGnd].counter <= dtc_filter[WCM_SolidStateRearPump_ScGnd].passed){
                if(dtc_filter[WCM_SolidStateRearPump_ScGnd].active)
                    dtc_update(WCM_SolidStateRearPump_ScGnd, false);
            }else
                dtc_filter[WCM_SolidStateRearPump_ScGnd].counter -= dtc_filter[WCM_SolidStateRearPump_ScGnd].stepDown;

            if(dtc_filter[WCM_SolidStateRearPump_OL].counter <= dtc_filter[WCM_SolidStateRearPump_OL].passed){
                if(dtc_filter[WCM_SolidStateRearPump_OL].active)
                    dtc_update(WCM_SolidStateRearPump_OL, false);
            }else
                dtc_filter[WCM_SolidStateRearPump_OL].counter -= dtc_filter[WCM_SolidStateRearPump_OL].stepDown;
        }
    }
}

static void dtc_washer_right(void)
{
    static int voltage;
    
    if(get_PumpRightOutput()){
        adc_get(ADC_DIAG_WASH_RIGHT, &voltage);
        LOG_DBG("\n ADC_DIAG_WASH_RIGHT %d", voltage);
        if(voltage >= config_get_washer_scgnd()){
            if(dtc_filter[WCM_SolidStateRightPump_ScGnd].counter >= dtc_filter[WCM_SolidStateRightPump_ScGnd].confirmed){
                if(!dtc_filter[WCM_SolidStateRightPump_ScGnd].active)
                    dtc_update(WCM_SolidStateRightPump_ScGnd, true);
            }else
                dtc_filter[WCM_SolidStateRightPump_ScGnd].counter += dtc_filter[WCM_SolidStateRightPump_ScGnd].stepUp;

        }else if (voltage < config_get_washer_ol()){
            if(dtc_filter[WCM_SolidStateRightPump_OL].counter >= dtc_filter[WCM_SolidStateRightPump_OL].confirmed){
                if(!dtc_filter[WCM_SolidStateRightPump_OL].active)
                    dtc_update(WCM_SolidStateRightPump_OL, true);
            }else
                dtc_filter[WCM_SolidStateRightPump_OL].counter += dtc_filter[WCM_SolidStateRightPump_OL].stepUp;

        }else{
            if(dtc_filter[WCM_SolidStateRightPump_ScGnd].counter <= dtc_filter[WCM_SolidStateRightPump_ScGnd].passed){
                if(dtc_filter[WCM_SolidStateRightPump_ScGnd].active)
                    dtc_update(WCM_SolidStateRightPump_ScGnd, false);
            }else
                dtc_filter[WCM_SolidStateRightPump_ScGnd].counter -= dtc_filter[WCM_SolidStateRightPump_ScGnd].stepDown;

            if(dtc_filter[WCM_SolidStateRightPump_OL].counter <= dtc_filter[WCM_SolidStateRightPump_OL].passed){
                if(dtc_filter[WCM_SolidStateRightPump_OL].active)
                    dtc_update(WCM_SolidStateRightPump_OL, false);
            }else
                dtc_filter[WCM_SolidStateRightPump_OL].counter -= dtc_filter[WCM_SolidStateRightPump_OL].stepDown;
        }
    }
}


/* Global functions
---------------------------------------------------------------------------*/
/** @brief Step for fault_manager module
 */
void fault_manager_step(void)
{
    if(!dgn_status()){
        dtc_output_over_load_status();
        dtc_output_open_load_status();
        dtc_washer_left();
        dtc_washer_rear();
        dtc_washer_right();
    }
}
