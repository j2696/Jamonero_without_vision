/**
 * @file   can.c
 * @author Abel Tarragó
 * @date   April 2022
 * @brief  Public APIs and resources for CAN module.
 */

/* header of project specific types
---------------------------------------------------------------------------*/
#include "util.h"
#include "can_claas.h"
#include "led.h"
#include "timer.h"
#include "assert.h"
#include "can_uds.h"
#include "config.h"
#include "pwm.h"
#include "dgn.h"
#include "watchdog.h"
#include "keyb.h"
#include "application.h"
#include "eeprom.h"

/* Constant definitions
---------------------------------------------------------------------------*/
#define ENGINE_RPM_FACTOR 0.1
#define HOT_KEY_WIPER_FUNC 0x49

/* Private macro
---------------------------------------------------------------------------*/
LOG_MODULE_REGISTER(can);
/** @internal
 * @ref s_can_claas_frame fields initializer
 */
#define S_CAN_FRAME_DEF(id_, mask_, operation_) \
{ \
    .filter = { \
        .id_type = CAN_EXTENDED_IDENTIFIER, \
        .rtr = CAN_DATAFRAME, \
        .id = id_, \
        .rtr_mask = 1, \
        .id_mask = mask_, \
    }, \
    .received_time = 0, \
    .operation = &operation_, \
    .rx_work = {{{ 0 }}}, \
}

/* Project specific types
---------------------------------------------------------------------------*/
/** @brief can_state
 * Keep below @ref CAN_STATE_DEF_INIT in sync with changes here
 */
typedef struct s_can_claas_frame_t
{
    const struct zcan_filter filter;
    uint32_t received_time;
    const can_rx_callback_t operation;
    struct zcan_work rx_work;
}s_can_claas_frame;

typedef enum WiperParamSel_str_t
{
    Intermittence_Min = 0x1,
    Intermittence_Max = 0x2,
    Intermittence_SetVal = 0x3,
} WiperParamSel_str;

typedef enum WiperParamState_str_t
{
    ResponsePending = 0x4,
    RequestAccepted = 0x5,
    RequestRejected = 0x6,
    LockedOut = 0x7,
    LastFrame = 0x8,
    Error_pausing = 0xE,
    NotAvailable_pausing = 0xF

} WiperParamState_str;

typedef struct intermittence_str_t
{
    WiperParamSel_str WiperParamSel;
    WiperCtrlSet_str WiperParamSet;
    bool WiperCtrlSet_cyclic_enabled;
    WiperParamState_str WiperParamState;
} intermittence_str;

typedef struct wiper_str_t
{
    WiperCtrlSel_str WiperCtrlSel;
    WiperCtrlSet_str WiperCtrlSet;
    bool WiperCtrlSet_cyclic_enabled;
    WiperParamState_str WiperCtrlState;
} wiper_str;

/* local defined variables
-----------------------------------------------------------------------------*/
const struct device *can_dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_can_primary));
intermittence_str intermittence[4];
wiper_str wiper[8];
struct zcan_work rx_work;
enum can_state current_state;
struct can_bus_err_cnt current_err_cnt;
struct can_timing timing;
struct k_work state_change_work;
uint8_t wa_can_mcan_send = 1;
uint8_t canErrorFlag = 0;

/* last elements extracted from each one of the buffers */
can_frame_info recvFrameOh = {0};
can_frame_info recvFrameDTC = {0};
can_frame_info recvFrameIntermittence = {0};
can_frame_info recvFrameAutoWiping = {0};
can_frame_info recvFramePWM = {0};
can_frame_info recvFrameWiperreq = {0};
can_frame_info recvFrameAcknowledgement = {0};
can_frame_info recvHotKey = {0};
can_frame_info recvSwitch2B = {0};
can_frame_info recvLEDs = {0};
static bool incorrect_WiperParamSet = false;
static bool CanFrameIsReceived = false;
static uint8_t switch2BActVal_1_Left = 0;
static uint8_t switch2BActVal_2_Right = 0;
static uint16_t pwm;
static bool hotkey_recived = false;

/* external defined variables
-----------------------------------------------------------------------------*/
extern STATUS status_str;

/* Local defined functions
-----------------------------------------------------------------------------*/
/** @brief ISR for CAN bus status modification
 *  @param state enum containing the CAN status 
 *  @param err_cnt CAN bus error counter structure
 * */ 
static void state_change_isr(enum can_state state, struct can_bus_err_cnt err_cnt)
{
    LOG_DBG("ISR State %d. Errors: rx %d tx: %d", state, err_cnt.rx_err_cnt, err_cnt.tx_err_cnt);
    current_state = state;
	current_err_cnt = err_cnt;
	k_work_submit(&state_change_work);
}

/** @brief return a string depending on the requested CAN state
 *  @param state enum to check 
 *  @return char*
 * */
char *state_to_str(enum can_state state)
{
	switch (state) {
	case CAN_ERROR_ACTIVE:
		return "error-active";
	case CAN_ERROR_PASSIVE:
		return "error-passive";
	case CAN_BUS_OFF:
		return "bus-off";
	default:
		return "unknown";
	}
}

/** @brief work handler for CAN status, it is called by the ISR when the CAN status is modified
 * */
void state_change_work_handler(struct k_work *work)
{
	LOG_DBG("\nState Change ISR\nstate: %s\n"
	       "rx error count: %d\n"
	       "tx error count: %d\n",
		state_to_str(current_state),
		current_err_cnt.rx_err_cnt, current_err_cnt.tx_err_cnt);

	if (current_state == CAN_BUS_OFF) {
		LOG_DBG("Recover from bus-off\n");
		if (can_recover(can_dev, K_MSEC(100)) != 0)
			LOG_DBG("Recovery timed out\n");
	}
}

static int can_claas_attach(const struct zcan_filter *filter, const can_rx_callback_t handler, struct zcan_work *work)
{
    // TODO: work is valid pointer
    int ret = can_attach_workq(can_dev, &k_sys_work_q, work, handler, (void *)filter, filter);

    S_ASSERT(ret != CAN_NO_FREE_FILTER, ASSERT_RUNTIME, "Error initializing can module");
    return ret;
}

/** @brief */
void can_send_alive_cyclic(void)
{
    static uint8_t can_fr[8];
    static uint8_t cnt = 0;

    can_fr[0U] = cnt++; /* alive count */
    can_fr[1U] = 1U;    /* module status active */
    can_fr[2U] = 0U;
    can_fr[3U] = 0U;
    can_fr[4U] = 0x56; // WCC ID
    can_fr[5U] = MSB_SW_VER;
    can_fr[6U] = MID_SW_VER;
    can_fr[7U] = LSB_SW_VER;
    can_send_frame(cWcmsBrc_WcmmAlive, can_fr, 8U);
}

/** @brief */
void can_send_status_cyclic(void)
{
    static uint8_t can_fr[8];
    for (uint8_t index = 0; index < KEYB_MAX; index++)
        can_fr[index] = status_str.bytes.Bytes[index];
    can_send_frame(cWcmcBrc_WiperStatus, can_fr, 8U);
}

/** @brief */
void can_send_intermittence_cyclic(uint8_t intermittence_type, uint8_t WiperCtrlSet_type, RETStatus ret_val)
{
    static uint8_t can_fr[8];
    static WiperParamState_str state_response = RequestAccepted;

    if(ret_val != STATUS_OK) state_response = Error_pausing;
    else state_response = RequestAccepted;
    
    if((WiperCtrlSet_type == CyclicTransmissionOff) && (intermittence[intermittence_type].WiperCtrlSet_cyclic_enabled)){
        state_response = LastFrame;
        WiperCtrlSet_type = CyclicTransmissionOn;
    }
    
    can_fr[0U] = intermittence_type;

    if (intermittence_type == Intermittence_Min)
    {
        can_fr[1U] = (WiperCtrlSet_type << 4) | state_response;
        can_fr[2U] = (uint8_t)(get_CANIntermittencePausingTime(1) >> 24);
        can_fr[3U] = (uint8_t)(get_CANIntermittencePausingTime(1) >> 16);
        can_fr[4U] = (uint8_t)(get_CANIntermittencePausingTime(1) >> 8);
        can_fr[5U] = (uint8_t)(get_CANIntermittencePausingTime(1));
        can_fr[6U] = 0U;
        can_fr[7U] = 0U;
    }
    else if (intermittence_type == Intermittence_Max)
    {
        can_fr[1U] = (WiperCtrlSet_type << 4) | state_response;
        can_fr[2U] = (uint8_t)(get_CANIntermittencePausingTime(2) >> 24);
        can_fr[3U] = (uint8_t)(get_CANIntermittencePausingTime(2) >> 16);
        can_fr[4U] = (uint8_t)(get_CANIntermittencePausingTime(2) >> 8);
        can_fr[5U] = (uint8_t)(get_CANIntermittencePausingTime(2));
        can_fr[6U] = 0U;
        can_fr[7U] = 0U;
    }
    else if (intermittence_type == Intermittence_SetVal)
    {
        can_fr[1U] = (WiperCtrlSet_type << 4) | state_response;
        can_fr[2U] = (uint8_t)(get_CANIntermittencePausingTime(3) >> 24);
        can_fr[3U] = (uint8_t)(get_CANIntermittencePausingTime(3) >> 16);
        can_fr[4U] = (uint8_t)(get_CANIntermittencePausingTime(3) >> 8);
        can_fr[5U] = (uint8_t)(get_CANIntermittencePausingTime(3));
        can_fr[6U] = 0U;
        can_fr[7U] = 0U;
    }else
    {
        state_response = NotAvailable_pausing;
        can_fr[1U] = (WiperCtrlSet_type << 4) | state_response;
        can_fr[2U] = 0U;
        can_fr[3U] = 0U;
        can_fr[4U] = 0U;
        can_fr[5U] = 0U;
        can_fr[6U] = 0U;
        can_fr[7U] = 0U;        
    }
    can_send_frame(cWcmcVhbtWiperParam, can_fr, 8U);
}

/** @brief */
void can_send_wiper_cyclic(uint8_t wiper_type, uint8_t WiperCtrlSet_type)
{
    static uint8_t can_fr[8];

    can_fr[0U] = wiper_type;
    can_fr[1U] = (WiperCtrlSet_type << 4) | RequestAccepted;
    can_fr[2U] = keyb_get_wipercv_x(wiper_type);
    can_fr[3U] = 0;
    can_fr[4U] = 0;
    can_fr[5U] = 0;
    can_fr[6U] = 0;
    can_fr[7U] = 0U;

    can_send_frame(cWcmcVhbtWiperCtrl, can_fr, 8U);
}

/* Global functions
---------------------------------------------------------------------------*/
/** @brief Main function for sending CAN cyclic messages and CAN monitoring status */
void can_claas_step(uint8_t step)
{
    static uint8_t intermittence_id = Intermittence_Min;
    static uint8_t wiper_id = LeftWiperButton;
    struct can_bus_err_cnt err_cnt = {0};
    enum can_state state = can_get_state(can_dev, &err_cnt);

    switch (step)
    {
    case CAN_CYCLIC_500_MS:
        if (intermittence[intermittence_id].WiperCtrlSet_cyclic_enabled)
            can_send_intermittence_cyclic(intermittence_id, CyclicTransmissionOn, STATUS_OK);
        intermittence_id++;
        if (intermittence_id > Intermittence_SetVal)
            intermittence_id = Intermittence_Min;
        break;
    case CAN_CYCLIC_1000_MS:
        can_send_alive_cyclic();
        can_send_status_cyclic();
        if (wiper[wiper_id].WiperCtrlSet_cyclic_enabled)
            can_send_wiper_cyclic(wiper_id + BUTTON_ID_OFFSET, CyclicTransmissionOn);
        wiper_id++;
        if (wiper_id > RightPumpButton)
            wiper_id = LeftWiperButton;
        break;
    default:
        break;
    }

    LOG_DBG("\nState %d. Errors: rx %d tx: %d", state, err_cnt.rx_err_cnt, err_cnt.tx_err_cnt);
    if (state == CAN_BUS_OFF)
    {
        LOG_DBG("Recover from bus-off\n");
        if (can_recover(can_dev, K_MSEC(100)) != 0)
            LOG_DBG("Recovery timed out\n");
        // sys_reboot(0U);
    }
}

static void handle_cVhbt(struct zcan_frame *frame, void *arg)
{
    struct zcan_filter *filter = arg;
    s_can_claas_frame *canframe = CONTAINER_OF(filter, s_can_claas_frame, filter);
    canframe->received_time = k_uptime_get_32();
    CanFrameIsReceived = true;
    clearCanErrorFlag();

    switch(frame->id) {
        case cVcmcBrc_CntHours1Total:
            config_set_working_hour(frame->data);
        break;
        case cChtcBrc_ChutePositionDeg:
            if (frame->data[6] == 0x00)
                set_AutoWipingActive(0);
            else if (frame->data[6] == 0x20)
                set_AutoWipingActive(2); // right
            else if (frame->data[6] == 0x10)
                set_AutoWipingActive(1); // left
            else if (frame->data[6] == 0x30)
                set_AutoWipingActive(3); // both
        break;
        case cVhbtBrc_LocalizationSettings:
            pwm = ((frame->data[1] << 8) | frame->data[2]);
            pwm_set(pwm);
            led_set_bright_value(pwm);
        break;
        case cVhbtBrc_HotkeyStatus:
            if (frame->data[0] == HOT_KEY_WIPER_FUNC){
                keyb_hotKey(switch2BActVal_1_Left, switch2BActVal_2_Right);
                hotkey_recived = true;
            }
        break;
    }
}
static s_can_claas_frame state_cVhbt = S_CAN_FRAME_DEF(CANID_cVhbt_id, CANID_cVhbt_mask, handle_cVhbt);

static void handle_cVbcc(struct zcan_frame *frame, void *arg)
{
    struct zcan_filter *filter = arg;
    s_can_claas_frame *canframe = CONTAINER_OF(filter, s_can_claas_frame, filter);
    canframe->received_time = k_uptime_get_32();
    CanFrameIsReceived = true;
    clearCanErrorFlag();
    
    switch(frame->id) {
        case cVbccBrc_AlarmMonitoringStatus:
             dgn_set_alarm_monitoring_status(frame->data);
        break;
        case cVbccWcmsAlarmAck:
            dgn_set_Alarm_Acknowledgement();
        break;
    }   
}
static  s_can_claas_frame state_cVbcc = S_CAN_FRAME_DEF(CANID_cVbcc_id, CANID_cVbcc_mask, handle_cVbcc);

static void handle_cVhbtWcmcWiper(struct zcan_frame *frame, void *arg)
{
    struct zcan_filter *filter = arg;
    s_can_claas_frame *canframe = CONTAINER_OF(filter, s_can_claas_frame, filter);
    canframe->received_time = k_uptime_get_32();
    CanFrameIsReceived = true;
    clearCanErrorFlag();
    RETStatus ret_val = STATUS_OK;
    static uint8_t reset_value[8U];
    
     switch(frame->id) {
        case cVhbtWcmcWiperParam:
            intermittence[frame->data[0]].WiperParamSel = frame->data[0];
            intermittence[frame->data[0]].WiperParamSet = (WiperCtrlSet_str)((frame->data[1] & 0Xf0) >> 4);
            intermittence[frame->data[0]].WiperParamState = (frame->data[1] & 0X0f);

            uint32_t intermittence_SetVal = frame->data[2] << 24 | frame->data[3] << 16 | frame->data[4] << 8 | frame->data[5];

            if (intermittence[frame->data[0]].WiperParamSet == CyclicTransmissionOn){        
                can_send_intermittence_cyclic(frame->data[0], CyclicTransmissionOn, STATUS_OK);
                intermittence[frame->data[0]].WiperCtrlSet_cyclic_enabled = true;
            }else if (intermittence[frame->data[0]].WiperParamSet == CyclicTransmissionOff){
                can_send_intermittence_cyclic(frame->data[0], CyclicTransmissionOff, STATUS_OK);
                intermittence[frame->data[0]].WiperCtrlSet_cyclic_enabled = false;
                can_send_intermittence_cyclic(frame->data[0], CyclicTransmissionOff, STATUS_OK);
            }else if (intermittence[frame->data[0]].WiperParamSet == ReadParameter)
                can_send_intermittence_cyclic(frame->data[0], ReadParameter, STATUS_OK);
            else if (intermittence[frame->data[0]].WiperParamSet == WriteParameter){
                if( get_CANIntermittencePausingTime(frame->data[0]) != intermittence_SetVal){
                    ret_val = set_IntermittencePausingTime(intermittence_SetVal, frame->data[0]);
                }else{}
                can_send_intermittence_cyclic(frame->data[0], WriteParameter, ret_val);
            }
            else if (intermittence[frame->data[0]].WiperParamSet == ResetParameter)
            {
                incorrect_WiperParamSet = false;
                switch (frame->data[0])
                {
                case Intermittence_Max:
                    eeprom_read(MAX_INTERVAL_TIME, reset_value);
                    break;
                case Intermittence_Min:
                    eeprom_read(MIN_INTERVAL_TIME, reset_value);
                    break;
                case Intermittence_SetVal:
                    eeprom_read(PRESENT_INTERVAL_TIME, reset_value);
                    break;
                }
                intermittence_SetVal = (reset_value[0] << 8) | reset_value[1];
                can_send_intermittence_cyclic(frame->data[0], ResetParameter, STATUS_OK);
                ret_val = set_IntermittencePausingTime(intermittence_SetVal, frame->data[0]);            
            }else can_send_intermittence_cyclic(frame->data[0], ((frame->data[1] & 0Xf0) >> 4), STATUS_ERROR);
        break;
        case cVhbtWcmcWiperCtrl:
            wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSel = frame->data[0];
            wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet = (WiperCtrlSet_str)((frame->data[1] & 0Xf0) >> 4);

            if (wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet == CyclicTransmissionOn){
                wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet_cyclic_enabled = true;
            }
            else if (wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet == CyclicTransmissionOff){
                wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet_cyclic_enabled = false;
            }
            else if (wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet == ReadParameter){
                can_send_wiper_cyclic(frame->data[0], ReadParameter);
            }
            else if (wiper[frame->data[0]-BUTTON_ID_OFFSET].WiperCtrlSet == WriteParameter){
                keyb_set_data(frame->data);
            }
        break;
    }

    
}
static  s_can_claas_frame state_cVhbtWcmcWiper = S_CAN_FRAME_DEF(CANID_cVhbtWcmcWiper_id, CANID_cVhbtWcmcWiper_mask, handle_cVhbtWcmcWiper);

static void handle_switch2B(struct zcan_frame *frame, void *arg)
{
    struct zcan_filter *filter = arg;
    s_can_claas_frame *canframe = CONTAINER_OF(filter, s_can_claas_frame, filter);
    canframe->received_time = k_uptime_get_32();
    CanFrameIsReceived = true;
    clearCanErrorFlag();

    switch2BActVal_1_Left = (frame->data[1] & 0xF0) >> 4;
    switch2BActVal_2_Right = (frame->data[3] & 0xF0) >> 4;
    if(hotkey_recived) keyb_hotKey(switch2BActVal_1_Left, switch2BActVal_2_Right);
}
static  s_can_claas_frame switch2B = S_CAN_FRAME_DEF(cVuicMuicSwitch2B, CAN_EXT_ID_MASK, handle_switch2B);

static void handle_statusLEDs(struct zcan_frame *frame, void *arg)
{
    struct zcan_filter *filter = arg;
    s_can_claas_frame *canframe = CONTAINER_OF(filter, s_can_claas_frame, filter);
    canframe->received_time = k_uptime_get_32();
    CanFrameIsReceived = true;
    clearCanErrorFlag();

    set_FL_Left_01((frame->data[0] & 0x10) >> 4);
    set_FL_Left_02((frame->data[0] & 0x20) >> 5);
    set_FL_Rear_03((frame->data[1] & 0x10) >> 4);
    set_FL_Rear_04((frame->data[1] & 0x20) >> 5);
    set_FL_Right_05((frame->data[2] & 0x10) >> 4);
    set_FL_Right_06((frame->data[2] & 0x20) >> 5);
    set_FL_Auto_07((frame->data[3] & 0x10) >> 4);
    set_FL_Auto_08((frame->data[3] & 0x20) >> 5);
    set_FL_Park_09((frame->data[4] & 0x10) >> 4);
    set_FL_Park_10((frame->data[4] & 0x20) >> 5);
}
static  s_can_claas_frame statusLED = S_CAN_FRAME_DEF(cCoecWcmcStatusLEDs, CAN_EXT_ID_MASK, handle_statusLEDs);

bool get_CanFrameIsReceived(void){ return CanFrameIsReceived;}
void set_CanFrameIsReceived(bool value){ CanFrameIsReceived = value;}

/** @brief Initialization function for CAN module */
void can_claas_init(void)
{
    MODULE_SINGLE_INIT;

    S_ASSERT(device_is_ready(can_dev), ASSERT_RUNTIME, "Invalid can device");

    can_register_state_change_isr(can_dev, state_change_isr);
    k_work_init(&state_change_work, state_change_work_handler);

    can_claas_attach(&state_cVhbt.filter, state_cVhbt.operation, &state_cVhbt.rx_work);
    can_claas_attach(&state_cVbcc.filter, state_cVbcc.operation, &state_cVbcc.rx_work);
    can_claas_attach(&state_cVhbtWcmcWiper.filter, state_cVhbtWcmcWiper.operation, &state_cVhbtWcmcWiper.rx_work);
    can_claas_attach(&switch2B.filter, switch2B.operation, &switch2B.rx_work);
    can_claas_attach(&statusLED.filter, statusLED.operation, &statusLED.rx_work);

    intermittence[Intermittence_Min].WiperParamSet = CyclicTransmissionOn;
    intermittence[Intermittence_Max].WiperParamSet = CyclicTransmissionOn;
    intermittence[Intermittence_SetVal].WiperParamSet = CyclicTransmissionOn;
}

void can_class_irq_send_cb (uint32_t error_flags, void *arg){
    /* Passing through here when the transmission is successful, not running it in the case there is an error*/
    if(error_flags)
        LOG_DBG("CAN TX error %d, in %s \n\n", error_flags, (char *)arg);
}

void can_send_frame(const uint32_t message_id, const uint8_t *data, const uint8_t dlc)
{
    struct zcan_frame can_frame = {
        .id_type = CAN_EXTENDED_IDENTIFIER,
        .rtr = CAN_DATAFRAME,
        .id = message_id,
        .dlc = dlc,
        .data = {0xFF},
    };

    memcpy(can_frame.data, data, dlc);
    static can_tx_callback_t cb_can_class_send = (void*) &can_class_irq_send_cb;
    int bFlagSt = CAN_TX_OK;

    if(canErrorFlag == 0){
        bFlagSt = can_send(can_dev, &can_frame, K_MSEC(1), cb_can_class_send, "WCC-Sender");
        if (bFlagSt != CAN_TX_OK) {
            LOG_DBG("\n[ ret = %d ] retrying...%d", bFlagSt, wa_can_mcan_send);
            wa_can_mcan_send++;
            canErrorFlag = 1;
        }
        else { wa_can_mcan_send = 1;}
    }
}

/** @brief set canErrorFlag to its default value
 * */
void clearCanErrorFlag(void){
    canErrorFlag = 0;
}

/** @brief getter for canErrorFlag
 * */
uint8_t getCanErrorFlag(void){
    return canErrorFlag;
}

/** @brief getter for wa_can_mcan_send
 * */
uint8_t getWaCanMcanSend (void){
    return wa_can_mcan_send;
}

/** @brief setter for wa_can_mcan_send
 * */
void setWaCanMcanSend (uint8_t value){
    wa_can_mcan_send = value;
}
