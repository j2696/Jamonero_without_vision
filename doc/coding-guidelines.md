# Base guideliness

Use [Zephyr Coding Style](https://docs.zephyrproject.org/latest/contribute/index.html#coding-style) unless other indication is stated in this document.

# MISRA-C

In the project, [Zephyr MISRA C rules](https://docs.zephyrproject.org/latest/guides/coding_guidelines/index.html#main-rules) will be observed.
In case of doubt, it should be pointed and discussed by the team

# Coding style

## Indentation, braces, newlines

Always use spaces for indentation. Indentation blocks will be multiple of 4 spaces.

Braces for if/switch/while code blocks starts in newline and this newline is specific for the (curly) brace. Closing curly brace will we place in another specific newline

Braces can be avoided for single line code blocks:

```c
    if(condition)
        function_call();
```

There should be a newline before function defintion. function definition includes documentation block

## Documenting code

[Doxygen](https://www.doxygen.nl/manual/starting.html) syntax will be used. [Doxygen commands](https://www.doxygen.nl/manual/commands.html)

[Documentation block](https://www.doxygen.nl/manual/docblocks.html#cppblock) will start with `/**` or `/**<` for inline doc doxygen keyword will use the `@` syntax (eg: `@brief`)


## Type and variable names

```c
typedef struct param_t
{
  uint16_t field1;
  bool error;
} param;
```

```c
typedef enum values_t
{

} values;
```

# Commit messages

```
<topic> Short description

Some longer description line 1
Some longer description line 1
```

Short description examples:

`adc: Fix adc_get function parameter handling`

`tests/spi: Improve test coverage`


# Coding review

* Tips on [Merge requests](https://docs.gitlab.com/ee/development/code_review.html)

## Coding best practices

* Avoid dynamic memory usage (heap or slab allocation).
* Encapsulate modules. Do not expose internal data/logic/functions. E.g.: Use `static`
* Always initialize defined variables. E.g.: Set it to zero or other known value
* Use const when possible, ie: when variable value shall not change `int function(const int param)`
* Single entry and exit for a function.
* Avoid global variables.
* Avoid type conversions. Generally speaking, variables should have the right type and should not be converted to other type
