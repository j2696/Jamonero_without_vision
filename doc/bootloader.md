# bootloader

This is a customized bootloader supporting:

* Low power modes
* Software upgrade via UDS/CAN
* Application image integrity check
* Application booting

# Bootloader configuration block

The bootloader requires the application binary to provide a configuration block containing important information for the bootloader to check that application image is valid. Typically this configuration block consists on the following fields:

| offset | size | field | description |
|-------:|:----:|------:|-------------|
|  0 | 4 | sizeOfApplication | |
|  4 | 2 | crc | |
|  6 | 2 | reserved1  | |
|  8 | 4 | od1018Vendor | |
| 12 | 4 | od1018Product | |
| 16 | 4 | od1018Version | |
| 20 | 4 | crc32 | |

The configuration block can include additional stuffing, usually made up of random bytes.

# Binaries layout

## Components

* zephyr.bin: application + zephyr OS
* cb_zephyr.bin: Configuration block + zephyr.bin
* Bootloader.bin: bootloader, typically 41KB
* full.bin: Consolidated binary including bootloader, configuration block and application

# Binary/flash map 

| offset | size | component |
|-------:|:----:|----------:|
| 0  | ~41KB | bootloader |
| 0xF800  | 0x800 | configuration block |
| 0x10000 | 150KB | application binary |

# Bootloader build targets

## all (default target)

Adds configuration block to the `zephyr.bin`. Creating the `cb_zephyr.bin` binary

The configuration block and the output binary is created by invoking the `appendconfigblock` tool.

## fullbinary

`fullbinay` is a new target that arranges bootloader and `cb_zephyr.bin` into a single binary that can be flashed into the flash.

The target can be inkoved via `west`:

`west build -t fullbinary`

The `full.bin` will be typically available at `build/zephyr/`

Note that as the application entry point is `0x10000` the `cb_zephyr.bin` should be placed at offset 0xF800. 0xF800 is the result of substracting configuration block size 0x800 to the application entry point. This leaves a slot of up to 62KB (0xF800) for bootloader.
