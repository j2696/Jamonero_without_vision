﻿/*
* uds_core.c - contains main uds functions
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file uds_core.c
* \brief main uds routines
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_commtask.h>
#include <co_timer.h>
#include <iso_tp.h>
#include <ico_event.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>

#include "iuds_core.h"
#include "iuds_updown.h"
#include "iuds_comm.h"
#include "iuds_datatrans.h"
#include "iuds_routinectrl.h"

#include <bl_user.h>

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

typedef struct {
    uint8_t expResp;
    uint8_t lastResp;
} UDS_DEVICE_T;

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/
#ifdef POLLING
extern void icoQueueHandler(void);
#endif /* POLLING */

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static RET_T isoTpServerSplitIndication (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize,
        uint16_t  tranferSize           ) ;

static RET_T isoTpClientIndikation (
        uint8_t   addr,
        uint8_t  *pData,
        RET_T     result           )      ;

static RET_T isoTpClientTxAckInd (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  size           )        ;

static RET_T iisoTpMsgHandler (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize    )           ;

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/
static uint8_t       isoTpBuf[UDS_BUFFER_SIZE];

static CO_EVENT_T    sessionEvent;

static UDS_DEVICE_T  udsDevice;


/******************************************************************************
* \brief udsStackInit -
*
*/
RET_T udsStackInit (
        void     )
{
    RET_T retVal;

    //DBG_RAM_Init();

    icoEventInit();

    isoTpInit();

    retVal = isoTpEventRegister_SERVER(iisoTpMsgHandler);
    if (retVal != RET_OK) {
        return (retVal);
    }

    retVal = isoTpEventRegister_CLIENT(isoTpClientIndikation);
    if (retVal != RET_OK) {
        return (retVal);
    }

    retVal = isoTpEventRegister_CLIENT_TXACK(isoTpClientTxAckInd);
    if (retVal != RET_OK) {
        return (retVal);
    }

    if (isoTpEventRegister_SERVER_SPLIT(isoTpServerSplitIndication) != RET_OK) {
        exit(16);
    }

    retVal = isoTpInitServerChannel( ISOTP_SERVER_PHYS_ADDR_REQ_CANID, ISOTP_SERVER_PHYS_ADDR_RESP_CANID,
                                      1u, 1u, ISOTP_ADDRESS_PHYS, ISOTP_FORMAT_NORMAL, &isoTpBuf[0],
                                      UDS_BUFFER_SIZE );
    if (retVal != RET_OK) {
        return (retVal);
    }

    retVal = isoTpInitClientChannel( ISOTP_CLIENT_PHYS_ADDR_REQ_CANID, ISOTP_CLIENT_PHYS_ADDR_RESP_CANID,
                                     1u, 1u, ISOTP_ADDRESS_PHYS, ISOTP_FORMAT_NORMAL, &isoTpBuf[0],
                                     UDS_BUFFER_SIZE );
    if (retVal != RET_OK) {
        return (retVal);
    }

    (void)icoEventStart(&sessionEvent, iudsCommEventSession, NULL);

    return (RET_OK);
}


/******************************************************************************
* \brief iisoTpMsgHandler -
*
*/
static RET_T iisoTpMsgHandler (
        uint8_t   addr,         /* source address */
        uint8_t  *pData,        /* pointer to data */
        uint16_t  dataSize      /* valid data size */ )
{
    /*
    bl_dbgvect[0U] = bl_dbg_cnt;
    
    //DBG_RAM_Trace_3 ( (dataSize >> 8U), dataSize, 0xEE );

    //DBG_RAM_Trace_3 ( pData[0U], pData[1U], pData[2U] );

    //DBG_RAM_Trace_4 ( pData[3U], pData[4U], pData[5U], pData[6U] );
    */

    /* complete message in buffer */
    if (dataSize <= UDS_BUFFER_SIZE) {
        (void)iudsMsgHandler(addr, pData, dataSize);
    }

    return (RET_OK);
}


/******************************************************************************
* \brief isoTpServerSplitIndication -
*
*/
static RET_T isoTpServerSplitIndication (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize,
        uint16_t  tranferSize                 )
{
    (void)addr; /* Remove Warning */
    (void)pData; /* Remove Warning */
    (void)dataSize; /* Remove Warning */
    (void)tranferSize; /* Remove Warning */

    /*
    bl_dbgvect[0U] = bl_dbg_cnt;
    
    //DBG_RAM_Trace_3 ( (dataSize >> 8U), dataSize, 0xDD );

    //DBG_RAM_Trace_3 ( (tranferSize >> 8U), tranferSize, 0xEE );

    //DBG_RAM_Trace_3 ( pData[0U], pData[1U], pData[2U] );

    //DBG_RAM_Trace_4 ( pData[3U], pData[4U], pData[5U], pData[6U] );
    */

    return (RET_OK);
}


/******************************************************************************
* \brief isoTpClientIndikation -
*
*/
static RET_T isoTpClientIndikation (
        uint8_t  addr,      /* source address */
        uint8_t *pData,     /* pointer to data */
        RET_T    result     /* result of transfer */ )
{
    (void)addr;   /* Remove Warning */
    (void)pData;  /* Remove Warning */
    (void)result; /* Remove Warning */

    return (RET_OK);
}


/******************************************************************************
* \brief isoTpClientTxAckInd -
*
*/
static RET_T isoTpClientTxAckInd (
        uint8_t   addr,         /* source address */
        uint8_t  *pData,        /* pointer to data */
        uint16_t  size          /* valid data size */ )
{
    if (size == 0u) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    switch (pData[0])
    {
    case UDS_RESP_SESSION_CONTROL:
    case UDS_RESP_ECU_RESET:
        if (size >= 2u) {
            iudsCommMsgAck(addr, pData);
        }
        break;
    default:
        break;
    }
    return (RET_OK);
}


/******************************************************************************
* \brief udsSendResponsePending -
*
*/
RET_T udsSendResponsePending (
        uint8_t addr,
        uint8_t reqType           )
{
    uint8_t data[3];

    data[0] = UDS_NRC_SID;
    data[1] = reqType;
    data[2] = UDS_NRC_RCRRP;

    isoTpSendReq(addr, &data[0], 3u, 0u);
#ifdef POLLING
    icoQueueHandler();
#endif /* POLLING */

    return (RET_OK);
}


/******************************************************************************
* \brief iudsMsgHandler -
*
*/
RET_T iudsMsgHandler (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize     )
{
    /* valid uds message received, restart session timeout */
    (void)iudsCommRestartTimeOut();

    udsDevice.expResp = UDS_RESP_INVALID;
    udsDevice.lastResp = UDS_RESP_INVALID;
    if ((pData[1u] & UDS_SUB_NO_RESP_BIT) == 0u) {
        udsDevice.expResp = pData[0] + 0x40;
    }

    switch (pData[0u]) {
    case UDS_REQ_SESSION_CONTROL:
    case UDS_REQ_ECU_RESET:
    case UDS_REQ_TESTER_PRESENT:
    case UDS_REQ_SEC_ACCESS_SEED:
    case UDS_REQ_ControlDTCSetting:
    case UDS_REQ_COMM_CONTROL:
        iudsCommMsgHandler(addr, pData, dataSize);
        break;

    case UDS_REQ_ROUTINE_CTRL:
        iudsRoutineControlHandler(addr, pData, dataSize);
        break;

    case UDS_REQ_DOWNLOAD:
    case UDS_TRANSFER_DOWNLOAD:
    case UDS_REQ_TRANSFER_EXIT:
        udsDevice.expResp = pData[0] + 0x40;
        iudsUpDownMsgHandler(addr, pData, dataSize);
        break;

    case UDS_REQ_READ_BY_ID:
    case UDS_REQ_WRITE_BY_ID:
        udsDevice.expResp = pData[0] + 0x40;     
        iudsDataTransMsgHandler(addr, pData, dataSize);
        break;


    default:
        break;
    }

    if (udsDevice.lastResp != udsDevice.expResp) {
        pData[0] = udsDevice.expResp;
        iudsSendResponse(addr, pData, dataSize, 0u);
    }

    /*
    bl_dbgvect[0U] = bl_dbg_cnt;
    
    //DBG_RAM_Trace_3 ( (dataSize >> 8U), dataSize, 0xEE );

    //DBG_RAM_Trace_3 ( pData[0U], pData[1U], pData[2U] );

    //DBG_RAM_Trace_4 ( pData[3U], pData[4U], pData[5U], pData[6U] );
    */

    return (RET_OK);
}


/******************************************************************************
* \brief iudsSendResponseWA -
*
*/
RET_T iudsSendResponseWA (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize,
        uint8_t   flags      )
{
    if (udsDevice.lastResp != udsDevice.expResp) {
        pData[0] = udsDevice.expResp;
        isoTpSendReqWA(addr, pData, dataSize, flags);
    }
    udsDevice.expResp = UDS_RESP_INVALID;
    udsDevice.lastResp = UDS_RESP_INVALID;

    return (RET_OK);
}


/******************************************************************************
* \brief iudsSendResponse -
*
*/
RET_T iudsSendResponse(
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize,
        uint8_t   flags      )
{
    if (udsDevice.lastResp != udsDevice.expResp) {
        pData[0] = udsDevice.expResp;

        //DBG_RAM_Trace_3 ( 0xEB, pData[0U], dataSize );

        isoTpSendReq(addr, pData, dataSize, flags);
    }
    udsDevice.expResp = UDS_RESP_INVALID;
    udsDevice.lastResp = UDS_RESP_INVALID;

    return (RET_OK);
}


/******************************************************************************
* \brief iudsSendAbort - abort uds transfer
*
* \return RET_OK
*
*/
RET_T iudsSendAbort (
        uint8_t  addr,
        uint8_t *pData,
        uint8_t  reason,
        uint8_t  flags    )
{
    /* copy service request */
    pData[1u] = pData[0u];
    /* set abort service */
    pData[0u] = UDS_NRC_SID;
    /* set abort reason */
    pData[2] = reason;

    /* reset automatism */
    udsDevice.expResp = UDS_RESP_INVALID;
    udsDevice.lastResp = UDS_RESP_INVALID;

    /* send abort message */
    isoTpSendReq(addr, pData, 3u, flags);
#ifdef POLLING
    //icoQueueHandler();
#endif /* POLLING */

    return (RET_OK);
}


/******************************************************************************
* \brief iudsSendRetAbort - abort uds transfer with RET value translation
*
* \return RET_OK
*
*/
RET_T iudsSendRetAbort (
        uint8_t  addr,
        uint8_t *pData,
        RET_T    retReason,
        uint8_t  flags        )
{
    (void)flags; /* Remove Warning */

typedef struct {
    RET_T    reason;       /* error reason */
    uint8_t  abortCode;    /* uds abort code */
} UDS_ABORT_CODE_TABLE_T;

    static const UDS_ABORT_CODE_TABLE_T abortCodeTable[] = {
        { RET_UDS_NRC_GR,       UDS_NRC_GR      },
        { RET_UDS_NRC_SNS,      UDS_NRC_SNS     },
        { RET_UDS_NRC_SFNS,     UDS_NRC_SFNS    },
        { RET_UDS_NRC_IMLOIF,   UDS_NRC_IMLOIF  },
        { RET_UDS_NRC_RTL,      UDS_NRC_RTL     },
        { RET_UDS_NRC_BRR,      UDS_NRC_BRR     },
        { RET_UDS_NRC_CNC,      UDS_NRC_CNC     },
        { RET_UDS_NRC_RSE,      UDS_NRC_RSE     },
        { RET_UDS_NRC_NRFSC,    UDS_NRC_NRFSC   },
        { RET_UDS_NRC_FPEORA,   UDS_NRC_FPEORA  },
        { RET_UDS_NRC_ROOR,     UDS_NRC_ROOR    },
        { RET_UDS_NRC_SAD,      UDS_NRC_SAD     },
        { RET_UDS_NRC_IK,       UDS_NRC_IK      },
        { RET_UDS_NRC_ENOA,     UDS_NRC_ENOA    },
        { RET_UDS_NRC_RTDNE,    UDS_NRC_RTDNE   },
        { RET_UDS_NRC_UDNA,     UDS_NRC_UDNA    },
        { RET_UDS_NRC_TDS,      UDS_NRC_TDS     },
        { RET_UDS_NRC_GPF,      UDS_NRC_GPF     },
        { RET_UDS_NRC_WBSC,     UDS_NRC_WBSC    },
        { RET_UDS_NRC_RCRRP,    UDS_NRC_RCRRP   },
        { RET_UDS_NRC_SFNSIAS,  UDS_NRC_SFNSIAS }
    };
    uint8_t   abortCode = RET_UDS_NRC_GR;
    uint16_t  i;

    for (i = 0u; i < (sizeof(abortCodeTable) / sizeof(UDS_ABORT_CODE_TABLE_T)); i++) {
        if (abortCodeTable[i].reason == retReason) {
            abortCode = abortCodeTable[i].abortCode;
            break;
        }
    }

    (void)iudsSendAbort(addr, pData, abortCode, 0u);

    return (RET_OK);
}
