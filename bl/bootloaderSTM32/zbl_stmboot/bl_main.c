/*
* bl_main.c
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file bl_main.c
* \brief main entry point
*/

/* hardware specific includes and settings
-----------------------------------------------------------------------------*/

/* standard includes
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* header of project specific types
-----------------------------------------------------------------------------*/
#include <bl_config.h>

#include <bl_type.h>
#include <bl_debug.h>
#include <bl_flash.h>
#include <bl_crc.h>
#include <bl_application.h>
#include <bl_call.h>
#include <bl_timer.h>
#ifdef CFG_CAN_BACKDOOR
#include <bl_canbackdoor.h>
#endif

#include <co_datatype.h>
#include <co_timer.h>
#include <bl_main.h>
#include <bl_uds.h>

#include <stm32g0xx.h>

#include <stm32g0xx_hal_dma.h>
#include <stm32g0xx_hal_rcc.h>
#include <stm32g0xx_hal_tim.h>

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/

/* external variables
-----------------------------------------------------------------------------*/

extern RTC_HandleTypeDef hrtc;

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/


/******************************************************************************
* \brief bl_main - main entry
*
*/
int bl_main(void)
{
    BlRet_t ret;
    Flag_t  callFlag;
    BOOL_T bWakeUpPinEvent = CO_FALSE;
    BOOL_T bAvoidFARCheck, bAllowJumping2Appl = CO_FALSE;

    /* disable all interrupts, if not disabled after reset */
    __disable_irq();

    /* initialize bl_command[] 'NNNN' ECC == 0
     * if not initialized */
    resetUnknownCommand();

    /* get reset reason */
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PWRRST))  {
        /* save reset reason */
        bl_command[RESET_REG_IDX] = RCC_FLAG_PWRRST;
    }

    callFlag = stayInBootloader();

    bAvoidFARCheck = (callFlag == BL_COMMAND_NOFAR) ? CO_TRUE : CO_FALSE;

    if ((callFlag) && (callFlag < BL_COMMAND_BL)) {

        setNoneCommand();

        /* awakening GPIOC pin-events have happened ? */
        bWakeUpPinEvent = CO_TRUE;

        if (bWakeUpPinEvent == CO_FALSE) {

            uint32_t u32cntDR1 = __HAL_RCC_GET_FLAG(RCC_FLAG_PWRRST) != RESET ? 1U : HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR1) + 1U;

            if (u32cntDR1 % 1U == 0U) {  /* WD external: refresh period (1 count) ~ 768ms */
                HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);
                for (uint32_t u32cnt = 0U; u32cnt < 0x64; u32cnt++) { asm volatile("nop"); } /* 10 us pulse */
                HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_1);
            }

            HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, u32cntDR1);

            /* back to low power mode */
            mainResumeLowPower();
        }
        else                 
            callFlag = BL_COMMAND_START;        
    }

    if (callFlag == BL_COMMAND_START) {

        /* clear flags */
        __HAL_RCC_CLEAR_RESET_FLAGS();

        /* check CRC of all domains */
        ret = mainChecksumCheck();
        if (ret == BL_RET_OK)
        {
            /* Wait for possible FAR condition (Flash After Reset) */
            udsSetFARstatus(CO_FALSE);
            /* Conditions for starting Appl are correct */
            if (bAvoidFARCheck == CO_TRUE)
                jump2Appl();
            else
                bAllowJumping2Appl = CO_TRUE;

        } else {
            /* wrong application */
        }
    }


    /* reset stay in boot loader marker */
    setStartCommand();
    
    /* UDS / Hardware Initialization */
    if (udsInit() != BL_RET_OK)  {
        while(1);
    }

    /* initialize - at the first - hardware */
    ret = flashInit(0u);
    if (ret != BL_RET_OK)  {
        while(1);
    }

    /* endless loop */
    do {

        if (bAllowJumping2Appl == CO_TRUE) {
            /* If a valid appl has been detected jum2Appl() is called after FAR window */
            if (udsIsFARtimeElapsed() == CO_TRUE) {
                setNoFARCommand();
                __HAL_RCC_CLEAR_RESET_FLAGS();
                USER_RESET();
            }
        }
        
        if (udsIsFblPingTimeoutElapsed() == CO_TRUE) {
            setNoFARCommand();
            __HAL_RCC_CLEAR_RESET_FLAGS();
            USER_RESET();
        }        

        /* UDS handler */
        udsCyclic();

        /* flash handler */
        flashCyclic();

    } while (1);

    return (0);
}


/******************************************************************************
* \brief mainResumeLowPower - resuming the Low Power Standby Mode
*
*/
void mainResumeLowPower(void)
{
    setUSLECommand();
    __HAL_RCC_CLEAR_RESET_FLAGS();
    USER_RESET();
}


/******************************************************************************
* \brief mainFlashTag - stores a tag in program flash
*
*/
void mainFlashTag (
        uint32_t tagAddress,
        uint8_t  byte     )
{
    uint8_t pFldata[8U] = {0};

    memset(pFldata, byte, 8U);
    setFlash_custom_valFlashAddress(tagAddress);

    flashInit(0u);

    uint32_t cntblmain = 0U;

    do {
            flashCyclic();
            if (cntblmain == 32U) {
                flashPage(&pFldata[0], 8U, FLASH_BLOCK_FUNCTION);
            }

    } while (cntblmain++ < 1024);
}


/******************************************************************************
* \brief mainChecksumCheck - customer specific checksum check
*
* \retval BL_RET_OK application and required domains are correct in the Flash
*/
BlRet_t mainChecksumCheck(void)
{
    BlRet_t ret = BL_RET_OK;
#if BL_DOMAIN_COUNT > 1
    uint8_t i;
#endif

#ifdef BL_CHECK_PRODUCTID
    ret = applCheckImage();
#endif /* BL_CHECK_PRODUCTID */

    if ( ret == BL_RET_OK ) {
        ret = applCheckChecksum(0u);
    }

    if ( ret == BL_RET_OK )
    {
#ifdef BL_DOMAIN_COUNT
        for( i = 1; i < BL_DOMAIN_COUNT ; i++ )
        {
            if ( ret == BL_RET_OK )
            {
                ret = applCheckChecksum(i);
            }
        }
#endif /* BL_DOMAIN_COUNT */
    }
    return (ret);
}


/***************************************************************************/
/**
* \brief jump2Appl - jump to application
*
*
* no return
*
*/


/******************************************************************************
* \brief jump2Appl - jump to application
*
*/
void jump2Appl(void)
{
    typedef void (*t_pFunctionNoReturn)(void) __attribute__((noreturn));

    uint32_t topOfStack;

#ifdef BL_WAIT_FOR_START
    setBootCommand();   /* stay in BL, if the appl don't change this value */
#else
    setStartCommand();  /* call application - overwrite last command */
#endif

#ifdef CFG_LOCK_USED
    unlockException();
#endif
#ifdef CFG_CAN_BACKDOOR
    setBackdoorCommand(BL_BACKDOOR_DEACTIVATE);
#endif

    topOfStack = *(uint32_t*)applFlashStart(0u);

    __set_MSP(topOfStack);

    /*
     * At this point we use the new Stack!
     * This Stack can be located at a different place.
     * All of the RAM variables from the Bootloader could be corrupt!
     * In case of problems you have to check the disassembler source.
     * The register variables should be optimized to register use.
     * Do not add additional commands without checking in this sequence.
     */
    register t_pFunctionNoReturn  pAppl;
    register uint32_t             resetHandler;

    /* Register Program Counter 'll be set to appl reset vector [ 0x08010004 ] */
    resetHandler = (uint32_t)applFlashStart(0u) + 4ul;
    pAppl = (t_pFunctionNoReturn)(*(uint32_t *)resetHandler);
    pAppl();
    while(1)  { } // never reach this point

}
