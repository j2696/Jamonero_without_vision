/*
* co_queue.c - contains functions for queue handling
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file bl_queue.c
* \brief contains functions for queue handling
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <string.h>

/* header of project specific types
-----------------------------------------------------------------------------*/
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <co_drv.h>
#include <co_commtask.h>
#include "ico_common.h"
#include "ico_indication.h"
#include "ico_cobhandler.h"
#include "ico_queue.h"

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/
typedef enum {
    CO_TR_STATE_FREE = 0,       /* buffer is free */
    CO_TR_STATE_WAITING,        /* buffer waits for inhibit elapsed */
    CO_TR_STATE_TO_TRANSMIT,    /* buffer should be transmitted */
    CO_TR_STATE_ACTIVE,         /* buffer is transmitting */
    CO_TR_STATE_TRANSMITTED     /* buffer was transmitted */
} CO_TR_STATE_T;

struct CO_TRANS_QUEUE {
    CO_CAN_TR_MSG_T msg;        /* can message */
    COB_REFERENZ_T  cobRef;     /* cob reference */
    CO_TR_STATE_T   state;      /* status transmit message (written from ISR */
    struct CO_TRANS_QUEUE *pNext;   /* index to next buffer */
};
typedef struct CO_TRANS_QUEUE CO_TRANS_QUEUE_T;


typedef struct {
    uint32_t        canId;      /* CAN identifier */
    uint8_t         dataLen;    /* CAN msg len */
    uint8_t         flags;      /* flags (rtr, extended, enabled, ... */
} CO_RECBUF_HDR_T;

typedef struct {
    CO_RECBUF_HDR_T bufHdr;
    uint16_t        hdrStart;       /* offset start header */
    uint16_t        lastData;       /* offset last data byte */
    BOOL_T          swap;
    uint8_t         tmpDataBuf[CO_CAN_MAX_DATA_LEN];
} CO_RECBUF_DATA_T;


/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/
static CO_TRANS_QUEUE_T *getNextTransBuf(void);
static void addToTransmitList(CO_TRANS_QUEUE_T *pTrBuf);

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/
static CO_REC_DATA_T     recDataBuffer[CO_CONFIG_REC_BUFFER_CNT];
static uint16_t          recBufferWrCnt = { 0u };
static uint16_t          recBufferRdCnt = { 0u };
static CO_TRANS_QUEUE_T  trDataBuffer[CO_CONFIG_TRANS_BUFFER_CNT];
static BOOL_T            drvBufAccess = { CO_TRUE };
static CO_TRANS_QUEUE_T *pInhibitList = { NULL };
static CO_TRANS_QUEUE_T *pTransmitList = { NULL };
static BOOL_T            recBufFull = { CO_FALSE };

#ifdef ISOTP_CLIENT_CONNECTION_CNT
#include <iiso_tp.h>
#endif /* ISOTP_CLIENT_CONNECTION_CNT */


/***************************************************************************/
/**
* \internal
*
* \brief icoGetReceiveMessage - get next receives message
*
*
* \retval CO_FALSE
*   no data available
* \retval CO_FALSE
*   data available
*/
BOOL_T icoQueueGetReceiveMessage(
        CO_REC_DATA_T   *pRecData       /* pointer to receive data */
    )
{
CO_REC_DATA_T   *pData = NULL;
CO_COB_T        *pCob;
BOOL_T          cobFound = CO_FALSE;
BOOL_T          msgFound = CO_FALSE;

    /* check queue for message and get it */
    while (recBufferWrCnt != recBufferRdCnt)  {
        {

            /* get data from CAN buffer */
            if (recBufferWrCnt != recBufferRdCnt)  {
                msgFound = CO_TRUE;

                recBufferRdCnt++;
                if (recBufferRdCnt >= CO_CONFIG_REC_BUFFER_CNT)  {
                    recBufferRdCnt = 0u;
                }
                pData = &recDataBuffer[recBufferRdCnt];

                /* if buffer is empty, inform application */
                if (recBufferRdCnt == recBufferWrCnt)  {
                    coCommStateEvent(CO_COMM_STATE_EVENT_REC_QUEUE_EMPTY);
                }
            }
        }

        /* msg available (CAN or GW) */
        if ((msgFound == CO_TRUE) && (pData != NULL))  {

            cobFound = CO_FALSE;
            pCob = icoCobCheck(&pData->msg);

            if (pCob != NULL)  {
                pData->service = pCob->service;
                pData->spec = pCob->serviceNr;
                cobFound = CO_TRUE;
            }
        }

        if ((cobFound == CO_TRUE) && (pData != NULL))  {
            memcpy((void *)pRecData, (void *)pData, sizeof(CO_REC_DATA_T));
            return (CO_TRUE);
        }
    }

    return (CO_FALSE);
}


/***************************************************************************/
/**
*
* \brief coQueueReceiveMessageAvailable - receive messages available
*
* This functions checks the receive queue for new messages.
* Are new messages available, return CO_TRUE.
* Otherwise CO_FALSE
*
* \retval CO_FALSE
*   no data available
* \retval CO_TRUE
*   data available
*/
BOOL_T coQueueReceiveMessageAvailable(
        void    /* no parameter */
    )
{
    /* check queue for message */
    if ((recBufferWrCnt != recBufferRdCnt)
        ) {
        return (CO_TRUE);
    } else {
        return (CO_FALSE);
    }
}


/***************************************************************************/
/**
* \internal
*
* \brief coQueueGetReceiveBuffer - get pointer to receive buffer
*
* Check if buffer is available,
* save id and flags
* and return pointer to data buffer.
*
* If no buffer is necessary (0 databytes or RTR)
* return NULL and set buffer as filled.
*
* can be called at interrupt level
*
* \return pointer to save receive data
*
*/
uint8_t *coQueueGetReceiveBuffer(
        uint32_t    canId,      /**< can ID */
        uint8_t     dataLen,    /**< data len */
        uint8_t     flags       /**< flags */
    )
{
    /* printf("coQueueGetReceiveBuffer: rdCnt: %d, wrCnt: %d\n",
        recBufferRdCnt, recBufferWrCnt);
    */
uint16_t    tmpCnt;

    /* get next receive buffer */
    tmpCnt = recBufferWrCnt + 1u;
    if (tmpCnt >= CO_CONFIG_REC_BUFFER_CNT)  {
        tmpCnt = 0u;
    }
    if (tmpCnt == recBufferRdCnt)  {
        /* save event, and signal it later outsite of the interrupt */
        recBufFull = CO_TRUE;

        return (NULL);
    }

    recDataBuffer[tmpCnt].msg.canId = canId;
    recDataBuffer[tmpCnt].msg.len = dataLen;
    recDataBuffer[tmpCnt].msg.flags = flags;
    return (&recDataBuffer[tmpCnt].msg.data[0]);
}


/***************************************************************************/
/**
*
* \internal
*
* \brief coQueueReceiveBufferIsFilled - given queue buffer is filled
*
* This function is called from the driver,
* if the given receive buffer was filled by a received CAN message.<br>
*
* It can be called at interrupt level.
*
*/
void coQueueReceiveBufferIsFilled(
        void    /* no parameter */
    )
{
uint16_t    tmpCnt;

    tmpCnt = recBufferWrCnt + 1u;
    if (tmpCnt >= CO_CONFIG_REC_BUFFER_CNT)  {
        tmpCnt = 0u;
    }
    if (tmpCnt == recBufferRdCnt)  {
        coCommStateEvent(CO_COMM_STATE_EVENT_REC_QUEUE_OVERFLOW);
        return;
    }

    recBufferWrCnt = tmpCnt;

    coCommTaskSet(CO_COMMTASK_EVENT_MSG_AVAIL);

    /* last message buffer used ? */
/*  coCommStateEvent(CO_COMM_STATE_EVENT_REC_QUEUE_FULL); */
}


/***************************************************************************/
/**
* \internal
*
* \brief icoTransmitMessage - save message in transmit queue
*
* flags:
* MSG_OVERWRITE - if the last message is not transmitted yet,
*   overwrite the last data with the new data
* MSG_RET_INHIBIT - add message to transmit queue
*   and send it after the inhibit time has been ellapsed
* Otherwise the function returns with RET_COB_INHIBIT
*
* \return RET_T
*
*/
RET_T icoTransmitMessage(
        COB_REFERENZ_T    cobRef,         /* cob reference */
        CO_CONST uint8_t *pData,          /* pointer to transmit data */
        uint8_t           flags           /* data handle flags */
    )
{
CO_COB_T    *pCob;
CO_TRANS_QUEUE_T *pTrBuf = NULL;
uint16_t     i;

    pCob = icoCobGetPointer(cobRef);
    if (pCob == NULL)  {
        return (RET_EVENT_NO_RESSOURCE);
    }

    /* if cob is disabled, return */
    if ((pCob->canCob.flags & CO_COBFLAG_ENABLED) == 0u)  {
        return (RET_COB_DISABLED);
    }
    /* disable driver buffer access */
    drvBufAccess = CO_FALSE;

    pTrBuf = getNextTransBuf();
    if (pTrBuf == NULL)  {
        /* allow driver buffer access */
        drvBufAccess = CO_TRUE;

        /* start can transmission again */
        (void) codrvCanStartTransmission();

        /* inform application */
        coCommStateEvent(CO_COMM_STATE_EVENT_TR_QUEUE_OVERFLOW);
        return (RET_DRV_TRANS_BUFFER_FULL);
    }

    /* save data at transmit buffer */
    pTrBuf->cobRef = cobRef;
    pTrBuf->msg.flags = CO_COBFLAG_NONE;
    pTrBuf->msg.len = pCob->len;
    pTrBuf->msg.canId = pCob->canCob.canId;
    if ((pCob->canCob.flags & CO_COBFLAG_EXTENDED) != 0u)  {
        pTrBuf->msg.flags |= CO_COBFLAG_EXTENDED;
    }
    pTrBuf->msg.flags |= (pCob->canCob.flags & CO_COBFLAG_FD);

    if (pCob->type == CO_COB_TYPE_RECEIVE)  {
        pTrBuf->msg.flags |= CO_COBFLAG_RTR;
    }
    pTrBuf->msg.canChan = pCob->canCob.canChan;

    if (pData != NULL)  {
        for (i = 0u; i < pCob->len; i++)  {
            pTrBuf->msg.data[i] = pData[i];
        }
    }

    /* should this message create a transmit event */
    if ((flags & MSG_INDICATION) != 0u)  {
        pTrBuf->msg.flags |= CO_COBFLAG_IND;
    }

    addToTransmitList(pTrBuf);

    /* allow driver buffer access */
    drvBufAccess = CO_TRUE;

    /* start blocking can transmitting */
    while (codrvCanStartTransmission() == RET_DRV_BUSY)  {};

    return (RET_OK);
}


/***************************************************************************/
/**
* \internal
*
* \brief addToTransmitList - add an buffer to transmit list
*
* Add an buffer entry to the transmit list at the end of the list
*
* \return none
*
*/
static void addToTransmitList(
        CO_TRANS_QUEUE_T    *pTrBuf
    )
{
CO_TRANS_QUEUE_T    *pList;

    /* add it at last position */
    pList = pTransmitList;

    /* list empty ? */
    if (pList == NULL)  {
        /* save at first */
        pTransmitList = pTrBuf;
    } else {
        /* look for end of the list */
        while (pList->pNext != NULL)  {
            pList = pList->pNext;
        }
        pList->pNext = pTrBuf;
    }

    /* set next element to 0 */
    pTrBuf->pNext = NULL;
    /* set buffer state */
    pTrBuf->state = CO_TR_STATE_TO_TRANSMIT;
    /* printf(" TO TRANSMIT\n"); */
}


/***************************************************************************/
/**
* \internal
*
* \brief icoQueueGetTransBufFillState - get filling state of transmit buffer
*
* get filling state of transmit buffer (0..100)%
* 0 - buffer empty
* 100 - buffer full
*
* \return buffer fill state
*
*/
uint32_t icoQueueGetTransBufFillState(
        void    /* no parameter */
    )
{
uint32_t    cnt = 0u;
uint32_t    i;

    /* look for next free buffer */
    for (i = 0u; i < CO_CONFIG_TRANS_BUFFER_CNT; i++)  {
        if (trDataBuffer[i].state == CO_TR_STATE_FREE)  {
            cnt++;
        }
    }

    return (cnt * 100u / CO_CONFIG_TRANS_BUFFER_CNT);
}


/***************************************************************************/
/**
* \internal
*
* \brief getNextTransBuf - get next transmit buffer
*
* get next transmit buffer depending on the last transmission time
*
* \return buffer index
*/
static CO_TRANS_QUEUE_T *getNextTransBuf(
        void    /* no parameter */
    )
{
uint16_t    i;

    /* look for next free buffer */
    for (i = 0u; i < CO_CONFIG_TRANS_BUFFER_CNT; i++)  {
        if (trDataBuffer[i].state == CO_TR_STATE_FREE)  {
            /* check for buffer full -
             * it comes over only if we use the last buffer */
            if (i == (CO_CONFIG_TRANS_BUFFER_CNT - 1u))  {
                coCommStateEvent(CO_COMM_STATE_EVENT_TR_QUEUE_FULL);
            }
            return (&trDataBuffer[i]);
        }
    }

    return (NULL);
}


/***************************************************************************/
/**
* \brief coQueueGetNextTransmitMessage - get next message to transmit
*
* This function returns the next available transmit message
* from the transmit queue.
* It increments also trBufferRdCnt.
*
* \return CO_CAN_TR_MSG_T* pointer to next tx message
* \retval !NULL
*   pointer to transmit queue entry
* \retval NULL
*   no message available
*
*/
CO_CAN_TR_MSG_T *coQueueGetNextTransmitMessage(
        void    /* no parameter */
    )
{
CO_TRANS_QUEUE_T *pBuf;

    /* access to buffer allowed ? */
    if (drvBufAccess != CO_TRUE)  {
        return (NULL);
    }

    /* start with first entry */
    pBuf = pTransmitList;
    while (pBuf != NULL)  {
        /* correct state ? */
        if (pBuf->state == CO_TR_STATE_TO_TRANSMIT)  {
            /* set state to active */
            pBuf->state = CO_TR_STATE_ACTIVE;
            /* save handle */
            pBuf->msg.handle = pBuf;
            return (&pBuf->msg);
        }

        pBuf = pBuf->pNext;
    }

    return (NULL);
}


/***************************************************************************/
/**
* \brief coQueueMsgTransmitted - message was transmitted
*
* This function is called after a message was succesfull transmitted.
*
* \return none
*
*/
void coQueueMsgTransmitted(
        const CO_CAN_TR_MSG_T *pBuf     /**< pointer to transmitted message */
    )
{
CO_TRANS_QUEUE_T    *pQueue;

    pQueue = (CO_TRANS_QUEUE_T  *)pBuf->handle;
    pQueue->state = CO_TR_STATE_TRANSMITTED;
}


/***************************************************************************/
/**
* \internal
*
* \brief icoQueueHandler
*
* \return none
*
*/
void icoQueueHandler(
        void    /* no parameter */
    )
{
    /* recBuffer was full ? */
    if (recBufFull == CO_TRUE)  {
        recBufFull = CO_FALSE;
        /* inform application, if buffer is full */
        coCommStateEvent(CO_COMM_STATE_EVENT_REC_QUEUE_OVERFLOW);
    }

    /* no entries at transmit list, return */
    if (pTransmitList == NULL)  {
        return;
    }

    /* check all buffer with state transmitted */
    while (pTransmitList->state == CO_TR_STATE_TRANSMITTED)  {
        /* delete transmitted messages ...... and save inhibit */

        /* get cob reference */
        CO_COB_T    *pCob;
        pCob = icoCobGetPointer(pTransmitList->cobRef);
        if (pCob != NULL)  {
            /* should this message create a tx acknowledge */
            if ((pTransmitList->msg.flags & CO_COBFLAG_IND) == CO_COBFLAG_IND)  {

#ifdef ISOTP_CLIENT_CONNECTION_CNT
                if (pCob->service == CO_SERVICE_ISOTP_CLIENT)  {
                    iisoTpClientTxAck(pTransmitList->cobRef, &pTransmitList->msg);
                }
#endif /* ISOTP_CLIENT_CONNECTION_CNT */
            }
        }

        /* set buffer state */
        pTransmitList->state = CO_TR_STATE_FREE;

        /* set transmit list to next buffer */
        pTransmitList = pTransmitList->pNext;
        if (pTransmitList == NULL)  {
            break;
        }
    }

    /* transmit list is empty */
    if (pTransmitList == NULL)  {
        /* inhibit list is also empty, signal it to eventHandler */
        if (pInhibitList == NULL)  {
            coCommStateEvent(CO_COMM_STATE_EVENT_TR_QUEUE_EMPTY);
        }
    } else {
        /* should buffer be transmitted? */
        if (pTransmitList->state == CO_TR_STATE_TO_TRANSMIT)  {
            (void) codrvCanStartTransmission();
        }
    }

}


/***************************************************************************/
/**
*
* \brief coQueueInit - (re)init queues
*
* This function clears the transmit and the receive queue
*
* \return none
*
*/
void coQueueInit(
        void    /* no parameter */
    )
{
uint16_t    i;

    /* transmit queue */
    for (i = 0u; i < CO_CONFIG_TRANS_BUFFER_CNT; i++)  {
        trDataBuffer[i].state = CO_TR_STATE_FREE;
    }

    drvBufAccess = CO_TRUE;
    pInhibitList = NULL;
    pTransmitList = NULL;

    /* receive queue */
    recBufferWrCnt = 0u;
    recBufferRdCnt = 0u;
    recBufFull = CO_FALSE;
}


/***************************************************************************/
/**
* \internal
*
* \brief icoQueuVarInit - init lokal variables
*
* \return none
*
*/
void icoQueueVarInit(
        CO_CONST uint16_t   *recQueueCnt,   /* receive queue cnt */
        CO_CONST uint16_t   *trQueueCnt     /* transmit queue cnt */
    )
{
    (void)trQueueCnt;
    (void)recQueueCnt; /* Remove Warning */

    recBufferWrCnt = 0u;
    recBufferRdCnt= 0u;
    drvBufAccess = CO_TRUE;
    pInhibitList = NULL;
    pTransmitList = NULL;
    recBufFull = CO_FALSE;
}

void icoQueueDeleteInhibit(COB_REFERENZ_T cobRef /* cob reference */ )
{
    (void)cobRef; /* Remove Warning */
}
