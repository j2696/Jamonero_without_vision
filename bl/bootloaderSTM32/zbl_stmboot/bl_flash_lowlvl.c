/*
* bl_flash_lowlvl.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_flash_lowlvl.c
* Low Level Flash functionality - direct access to Flash
* erase, flash
*
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>
#include <bl_type.h>
#include <bl_debug.h>
#include <bl_flash.h>
#include <bl_flash_lowlvl.h>
#include <bl_call.h>
#include <bl_application.h>


#include <stm32g0xx.h>

/*#include <bl_user.h>*/ /// Dbg ram code

extern uint8_t tmpFingerPrint[APPL_FINGERPRINT_LENGTH];
extern uint8_t tmpFingerPrintLength;
extern BlRet_t userGetImageNr(FlashAddr_t address, uint8_t* imageNr);

/* constant definitions
---------------------------------------------------------------------------*/
#define FlashStatusClear 0x00000034ul


#ifdef DECRYPT_INIT
#else /* DECRYPT_INIT */
/* DECRYPT_INIT - reset decrypt functionality
 * \param nbr
 * domain number
 * \returns void
 */
#  define DECRYPT_INIT(nbr)
#endif /* DECRYPT_INIT */

#ifdef DECRYPT_MEMORY
#else /* DECRYPT_MEMCPY */
/* DECRYPT_MEMORY - decrypt a small buffer
 * The entire memory is decrypted by calling this function frequently.
 * \param addr
 *	later flash address location
 * \param pdest 
 * destination pointer (uint8_t *)
 * \param psrc
 * source pointer (const uint8_t *)
 * \param ssize
 * number of byte of the current source memory block
 * \returns void 
 */
#  define DECRYPT_MEMORY(addr, pdest, psrc, size) inline_decrypt_memory(addr, pdest, psrc, size)
static inline void inline_decrypt_memory(
		FlashAddr_t addr,
		uint8_t * pdest,
		const uint8_t * psrc,
		size_t size
	)
{
    FlashAddr_t fingerPrintAddr;
    FlashAddr_t fingerPrintLenAddr;
    FlashAddr_t currentFlashPos;
    uint32_t fingerPrintCurPos = 0;
    const ConfigBlock_t* configBlock;
    uint8_t nbr = 0;
    uint32_t i;
    BlRet_t retVal = BL_RET_OK;
	(void)addr;
	memcpy(pdest, psrc, size);
#ifdef BL_DOMAIN_COUNT
	retVal = userGetImageNr(addr, &nbr);
#endif /* BL_DOMAIN_COUNT */

	if (retVal == BL_RET_OK)  {
		/* get config block */
		configBlock = applConfigBlock(nbr);

		/* get fingerprint address */
		fingerPrintAddr = (FlashAddr_t)&configBlock->applFingerprint[0];
		fingerPrintLenAddr = (FlashAddr_t)&configBlock->applFingerprintLength;

		/* insert fingerprint into flashdata */
		for (i = 0u; i < size; i++)  {
			/* save flash current address */
			currentFlashPos = (addr + i);
			if (currentFlashPos == fingerPrintLenAddr)  {
				/* insert fingerprint length */
				memcpy((pdest + i), &tmpFingerPrintLength, sizeof(configBlock->applFingerprintLength));
			} else if ((currentFlashPos >= fingerPrintAddr) &&
						(currentFlashPos < (fingerPrintAddr + tmpFingerPrintLength)))  {
				/* get current position in finger print */
				fingerPrintCurPos = (currentFlashPos - fingerPrintAddr);
				/* insert fingerprint part */
				memcpy((pdest + i), &tmpFingerPrint[fingerPrintCurPos], 1);
			}
		}
	}
}
#endif /* DECRYPT_INIT */


/* local defined data types
---------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/
static uint8_t llcheckErasePage(FlashAddr_t address);
static BlRet_t llcheckAddress(FlashAddr_t address);


/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/

#ifdef FLASH_ERASE_SIZE
#else /* FLASH_ERASE_SIZE */
/***************************************************************************/
/**
* \brief get pointer to the information of the current flash page
*
* \returns
*	pointer to the page information
*/
const FLASH_SECTOR_T * llsectorParams(
		FlashAddr_t address	/* address */
	)
{
static const uint16_t maxIndex = sizeof(sectorArray) / sizeof(FLASH_SECTOR_T);
uint16_t i;
FlashAddr_t saddr;
uint32_t ssize;

	for (i = 0; i < maxIndex; i++)  {
		saddr = sectorArray[i].baseAddress;
		ssize = sectorArray[i].sectorSize;

		/* for comatibility with old tables */
		if (ssize == 0)  {
			return (NULL);
		}
		/* check range */
		if ((address >= saddr) && (address < (saddr + ssize)))
		{
			return (&sectorArray[i]);
		}
	}

	return (NULL);
}

/***************************************************************************/
/**
* \brief word size of flash pages
*
* The different Flash pages have different size.
*
* \returns
*	page size in word
*/
uint32_t llsectorSize(
		FlashAddr_t address	/* address */
	)
{
const FLASH_SECTOR_T * pParam;
uint32_t size;

	pParam = llsectorParams(address);
	if (pParam == NULL)  {
		return (0u);
	}

	size = pParam->sectorSize;
	return (size);
}

#endif  /* FLASH_ERASE_SIZE */


/***************************************************************************/
/**
 * \brief init the low level modul
 * e.g. copy the RAM functions from Flash to RAM
 *
*/
BlRet_t llflashInit(
		uint8_t nbr
	)
{
	(void)nbr; /* avoid warning */
	DECRYPT_INIT(nbr);
	return (BL_RET_OK);
}


/***************************************************************************/
/**
 * \brief check for an empty flash page
 *
 * An empty flash page must not be erased.
 *
 * \retval 0
 *	empty and error
 *
 *	\retval 1
 *	not empty
 */
#if defined(USE_COMMON_VARIANT)
/***************************************************************************/
static uint8_t llcheckErasePage(
		FlashAddr_t address /**< flash address */
	)
{
uint8_t * pFlash;
uint16_t i;

	/* printf("checkErasePage %p %lx\n", dummyFlash, address); */
	if (fFullEraseRequired != 0u)  {
		return (1u);
	}

	/* calc page start address */
	pFlash = (uint8_t*)(address & ~(FlashAddr_t)(FLASH_ERASE_SIZE - 1));

	for( i = 0; i < FLASH_ERASE_SIZE; i++)  {
		if (*pFlash != FLASH_EMPTY)  {
			return (1);
		}
		pFlash ++;
	}

	return (0u);
}
#elif defined(FLASH_ERASE_SIZE)
/***************************************************************************/
static uint8_t llcheckErasePage(FlashAddr_t address /**< flash address */)
{
    (void)address; /* Remove Warning */
    
	/* optimization for uint32_t - constant page size */
#ifdef BL_ECC_FLASH
#else
uint32_t * pFlash;
uint16_t i;
static const uint32_t empty = (uint32_t)FLASH_EMPTY << 24
					| (uint32_t)FLASH_EMPTY << 16
					| (uint32_t)FLASH_EMPTY << 8
					| (uint32_t)FLASH_EMPTY;
#endif /* BL_ECC_FLASH */
	PRINTF1("checkErasePage %08lx\n", address);

	if (fFullEraseRequired != 0)  {
		PRINTF0("fFullEraseRequired set\n");
		return (1u);
	}

#ifdef BL_ECC_FLASH
	if (fFlashErased[activeDomain] != 1u)  {
		PRINTF0("fFlashErased not set\n");
		return (1u); //erase required
	}
#else /* BL_ECC_FLASH */

	/* calc page start address */
	pFlash = (uint32_t*)(address & ~(FlashAddr_t)(FLASH_ERASE_SIZE - 1));

	for( i = 0; i < FLASH_ERASE_SIZE; i += 4)  {
		if (*pFlash != empty)  {
			return (1u);
		}
		pFlash ++;
	}
#endif /* BL_ECC_FLASH */

	return (0u);
}
#else
/***************************************************************************/
static uint8_t llcheckErasePage(
		FlashAddr_t address /**< flash address */
	)
{
	/* optimization for uint32_t - constant page size */
uint32_t * pFlash;
uint32_t i;
static const uint32_t empty = (uint32_t)FLASH_EMPTY << 24
					| (uint32_t)FLASH_EMPTY << 16
					| (uint32_t)FLASH_EMPTY << 8
					| (uint32_t)FLASH_EMPTY;
const FLASH_SECTOR_T * pParam;
uint32_t eraseSize;

	if (fFullEraseRequired != 0)
	{
		return (1u);
	}

#ifdef BL_ECC_FLASH
	if (fFlashErased[activeDomain] != 1u)
	{
		return (1u); //erase required
	}

#else /* BL_ECC_FLASH */

	pParam = llsectorParams(address);
	if (pParam == NULL)  {
		return (0u); /* error */
	}

	eraseSize = pParam->sectorSize;
	pFlash = (uint32_t *)(pParam->baseAddress);

	for( i = 0u; i < eraseSize; i += 4u)  {
		if (*pFlash != empty)  {
			return (1u);
		}
		pFlash ++;
	}
#endif /* BL_ECC_FLASH */

	return (0u);
}
#endif

/***************************************************************************/
/**
 * \brief erase one page
 *
 * Before a flash page is erased, it is checked if it is already erased.
 * If it is already erased, all bytes have the FLASH_EMPTY value,
 * the function returns without action.
 *
 *
 * \retval BL_STATE_NOTHING
 * 		page is empty - no erase is required
 * \retval BL_STATE_OK
 * 		erase was started correctly
 */
BlRet_t lleraseOnePage(
		FlashAddr_t address /**< flash address */
	)
{
BlRet_t ret;
HAL_StatusTypeDef flashRet = HAL_BUSY;
FLASH_EraseInitTypeDef eraseInit;
uint32_t PageError;
uint32_t page = 0u;
uint32_t bank = FLASH_BANK_1;

	ret = llcheckAddress(address);
	if (ret != BL_RET_OK)  {
		return (ret);
	}

	/* check, if erase is required */
	if (llcheckErasePage(address) == 1)  {
		/* start flash action */
		PRINTF1("erase 0x%lx\n", address);

#ifdef FLASH_ERASE_SIZE

		/* 1 page = 2048
		 * pages count = 256
		 *
		 * FLASH_BL_SIZE
		 *
		 * FLASH_APPL_START
		 *
		 */

#ifdef FLASH_BANK_2
		if (address < FLASH_BASE) {
			return BL_RET_ERROR;
		} else if (address < (FLASH_BASE + FLASH_BANK_SIZE)) {
			bank = FLASH_BANK_1;
		} else if (address < (FLASH_BASE + (FLASH_BANK_SIZE << 2))) {
			bank = FLASH_BANK_2;
		} else {
			return BL_RET_ERROR;
		}
#else  /* FLASH_BANK_2 */
		bank = FLASH_BANK_1;
#endif  /* bank = FLASH_BANK_1; */

		page = ((address - FLASH_BASE) / FLASH_ERASE_SIZE);

		/* start erase if flash is not busy */
		if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != SET) {

			/* unlock flash */
			HAL_FLASH_Unlock();

			FLASH->SR = FlashStatusClear; /* reset error information */

			eraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
			eraseInit.Page = page;
			eraseInit.Banks = bank;
			eraseInit.NbPages = 1;
			flashRet = HAL_FLASHEx_Erase(&eraseInit, &PageError);

			/* lock flash */
			HAL_FLASH_Lock();
		}

#else /* FLASH_ERASE_SIZE */
		uint32_t sector = pageMask(address);

		HAL_FLASH_Unlock();
		eraseInit.Banks = Flashbank;
		eraseInit.NbSectors = 1;
		eraseInit.Sector = sector;
		eraseInit.TypeErase = FLASH_TYPEERASE_SECTORS;
		eraseInit.VoltageRange = voltageRange;
		flashRet = HAL_FLASHEx_Erase(&eraseInit, &PageError);
		HAL_FLASH_Lock();
#endif /* FLASH_ERASE_SIZE */

		SEND_EMCY(0xff00u, 0x18u, (uint16_t)(address & 0xFFFFu), 0u);

		/* in case of error */
		if ((flashRet != HAL_OK) || (PageError != 0xFFFFFFFF)) {
			/* error */
			SEND_EMCY(0xff00u, 0x19u, (uint16_t)(address & 0xFFFFu), flashRet);

			PRINTF2("Error Flash %d %lx\n", flashRet, PageError);

			return BL_RET_FLASH_ERROR;
		}

		return (BL_RET_OK);
	}

	return (BL_RET_NOTHING);
}

/***************************************************************************/
/**
 * \brief flash one page
 *
 *
 */
BlRet_t llflashOnePage(
		FlashAddr_t address, /**< flash address */
		const uint8_t * pSrc /**< data to flash */
	)
{
    BlRet_t ret;
    HAL_StatusTypeDef flashRet = HAL_BUSY;
    uint8_t decryptedBuffer[FLASH_ONE_CALL_SIZE];
    uint64_t buffer;
    uint8_t i;

	ret = llcheckAddress(address);
	if (ret != BL_RET_OK)  {
		return (ret);
	}

	DECRYPT_MEMORY(address, decryptedBuffer, pSrc, FLASH_ONE_CALL_SIZE);

	/* start flash action */
	PRINTF1("flash 0x%lx\n", address);

	FLASH->SR = FlashStatusClear; /* reset error information */

	HAL_FLASH_Unlock();

	for (i = 0u; i < FLASH_ONE_CALL_SIZE; i += 8u)  {
		/* copy 8 bytes to flash buffer */
		memcpy(&buffer, &decryptedBuffer[i], 8u);
		/* flash 8 bytes */
		flashRet = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address + i, buffer);
		if (flashRet != HAL_OK)  {
			break;
		}
	}

	HAL_FLASH_Lock();

	if (flashRet != HAL_OK) {
	    SEND_EMCY(0xff00u, 0x29u, (uint16_t)(address & 0xFFFFu), ret);
		return (BL_RET_FLASH_ERROR);
	}

	return (BL_RET_OK);
}

/***************************************************************************/
/**
 * \brief check if the address is within the FLASH application area
 *
 * This function checks, if address is inside of Application Flash start
 * and end.
 * 
 * \retval BL_RET_ERROR
 * address is wrong
 * \retval BL_RET_OK
 * address is correct
 *
 */

static BlRet_t llcheckAddress(
		FlashAddr_t address /**< address to check */
	)
{
	if (address > FLASH_APPL_END(activeDomain))  {
		return (BL_RET_ERROR);
	}

	if (address < FLASH_APPL_START(activeDomain))  {
		return (BL_RET_ERROR);
	}

	return (BL_RET_OK);
}
