/* user configuration
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief user configuration
*/

#ifndef BL_CONFIG_H
#define BL_CONFIG_H 1

/*--------------------------------------------------------------*/
/* Development settings                                         */
/*--------------------------------------------------------------*/

/* NO_BOOT - default : off
 * call the application without CRC check
 * NO_BOOT is used during the application development.
 * With this setting you can develop/debug the application 
 * without the required CRC sum directly using the debugger.
*/
//#define NO_BOOT 1

/* NO_APPL - default: off
 * do not call the application after reset
 * NO_APPL is used during the Bootloader development
*/
//#define NO_APPL 1

/* TEST_GENERATE_ECC_ERROR - default: off
 * check the Flash ecc error handling
 * With a set TEST_GENERATE_ECC_ERROR the Flash driver generate 
 * ECC errors during the update. This is needed to check the 
 * error handling.
 * This check is only useable of ECC Flash that allow a 
 * ECC manipulation.
*/
//#define TEST_GENERATE_ECC_ERROR 1

/*--------------------------------------------------------------*/
/* Hardware Header                                              */
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/* Type specific settings - before bl_type.h                  */
/*--------------------------------------------------------------*/
/* 16bit char */
//#define CONFIG_DSP 1

/* BL_BIG_ENDIAN - default: off
 * configure Big Endian - set before include bl_type.h 
 * bl_type.h include very simple reverse functionality
 * - BL_REVERSE_U32(x)
 * - BL_REVERSE_U16(x)
*/
//#define BL_BIG_ENDIAN 1

/* SIMULATION_FLASH - default: off
 * enable simulated flash for bootloader development
 * This setting require additional simulation functionality, that 
 * is not part of the generic bootloader.
*/
//#define SIMULATION_FLASH 1

//#define ATTR_NORETURN __attribute__((noreturn))
/*--------------------------------------------------------------*/
/* Bootloader related Header                                    */
/*--------------------------------------------------------------*/
#include <bl_type.h>
#include <bl_user.h>

#include <stm32g0xx.h>
#include <stm32g0xx_hal_flash.h>

/*--------------------------------------------------------------*/
/* Number of flash domains                                      */
/*--------------------------------------------------------------*/

/* BL_DOMAIN_COUNT - default: off
 * number of flash areas, that are defined by different domain objects
 * Set BL_DOMAIN_COUNT to the used count of areas.
*/
//#define BL_DOMAIN_COUNT 2

#ifdef SIMULATION_FLASH
#  define BL_DOMAIN_COUNT 2
#endif /* SIMULATION_FLASH */

/*--------------------------------------------------------------*/
/* Backdoor                                                     */
/*--------------------------------------------------------------*/

/* CFG_CAN_BACKDOOR - default: off
 * enable the optional feature Backdoor support
 * This functionality is optional and only implemented on request.
 * This setting need some more configurations and adaptations.
 * - CAN listen only mode, if required
 * - specific backdoor CAN message
 * Please read the manual.
*/
//#define CFG_CAN_BACKDOOR 1

/* CFG_BACKDOOR_TIME - required for CFG_CAN_BACKDOOR
 * wait time to receive the backdoor message
 * The time value is in ms and the resolution is depend of the 
 * timer implementation.
*/
#ifdef CFG_CAN_BACKDOOR
#  define CFG_BACKDOOR_TIME 300 /* 300ms */
#endif /* CFG_CAN_BACKDOOR */

/*--------------------------------------------------------------*/
/* Flash settings                                               */
/*--------------------------------------------------------------*/
#define FLASH_BL_SIZE	(0x10000 - 0x800)   //$klg> m 62 Kbytes

/*  CFG_MAX_DOMAINSIZE(x) - mandatory
 * size of the specific flash area, represented by the domain object
 *
 * In case of different flash areas a function is called.
 *   #define CFG_MAX_DOMAINSIZE(x) 	userGetDomainSize((x))
 * Image 0 .. Applikation
 * Image 1..n additional images
 *
 * In case of a simple one image it's the application size.
 * The default
 *   #define CFG_MAX_DOMAINSIZE(x) ((256 * 1024ul) - FLASH_BL_SIZE)
 * means the complete flash without the bootloader is used for 
 * the application.
*/
#ifdef BL_DOMAIN_COUNT
extern U32 userGetDomainSize(U8 nbr);
#  define CFG_MAX_DOMAINSIZE(x) 	userGetDomainSize((x))
#else /* BL_DOMAIN_COUNT */
   /** Application Flash size (byte) - 512k Derivat  */
#  define CFG_MAX_DOMAINSIZE(x) (FLASH_SIZE - FLASH_BL_SIZE)
#endif /* BL_DOMAIN_COUNT */

/** FLASH_WRITE_SIZE - mandatory 
 * number of bytes, that has to collect using the communication
 * before this buffer has to flash.
 * Because the implementation limits the flash write size 
 * - must be greater than one CAN Telegram 
 * - must greater/equal than the number of bytes, that are flashed
 *   in one internal cycle (FLASH_ONE_CALL_SIZE)
*/
#  define FLASH_WRITE_SIZE 	(100u * 4u)

/** FLASH_ERASE_SIZE - default: erase page size
 * flash erase page size
 * This setting is use, if the flash is divided into many 
 * equal erase pages.
 * Without this definition the flashdriver use different sizes 
 * (sectors / llsectorSize() )
 */
#define FLASH_ERASE_SIZE	FLASH_PAGE_SIZE

/* FLASH_EMPTY - default: 0xFF
 * value of erase flash
*/
#define FLASH_EMPTY			0xFF

/* FLASH_APPL_START/FLASH_APPL_END - mandatory
 * start/end address of the flash image 
 * Image 0 .. Applikation
 * Image 1..n additional images
 */
#ifdef BL_DOMAIN_COUNT
extern FlashAddr_t userGetApplStartAdr(U8 nbr);
	/* get the start addresses of each domain e.g. from a user function */
#  define FLASH_APPL_START(x) 	userGetApplStartAdr((x))
#  define FLASH_APPL_END(x)		((FLASH_APPL_START((x))) + ((CFG_MAX_DOMAINSIZE((x))) - 1))
#else /* BL_DOMAIN_COUNT */	
#define FLASH_APPL_START(x)		(0x08000000ul + FLASH_BL_SIZE) //$klg> 0x0800F800 address of config block for zephyr image 
#define FLASH_APPL_END(x)		((FLASH_APPL_START((x))) + ((CFG_MAX_DOMAINSIZE((x))) - 1))
#endif /* BL_DOMAIN_COUNT */

/*--------------------------------------------------------------*/
/* Configuration Block                                          */
/*--------------------------------------------------------------*/

/* CFG_CONFIG_BLOCK_SIZE - mandatory
* configuration block size
* Image 0 .. Applikation
* Image 1..n additional images
*/
#ifdef BL_DOMAIN_COUNT
extern U32 userGetConfigSize(U8 nbr);
#define CFG_CONFIG_BLOCK_SIZE(x) 	userGetConfigSize((x))
#else
/** Config Block size - e.g. depend of alignment of the IRQ table */
//$klg> when appending config block to zephyr.bin using tool 'generate_firmware' don't forget -c 0x800
#define CFG_CONFIG_BLOCK_SIZE(x)    0x800
#endif

/*--------------------------------------------------------------*/
/* CPU specific                                                 */
/*--------------------------------------------------------------*/
/** call a software reset */
#define USER_RESET() softwareReset()  /* software reset */

/*--------------------------------------------------------------*/
/* bootloader/application adaptations                           */
/*--------------------------------------------------------------*/
//#define CFG_APPL_NODEID 1

/** GET_NODEID - mandatory
 * Bootloader Node ID 
*/
#define GET_NODEID()	userGetNodeId()


/** CFG_BITRATE - mandatory
* Bootloader bitrate (kbit/s)
*/
#define CFG_BITRATE()	userGetBitrate()

/** COBL_LSS_FCT - optional
* Bootloader LSS callback
*/
//#define COBL_LSS_FCT	userLssSetNodeId

//#define COBL_OD_1000_0_FCT userOd1000
#define COBL_OD_1018_1_FCT userOd1018_1
#define COBL_OD_1018_2_FCT userOd1018_2
//#define COBL_OD_1018_3_FCT userOd1018_3 $klg> not used
//#define COBL_OD_1018_4_FCT userOd1018_4
//#define COBL_OD_5F00_0_FCT userOd5F00_0



//#define COBL_OD_U32_READ_FCT userOdU32ReadFct

#ifdef COBL_SDO_SEG_READ_TRANSFER_SUPPORTED
#define COBL_OD_SEG_READ_FCT userSegReadFct
#endif

/* BL_CHECKSUM_CHECK() returns BL_RET_OK, if checksum of application correct
 * BlRet_t mainChecksumCheck(void);
 */
#define BL_CHECKSUM_CHECK() mainChecksumCheck()

/* COBL_SDO_PROG_START - default: on
* SDO 1F51:1 = 1 call application enabled 
*/
#define COBL_SDO_PROG_START 1 

/* BL_CHECK_PRODUCTID - default: on
 * check the vendor id and product code of image (configuration block) 
 * and bootloader object entries
 * generate_firmware must be used with --od1018_1 and --od1018_2 
*/
#define BL_CHECK_PRODUCTID 1

/* BL_ECC_FLASH - default: off
* On ECC flash sometimes it is not possible to use the BL internal 
* empty check function, because it creates a new ECC Trap.
* This define activate functionality to force an flash erase.
* Additional device specific adaptations required (trap implementation).
*/
/*#define BL_ECC_FLASH 1*/            //$klg> cfg-bld

/* BL_ERASE_BEFORE_FLASH - default: off
* => erase command required before flash 
* Typical enabled for BL_ECC_FLASH.
* Note: If you abort the erase you have to erase it again before flash 
* You can use SDO_RESPONSE_AFTER_ERASE to prevent an Erase abort
* created by the next command.
*/
/*#define BL_ERASE_BEFORE_FLASH 1*/   //$klg> cfg-bld

/* BL_DONT_ABORT_FLASH - default: off
* => do not abort flash activity 
* The firmware download returns an error, if erase is active.
* Without this definition the erase is aborted. 
* The definition can be disabled in case, that the Flash 
* is erasing during download.
*/
//#define BL_DONT_ABORT_FLASH 1

/* SDO_RESPONSE_AFTER_ERASE - default: off
* SDO response at the end of erase.
* Without this setting the SDO returns immediately.
* Check 0x1F57 about the current state.
*/
//#define SDO_RESPONSE_AFTER_ERASE 1

/* BL_CANOPEN_LSS - default: off
* CANopen LSS support for the bootloader.
* With this set the bootloader supports minimal
* LSS functionality.
*/
//#define BL_CANOPEN_LSS 1

/* BL_WAIT_FOR_START - default: off
* bootloader stays in bootloader-Mode
* until a start command was received.
*/
//#define BL_WAIT_FOR_START 1


/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/

/* BL_DEBUG - default: off
 * Debug code enabled  
 * This settings enable printf() debugging code.
 * NO_PRINTF should not be set, if BL_DEBUG is using.
 */
//#define BL_DEBUG 1

/* COBL_DEBUG_EMCY - default: on
 * send additional Emergencies for debug 
 * This setting can be enabled a long time. With this setting there are some addtional
 * information in the CAN logging that help in case of update issues.
 * In the release version it could be disabled.
 */
//#define COBL_DEBUG_EMCY 1


/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/
#ifdef COBL_DEBUG_EMCY
#  define COBL_EMCY_PRODUCER 1
#  define SEND_EMCY(code, arg1, arg2, arg3) udsSendEmcy(code, arg1, arg2, arg3);
#else
#  define SEND_EMCY(code, arg1, arg2, arg3)
#endif

/* NO_PRINTF - default: on
 * Disable in general the use of printf().
 * In case of the use of BL_DEBUG you have to implement
 * an different output, if set, but needed.
 */

#define NO_PRINTF 1

/*
* all printf() should only used with BL_DEBUG.
* But in case printf() is used in other way, the Define
* NO_PRINTF should disable the command printf().
*/
#ifdef NO_PRINTF
# define printf(...)
#else /* NO_PRINTF */
# include <stdio.h>
# define PRINTF printf
#endif /* NO_PRINTF */

#endif /* BL_CONFIG_H */
