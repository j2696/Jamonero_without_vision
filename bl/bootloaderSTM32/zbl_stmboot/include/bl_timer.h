/*
* bl_timer.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_timer.h - Timer functionality
*
*/



#ifndef BL_TIMER_H
#define BL_TIMER_H 1

/* Prototypes
*--------------------------------------------------------------------*/
void timerInit(void);
uint8_t timerTimeExpired(uint16_t timeVal);
void timerWait100ms(void);

#endif /* BL_TIMER_H */
