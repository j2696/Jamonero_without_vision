/*
* ico_common.h - contains internal defines
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \file
* \brief common defines
*/

#ifndef ICO_COMMON_H
#define ICO_COMMON_H 1

/* datatypes */



/* function prototypes */

BOOL_T icoFdMode(void);

#endif /* ICO_COMMON_H */
