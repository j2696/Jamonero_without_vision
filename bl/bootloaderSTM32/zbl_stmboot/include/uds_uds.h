﻿/*
* uds_uds.h - contains defines for diagnostic
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief data types
*
* \file uds_uds.h - contains defines for diagnostic
*
*/

#ifndef UDS_UDS_H
#define UDS_UDS_H 1


#include <uds_datatype.h>

/* constant definitions
-----------------------------------------------------------------------------*/
/* service defines */
#define UDS_REQ_INVALID				(uint8_t)0x00u
#define UDS_RESP_INVALID			(uint8_t)0x00u

#define UDS_REQ_SESSION_CONTROL		(uint8_t)0x10u	/**< DiagnosticSessionControl service */
#define UDS_REQ_ECU_RESET			(uint8_t)0x11u	/**< ECUReset service */
#define UDS_REQ_TESTER_PRESENT		(uint8_t)0x3eu	/**< TesterPresent service */

#define UDS_REQ_SEC_ACCESS_SEED     (uint8_t)0x27u   /**< Security access request seed */
#define UDS_RESP_SEC_ACCESS_SEED    (uint8_t)0x67u   /**< Security access response seed */

#define UDS_RESP_SESSION_CONTROL	(uint8_t)0x50u
#define UDS_RESP_ECU_RESET			(uint8_t)0x51u
#define UDS_RESP_TESTER_PRESENT		(uint8_t)0x7eu

#define UDS_REQ_COMM_CONTROL        (uint8_t)0x28u    /**< Request coommunication control */

#define UDS_REQ_CDTCI				(uint8_t)0x14u	/**< ClearDiagnosticInformation service */
#define UDS_RESP_CDTCIPR			(uint8_t)0x54u	/**< ClearDiagnosticInformation  Positiv Response */

#define UDS_REQ_ROUTINE_CTRL        (uint8_t)0x31u    /**< Request Routine control */
#define UDS_RESP_ROUTINE_CTRL       (uint8_t)0x71u    /**< Respone Routine control */

#define UDS_REQ_DOWNLOAD            (uint8_t)0x34u    /**< Request Download service */
#define UDS_REQ_UPLOAD      		(uint8_t)0x35u	/**< RequestUpload service */
#define UDS_TRANSFER_DOWNLOAD       (uint8_t)0x36u    /**< Transfer Data Download */
#define UDS_REQ_TRANSFER_EXIT       (uint8_t)0x37u    /**< Request Transfer Exit */
#define UDS_RESP_TRANSFER_EXIT      (uint8_t)0x77u    /**< Response Transfer Exit */

#define UDS_REQ_READ_BY_ID          (uint8_t)0x22u    /**< Request Read Data by Identifier */
#define UDS_RESP_READ_BY_ID         (uint8_t)0x62u    /**< Response Read Data by Identifier */
#define UDS_REQ_WRITE_BY_ID         (uint8_t)0x2eu    /**< Request Write Data by Identifier */
#define UDS_RESP_WRITE_BY_ID        (uint8_t)0x6eu    /**< Response Write Data by Identifier */

#define UDS_RESP_RDPR				(uint8_t)0x74u	/**< RequestDownload Positiv Response */

#define UDS_REQ_ControlDTCSetting   (uint8_t)0x85u    /**< Request Control DTC Settings */

/* subfunctions */
#define UDS_SUB_NO_RESP_BIT		(uint8_t)0x80u	/**< suppressPosRspMsgIndicationBit */

#define	UDS_SUB_ZSUBF			(uint8_t)0x00u	/**< zeroSubFunction */


/* Security Access type */
#define UDS_SEC_ACCESS_SEED_LVL_1   0x01u       /**< REQUEST_SEED_SECURITY Level 1 */
#define UDS_SEC_ACCESS_SEED_LVL_2   0x03u       /**< REQUEST_SEED_SECURITY Level 2 */
#define UDS_SEC_ACCESS_SEED_LVL_3   0x05u       /**< REQUEST_SEED_SECURITY Level 3 */
#define UDS_SEC_ACCESS_SEED_LVL_4   0x07u       /**< REQUEST_SEED_SECURITY Level 4 */
#define UDS_SEC_ACCESS_SEED_LVL_5   0x09u       /**< REQUEST_SEED_SECURITY Level 5 */

#define UDS_SEC_ACCESS_KEY_LVL_1    0x02u       /**< SEND_KEY_SECURITY Level 1 */
#define UDS_SEC_ACCESS_KEY_LVL_2    0x04u       /**< SEND_KEY_SECURITY Level 2 */
#define UDS_SEC_ACCESS_KEY_LVL_3    0x06u       /**< SEND_KEY_SECURITY Level 3 */
#define UDS_SEC_ACCESS_KEY_LVL_4    0x08u       /**< SEND_KEY_SECURITY Level 4 */
#define UDS_SEC_ACCESS_KEY_LVL_5    0x0au       /**< SEND_KEY_SECURITY Level 5 */

/* SESSION Request message sub-function parameter definition */
#define UDS_SUB_SESSION_DS		(uint8_t)0x01u	/**< defaultSession */
#define UDS_SUB_SESSION_PRGS	(uint8_t)0x02u	/**< ProgrammingSession */
#define UDS_SUB_SESSION_EXTDS	(uint8_t)0x03u	/**< extendedDiagnosticSession */
#define UDS_SUB_SESSION_SSDS	(uint8_t)0x04u	/**< safetySystemDiagnosticSession */

/* RoutineControl */
#define UDS_ROUTINE_CTRL_START  (uint8_t)0x01u    /**< routine control type start */


#define UDS_RID_ERASE_MEMORY		(uint16_t)0xff00u /**< routine ctrl identifier erase Memory */
#define UDS_RID_CHK_PROG_DEPENDS	(uint16_t)0xff01u /**< check Programming dependencies */
#define UDS_RID_CHK_MEMORY			(uint16_t)0x0202u /**< check memory request */
#define UDS_RID_CHK_PRGM_PRE_COND	(uint16_t)0x0203u	/**< check programming pre condition */
#define UDS_RID_FLASH_AFTER_RESET	(uint16_t)0xf518u


/* RESET Request message sub-function parameter definition */
#define UDS_SUB_RESET_HR		(uint8_t)0x01u	/**< hardReset */
#define UDS_SUB_RESET_KOFFONR	(uint8_t)0x02u	/**< keyOffOnReset */
#define UDS_SUB_RESET_SR		(uint8_t)0x03u	/**< softReset */
#define UDS_SUB_RESET_ERPSD		(uint8_t)0x04u	/**< enableRapidPowerShutDown */
#define UDS_SUB_RESET_DRPSD		(uint8_t)0x05u	/**< disableRapidPowerShutDown */

/* Negative Response Code (NRC) definitions */
#define UDS_NRC_SID				(uint8_t)0x7f

#define UDS_NRC_PR				(uint8_t)0x00u	/**< positiveResponse */
#define UDS_NRC_GR				(uint8_t)0x10u	/**< generalReject */
#define UDS_NRC_SNS				(uint8_t)0x11u	/**< serviceNotSupported */
#define UDS_NRC_SFNS			(uint8_t)0x12u	/**< sub-functionNotSupported */
#define UDS_NRC_IMLOIF			(uint8_t)0x13u	/**< incorrectMessageLengthOrInvalidFormat */
#define UDS_NRC_RTL				(uint8_t)0x14u	/**< responseTooLong */
#define UDS_NRC_BRR				(uint8_t)0x21u	/**< busyRepeatRequest */
#define UDS_NRC_CNC				(uint8_t)0x22u	/**< conditionsNotCorrect */
#define UDS_NRC_RSE				(uint8_t)0x24u	/**< requestSequenceError */
#define UDS_NRC_NRFSC			(uint8_t)0x25u	/**< noResponseFromSubnetComponent */
#define UDS_NRC_FPEORA			(uint8_t)0x26u	/**< FailurePreventsExecutionOfRequestedAction */
#define UDS_NRC_ROOR			(uint8_t)0x31u	/**< requestOutOfRange */
#define UDS_NRC_SAD				(uint8_t)0x33u	/**< securityAccessDenied */
#define UDS_NRC_IK				(uint8_t)0x35u	/**< invalidKey */
#define UDS_NRC_ENOA			(uint8_t)0x36u	/**< exceedNumberOfAttempts */
#define UDS_NRC_RTDNE			(uint8_t)0x37u	/**< requiredTimeDelayNotExpired */
#define UDS_NRC_UDNA			(uint8_t)0x70u	/**< uploadDownloadNotAccepted */
#define UDS_NRC_TDS				(uint8_t)0x71u	/**< transferDataSuspended */
#define UDS_NRC_GPF				(uint8_t)0x72u	/**< generalProgrammingFailure */
#define UDS_NRC_WBSC			(uint8_t)0x73u	/**< wrongBlockSequenceCounter */
#define UDS_NRC_RCRRP			(uint8_t)0x78u	/**< requestCorrectlyReceived-ResponsePending */
#define UDS_NRC_SFNSIAS			(uint8_t)0x7eu	/**< sub-functionNotSupportedInActiveSession */


typedef enum {
	UDS_REQUEST_READ = 0,
	UDS_REQUEST_WRITE,
	UDS_TRANSFER_READ,
	UDS_TRANSFER_WRITE,
	UDS_TRANSFER_EXIT
} UDS_DATA_REQ_TYPE_T;



/* datatypes */

RET_T udsStackInit();
void udsCommSetPowerDownTime(
        uint8_t time		/**< Ppowerdown Time in s*/
    );
RET_T udsCommSetSessionTime(
        uint16_t p2ServerMaxMs,
        uint16_t p2StarServerMaxMs
    );

RET_T udsCommSetDiagnosticSessionType(uint8_t sessionType);
uint8_t udsCommGetDiagnosticSessionType(void);

RET_T udsSendResponsePending(uint8_t addr, uint8_t reqType);

RET_T udsCommSetSecurityLevel(uint8_t securityLevel);

RET_T udsDecodeAddress(uint8_t *pData, uint16_t dataSize, uint32_t *pAddress, uint32_t *pSize);

#endif /* UDS_UDS_H */

