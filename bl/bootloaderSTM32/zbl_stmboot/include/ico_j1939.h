/*
* ico_j1939.h - contains internal defines for J1939
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief data type header
*/

#ifndef ICO_J1939_H
#define ICO_J1939_H 1

/* datatypes */

/* function prototypes */
void ij1939_receive(CO_REC_DATA_T *pRecData);

#endif /* ICO_J1939_H */




