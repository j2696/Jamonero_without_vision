/*
* iso_tp.h - contains public api for iso-tp
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*
*/

/**
* \brief api for iso-tp
*
* \file iso_tp.h - contains api for iso-tp stack
*
*/

#ifndef ISO_TP_H
#define ISO_TP_H 1

#include <co_datatype.h>


/* constants */



/* datatypes */
typedef enum {
	ISOTP_ADDRESS_UNKNOWN = 0u,
	ISOTP_ADDRESS_PHYS,
	ISOTP_ADDRESS_FUNC
} ISO_TP_ADDRESS_T;


typedef enum {
	ISOTP_FORMAT_NORMAL = 0u,
	ISOTP_FORMAT_EXTENDED
} ISO_TP_FORMAT_T;


typedef enum {
	ISOTP_DATAPTR_SERVER = 0u,
	ISOTP_DATAPTR_CLIENT
} ISO_TP_DATAPTR_T;


/** \brief function pointer for isotp receive function
 *
 * \param addr - source address
 * \param pData - pointer to data
 * \param dataSize - valid data size
 *
 * \return RET_T
 *
 */
typedef RET_T (* ISOTP_EVENT_SERVER_T)(uint8_t addr, uint8_t *pData, uint16_t dataSize);

/** \brief function pointer for isotp receive split function
 *
 * \param addr - source address
 * \param pData - pointer to data
 * \param dataSize - valid data size
 * \param tranferSize - size of the currently tranfered
 *
 * \return RET_T
 *
 */
typedef RET_T (* ISOTP_EVENT_SERVER_SPLIT_T)(uint8_t addr, uint8_t *pData, uint16_t dataSize, uint16_t tranferSize);

 /** \brief function pointer for isotp transmit function
 *
 * \param addr - souce address
 * \param pData - pointer to data
 * \param result - result of the transfer
 *
 * \return RET_T
 *
 */
typedef RET_T (* ISOTP_EVENT_CLIENT_T)(uint8_t addr, uint8_t *pData, RET_T result);

/** \brief function pointer for isotp transmit acknowledge function
 *
 * \param addr - souce address
 * \param pData - pointer to data
 * \param size - valid data bytes
 *
 * \return RET_T
 *
 */
typedef RET_T(*ISOTP_EVENT_CLIENT_TXACK_T)(uint8_t addr, uint8_t* pData, uint16_t size);


/* function prototypes */
EXTERN_DECL RET_T isoTpInit(void);
EXTERN_DECL RET_T isoTpInitServerChannel(uint32_t reqId, uint32_t respId,
		uint8_t srcAddr, uint8_t destAdr, ISO_TP_ADDRESS_T addrType,
		ISO_TP_FORMAT_T formatType, uint8_t *pRecDataBuffer, uint16_t recBufSize);
EXTERN_DECL RET_T isoTpInitClientChannel(uint32_t reqId, uint32_t respId,
		uint8_t srcAddr, uint8_t destAdr, ISO_TP_ADDRESS_T addrType,
		ISO_TP_FORMAT_T formatType, uint8_t *pRecDataBuffer, uint16_t recBufSize);
EXTERN_DECL RET_T isoTpSetRecDataPtr(ISO_TP_DATAPTR_T direction, uint8_t destAddr, uint8_t *pRecData, uint16_t size);

EXTERN_DECL RET_T isoTpSendReq(uint8_t destAddr, uint8_t *pData, uint16_t dataLen, uint8_t flags);
EXTERN_DECL RET_T isoTpSendReqWA(uint8_t destAddr, uint8_t *pData, uint16_t dataLen, uint8_t flags);

EXTERN_DECL RET_T isoTpEventRegister_SERVER(ISOTP_EVENT_SERVER_T pFunction);
EXTERN_DECL RET_T isoTpEventRegister_CLIENT(ISOTP_EVENT_CLIENT_T pFunction);

EXTERN_DECL RET_T isoTpEventRegister_SERVER_SPLIT(ISOTP_EVENT_SERVER_SPLIT_T pFunction);
EXTERN_DECL RET_T isoTpEventRegister_CLIENT_TXACK(ISOTP_EVENT_CLIENT_TXACK_T pFunction);

EXTERN_DECL ISO_TP_ADDRESS_T isoTpGetCurrectConType(void);

#endif /* ISO_TP_H */
