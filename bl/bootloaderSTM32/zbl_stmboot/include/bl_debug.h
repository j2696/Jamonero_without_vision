/*
* BL_DEBUG.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief additional debug functionality
*
*/

#ifndef BL_DEBUG_H
#define BL_DEBUG_H 1

void bl_puts(const char * s);
void bl_hex8(uint8_t c);
void bl_hex16(uint16_t i);
void bl_hex32(uint32_t l);
void bl_ptr(const void * const p);
void bl_dump(const uint8_t * pStart, uint16_t iCnt);
void bl_printDatatypes(void);

#ifdef PRINTF
# define PRINTF0(fmt) PRINTF(fmt)
# define PRINTF1(fmt, arg1) PRINTF(fmt, arg1)
# define PRINTF2(fmt, arg1, arg2) PRINTF(fmt, arg1, arg2)
# define PRINTF3(fmt, arg1, arg2, arg3) PRINTF(fmt, arg1, arg2, arg3)
#else /* PRINTF */
# define PRINTF0(fmt)
# define PRINTF1(fmt, arg1)
# define PRINTF2(fmt, arg1, arg2)
# define PRINTF3(fmt, arg1, arg2, arg3)
#endif /* PRINTF */

#endif /* BL_DEBUG_H */
