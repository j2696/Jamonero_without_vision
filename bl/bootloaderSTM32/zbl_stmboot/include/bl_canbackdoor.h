/*
* bl_canbackdoor.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief Bootloader backdoor using CAN
*
* The Bootloader activate the CAN during startup and wait a short time
* for a backdoor message. Without this message the application is calling.
*
*/



#ifndef BL_CANBACKDOOR_H
#define BL_CANBACKDOOR_H 1

void backdoorCheck(void);


#endif /* BL_CANBACKDOOR_H */

