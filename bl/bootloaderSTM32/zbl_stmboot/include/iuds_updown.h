/*
* iuds_updown.h - contains internal defines for uds updown
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief internal defines for uds updown
*/

#ifndef IUDS_UPDOWN_H
#define IUDS_UPDOWN_H 1



/* function prototypes */

RET_T iudsUpDownMsgHandler(uint8_t addr, uint8_t* pData, uint16_t dataSize);


#endif /* IUDS_UPDOWN_H */
