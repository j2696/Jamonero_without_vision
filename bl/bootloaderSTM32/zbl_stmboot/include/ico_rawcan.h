/*
* ico_rawcan.h - contains internal defines for RAWCAN
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief data type header
*/

#ifndef ICO_RAWCAN_H
#define ICO_RAWCAN_H 1

/* datatypes */

/* function prototypes */
void irawcanReceive(CO_REC_DATA_T *pRecData);

#endif /* ICO_RAWCAN_H */




