﻿/*
* iuds_comm.h - contains internal defines for uds communication
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief internal defines for uds communication
*/

#ifndef IUDS_COMM_H
#define IUDS_COMM_H 1



/* function prototypes */

RET_T iudsCommMsgHandler(uint8_t addr, uint8_t* pData, uint16_t dataSize);
RET_T iudsCommMsgAck(uint8_t addr, uint8_t* pData);
BOOL_T iudsCheckRequiredSession(uint8_t accessType, const uint8_t *reqSessionTypes, uint8_t cnt);
BOOL_T iudsCheckSecurityLevel(const uint8_t *reqSesurityTypes, uint8_t cnt);
RET_T iudsCommRestartTimeOut(void);
void iudsCommEventSession(void* pData);

#endif /* IUDS_COMM_H */
