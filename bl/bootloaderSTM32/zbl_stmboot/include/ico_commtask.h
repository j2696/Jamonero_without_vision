/*
* ico_commtask.h - contains internal defines for communication task
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief dataty type header
*/

#ifndef ICO_COMM_TASK_H
#define ICO_COMM_TASK_H 1

#include <co_commtask.h>

/* datatypes */


/* function prototypes */
void icoCommTaskVarInit(void);


#endif /* ICO_COMM_TASK_H */

