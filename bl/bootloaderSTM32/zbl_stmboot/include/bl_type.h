/* global types
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief general type definitions
*
*/

#include <stdint.h>

#ifndef BL_TYPE_H
#define BL_TYPE_H 1

/* default definitions for Big Endian CPUs */
#ifndef BL_REVERSE_U32
  #ifdef BL_BIG_ENDIAN
	#define BL_REVERSE_U32(x)	((uint32_t)(((uint32_t)(x) >> 24) | (((x) & 0x00ff0000ul) >> 8) | (((x) & 0x0000ff00ul) << 8) | (((x) & 0x000000fful) << 24))) 		
  #else /* BL_BIG_ENDIAN */
	#define BL_REVERSE_U32(x) (x)
  #endif /* BL_BIG_ENDIAN */
#endif /*  !BL_REVERSE_U32 */

#ifndef BL_REVERSE_U16
  #ifdef BL_BIG_ENDIAN
	#define BL_REVERSE_U16(x)	((((uint16_t)(x) & 0xff00) >> 8) | (((x) & 0x00ff) << 8)) 
  #else /* BL_BIG_ENDIAN */
	#define BL_REVERSE_U16(x) (x)
  #endif /* BL_BIG_ENDIAN */
#endif /* !BL_REVERSE_U16 */

/* __attribute__((noreturn)) */
#ifndef ATTR_NORETURN
  #define ATTR_NORETURN
#endif

typedef uint8_t Flag_t; /**< small numbers, but sometime are greater types better */
#if defined(__linux__) || defined(_WIN32)
typedef uintptr_t FlashAddr_t; /**< integer value of the flash address */
#else
typedef unsigned long FlashAddr_t; /**< integer value of the flash address */
#endif
/** general return value of the bootloader */
typedef enum {
	BL_RET_OK=0,	/**< OK */
	BL_RET_CAN_BUSY, /**< CAN is busy, e.g. cannot transmit */

	BL_RET_BUSY, /**< functionality is busy/already running */
	BL_RET_NOTHING, /**< nothing is required to do */

	BL_RET_CRC_WRONG, /**< wrong CRC sum */

	BL_RET_FLASH_END, /**< action was stopped */
	BL_RET_FLASH_ERROR, /**< general error */

	BL_RET_CALLBACK_READY, /**< callback has answer sent */

	BL_RET_USB_WRONG_FORMAT, /**< format error */
	BL_RET_IMAGE_WRONG,/* flash image has incorrect parameter */
	BL_RET_ERROR /**< general error */
} BlRet_t;


#define BL_COMMAND_DISWD     0U
#define BL_COMMAND_START     1U    // call application
#define BL_COMMAND_NOFAR     2U    // jump application no FAR check
#define BL_COMMAND_AWAKE     3U
#define BL_COMMAND_BL        4U    // stay in bootloader
#define BL_COMMAND_BACKDOOR  5U    // activate backdoor

typedef enum {
	BL_BACKDOOR_DEACTIVATE, /* backdoor inactive */
	BL_BACKDOOR_ACTIVATE, /* activate the backdoor */
	BL_BACKDOOR_USED /* backdoor is using by the user */
} BlBackdoor_t;

typedef struct CoblObjInfo  {
	uint32_t	objSize;
	void *		pObjData;
} CoblObjInfo_t;

typedef enum {
	COBL_LSS_SERVICE_STORE,		/**< LSS service indication store node id */
	COBL_LSS_SERVICE_NEW_BITRATE,	/**< LSS service indication new bitrate */
	COBL_LSS_SERVICE_BITRATE_OFF, /**< LSS service indication bitrate off */
	COBL_LSS_SERVICE_BITRATE_SET, /**< LSS service indication set new bitrate*/
	COBL_LSS_SERVICE_BITRATE_ACTIVE,	/**< LSS service indication bitrate active */
	COBL_LSS_SERVICE_NEW_NODE_ID		/**< LSS service indication new node-id */
} CoblLssService_t;


#endif /* BL_TYPE_H */
