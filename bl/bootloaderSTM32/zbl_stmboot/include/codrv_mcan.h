/*
* codrv_mcan.h
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/


/********************************************************************/
/**
* \file
* \brief mcan from BOSCH (e.g. STM32H7,lpc546xx)
*
*/

#ifndef CODRV_MCAN_H
#define CODRV_MCAN_H 1


#define MCAN_CCCR_BIT_INIT	(1ul << 0u)
#define MCAN_CCCR_BIT_CCE	(1ul << 1u)
#define MCAN_CCCR_BIT_ASM	(1ul << 2u)
#define MCAN_CCCR_BIT_CSA	(1ul << 3u)
#define MCAN_CCCR_BIT_CSR	(1ul << 4u)
#define MCAN_CCCR_BIT_MON	(1ul << 5u)
#define MCAN_CCCR_BIT_DAR	(1ul << 6u)
#define MCAN_CCCR_BIT_TEST	(1ul << 7u)
#define MCAN_CCCR_BIT_FDOE	(1ul << 8u)
#define MCAN_CCCR_BIT_BRSE	(1ul << 9u)
#define MCAN_CCCR_BIT_NISO	(1ul << 15u)

#define MCAN_IE_BIT_RF0NE	(1ul << 0u)
#define MCAN_IE_BIT_TCE		(1ul << 9u)
#define MCAN_IE_BIT_BO		(1ul << 25u)
#define MCAN_IE_BIT_EP		(1ul << 23u)

#define MCAN_PSR_BIT_EP		(1ul << 5u)
#define MCAN_PSR_BIT_EW		(1ul << 6u)
#define MCAN_PSR_BIT_BO		(1ul << 7u)

#define MCAN_CANID_BIT_XTD	(1ul << 30u)
#define MCAN_CANID_BIT_RTR	(1ul << 29u)


#if defined(CODRV_MCAN_CFG_VER_3) || defined(CODRV_MCAN_STM32_MP1)
typedef struct
{
   uint32_t              CREL;                 /*!< CAN master control register,         							Address offset: 0x000          */
   uint32_t              ENDN;                 /*!< CAN master status register,          							Address offset: 0x004          */
   uint32_t              RESERVED0;            /*!< reserved					        							Address offset: 0x008          */
   uint32_t              DBTP;                 /*!< CAN Data bit timing and prescaler,							Address offset: 0x00C          */
   uint32_t              TEST;                 /*!< CAN Test register,         									Address offset: 0x010          */
   uint32_t              RWD;                  /*!< CAN interrupt enable register,       							Address offset: 0x014          */
   uint32_t              CCCR;                 /*!< CAN CC control,           									Address offset: 0x018          */
   uint32_t              NBTP;                 /*!< CAN Nominal bit timing and prescaler,							Address offset: 0x01C          */
   uint32_t              TSCC;                 /*!< CAN Timestamp counter configuration,  						Address offset: 0x020          */
   uint32_t              TSCV;                 /*!< CAN Timestamp counter value,          						Address offset: 0x024          */
   uint32_t              TOCC;                 /*!< CAN Timeout counter configuration,    						Address offset: 0x028          */
   uint32_t              TOCV;                 /*!< CAN Timeout counter value,         							Address offset: 0x02C          */
   uint32_t              RESERVED1[4];         /*!< reserved,         											Address offset: 0x030          */
   uint32_t              ECR;                  /*!< CAN Error counter,       										Address offset: 0x040          */
   uint32_t              PSR;                  /*!< CAN Protocol status,           								Address offset: 0x044          */
   uint32_t              TDCR;                 /*!< CAN Transmitter delay compensator,    						Address offset: 0x048          */
   uint32_t              RESERVED2;            /*!< reserved,         											Address offset: 0x04C          */
   uint32_t              IR;                   /*!< CAN Interrupt,          										Address offset: 0x050          */
   uint32_t              IE;                   /*!< CAN Interrupt enable,        									Address offset: 0x054          */
   uint32_t              ILS;                  /*!< CAN Interrupt line select,         							Address offset: 0x058          */
   uint32_t              ILE;                  /*!< CAN Interrupt line enable,         							Address offset: 0x05C          */
   uint32_t              RESERVED3[8];         /*!< reserved,       												Address offset: 0x060          */
   uint32_t              GFC;                  /*!< CAN Global filter configuration,      						Address offset: 0x080          */
   uint32_t              SIDFC;                /*!< CAN standard ID filter configuration,							Address offset: 0x084          */
   uint32_t              XIDFC;                /*!< CAN extended ID filter configuration,		  					Address offset: 0x088          */
   uint32_t              RESERVED4;            /*!< reserved,       												Address offset: 0x08C          */
   uint32_t              XIDAM;                /*!< CAN Extended ID and mask,        								Address offset: 0x090          */
   uint32_t              HPMS;                 /*!< CAN High priority message status,         					Address offset: 0x094          */
   uint32_t              NDAT1;                /*!< new data 1,       											Address offset: 0x098          */
   uint32_t              NDAT2;                /*!< new data 2,       											Address offset: 0x09C          */
   uint32_t              RXF0C;                /*!< CAN Rx FIFO 0 configuration,           						Address offset: 0x0A0          */
   uint32_t              RXF0S;                /*!< CAN Rx FIFO 0 status,             							Address offset: 0x0A4          */
   uint32_t              RXF0A;                /*!< CAN Rx FIFO 0 acknowledge,         							Address offset: 0x0A8          */
   uint32_t              RXBC;                 /*!< CAN Rx buffer configuration,          						Address offset: 0x0AC          */
   uint32_t              RXF1C;                /*!< CAN Rx FIFO 1 configuration,        							Address offset: 0x0B0          */
   uint32_t              RXF1S;                /*!< CAN Rx FIFO 1 status,         								Address offset: 0x0B4          */
   uint32_t              RXF1A;                /*!< CAN Rx FIFO 1 acknowledge,         							Address offset: 0x0B8          */
   uint32_t              RXESC;                /*!< CAN Rx buffer and FIFO element size configuration,       		Address offset: 0x0BC          */
   uint32_t              TXBC;                 /*!< CAN Tx buffer configuration,           						Address offset: 0x0C0          */
   uint32_t              TXFQS;                /*!< CAN Tx FIFO/queue status,             						Address offset: 0x0C4          */
   uint32_t              TXESC;                /*!< CAN Tx buffer element size configuration,         			Address offset: 0x0C8          */
   uint32_t              TXBRP;                /*!< CAN Tx buffer request pending,          						Address offset: 0x0CC          */
   uint32_t              TXBAR;                /*!< CAN Tx buffer add request,        							Address offset: 0x0D0          */
   uint32_t              TXBCR;                /*!< CAN Tx buffer cancellation request,         					Address offset: 0x0D4          */
   uint32_t              TXBTO;                /*!< CAN Tx buffer transmission occurred,         					Address offset: 0x0D8          */
   uint32_t              TXBCF;                /*!< CAN Tx buffer cancellation finished,       					Address offset: 0x0DC          */
   uint32_t              TXBTIE;               /*!< CAN Tx buffer transmission interrupt enable,           		Address offset: 0x0E0          */
   uint32_t              TXBCIE;               /*!< CAN Tx buffer cancellation finished interrupt enable,         Address offset: 0x0E4          */
   uint32_t              RESERVED5[2];         /*!< reserved,         											Address offset: 0x0E8          */
   uint32_t              TXEFC;                /*!< CAN Tx event FIFO configuration,          					Address offset: 0x0F0          */
   uint32_t              TXEFS;                /*!< CAN Tx event FIFO status,        								Address offset: 0x0F4          */
   uint32_t              TXEFA;                /*!< CAN Tx event FIFO acknowledge,         						Address offset: 0x0F8          */
   uint32_t              RESERVED6;            /*!< reserved,														Address offset: 0x0FC          */
   uint32_t              TTTMC;                /*!< CAN TT trigger memory configuration,         					Address offset: 0x100          */
   uint32_t              TTRMC;                /*!< CAN TT reference message configuration,         				Address offset: 0x104          */
   uint32_t              TTOCF;                /*!< CAN TT operation configuration,         						Address offset: 0x108          */
   uint32_t              TTMLM;                /*!< CAN TT matrix limits register,         						Address offset: 0x10C          */
   uint32_t              TURCF;                /*!< CAN TUR configuration,         								Address offset: 0x110          */
   uint32_t              TTOCN;                /*!< CAN TT operation control,         							Address offset: 0x114          */
   uint32_t              TTGTP;                /*!< CAN TT global time preset,         							Address offset: 0x118          */
   uint32_t              TTTMK;                /*!< CAN TT time mark,         									Address offset: 0x11C          */
   uint32_t              TTIR;                 /*!< CAN TT interrupt,         									Address offset: 0x120          */
   uint32_t              TTIE;                 /*!< CAN TT interrupt enable,         								Address offset: 0x124          */
   uint32_t              TTILS;                /*!< CAN TT interrupt line select,         						Address offset: 0x128          */
   uint32_t              TTOST;                /*!< CAN TT operation status,         								Address offset: 0x12C          */
   uint32_t              TURNA;                /*!< CAN TUR numerator actual,         							Address offset: 0x130          */
   uint32_t              TTLGT;                /*!< CAN TT local and global time,         						Address offset: 0x134          */
   uint32_t              TTCTC;                /*!< CAN TT cycle time and count,         							Address offset: 0x138          */
   uint32_t              TTCPT;                /*!< CAN TT capture time,         									Address offset: 0x13C          */
   uint32_t              TTCSM;                /*!< CAN TT cycle sync mark,         								Address offset: 0x140          */
   uint32_t              RESERVED7[111];       /*!< reserved,         											Address offset: 0x144          */
   uint32_t              TTTS;                 /*!< CAN TT trigger select,										Address offset: 0x300          */
} CODRV_MCANFD_T;

#else /* define(CODRV_MCAN_CFG_VER_3) || defined(CODRV_MCAN_STM32_MP1) */

# if defined(CODRV_MCAN_CFG_VER_2) || defined(CODRV_MCAN_STM32_L5) || defined(CODRV_MCAN_STM32_G0) || defined(CODRV_MCAN_STM32_G4)

typedef struct
{
   uint32_t              CREL;                 /*!< CAN master control register,         							Address offset: 0x00          */
   uint32_t              ENDN;                 /*!< CAN master status register,          							Address offset: 0x04          */
   uint32_t              CUST;                 /*!< CAN transmit status register,        							Address offset: 0x08          */
   uint32_t              DBTP;                 /*!< CAN Data bit timing and prescaler,							Address offset: 0x0C          */
   uint32_t              TEST;                 /*!< CAN Test register,         									Address offset: 0x10          */
   uint32_t              RWD;                  /*!< CAN interrupt enable register,       							Address offset: 0x14          */
   uint32_t              CCCR;                 /*!< CAN CC control,           									Address offset: 0x18          */
   uint32_t              NBTP;                 /*!< CAN Nominal bit timing and prescaler,							Address offset: 0x1C          */
   uint32_t              TSCC;                 /*!< CAN Timestamp counter configuration,  						Address offset: 0x20          */
   uint32_t              TSCV;                 /*!< CAN Timestamp counter value,          						Address offset: 0x24          */
   uint32_t              TOCC;                 /*!< CAN Timeout counter configuration,    						Address offset: 0x28          */
   uint32_t              TOCV;                 /*!< CAN Timeout counter value,         							Address offset: 0x2C          */
   uint32_t              RESERVED0[4];         /*!< reserved,         											Address offset: 0x30          */
   uint32_t              ECR;                  /*!< CAN Error counter,       										Address offset: 0x40          */
   uint32_t              PSR;                  /*!< CAN Protocol status,           								Address offset: 0x44          */
   uint32_t              TDCR;                 /*!< CAN Transmitter delay compensator,    						Address offset: 0x48          */
   uint32_t              RESERVED1;            /*!< reserved,         											Address offset: 0x4C          */
   uint32_t              IR;                   /*!< CAN Interrupt,          										Address offset: 0x50          */
   uint32_t              IE;                   /*!< CAN Interrupt enable,        									Address offset: 0x54          */
   uint32_t              ILS;                  /*!< CAN Interrupt line select,         							Address offset: 0x58          */
   uint32_t              ILE;                  /*!< CAN Interrupt line enable,         							Address offset: 0x5C          */
   uint32_t              RESERVED2[8];         /*!< reserved,       												Address offset: 0x60          */
   uint32_t              GFC;                  /*!< CAN Global filter configuration,      						Address offset: 0x80          */
   uint32_t              XIDAM;                /*!< CAN Extended ID and mask,        								Address offset: 0x84          */
   uint32_t              HPMS;                 /*!< CAN High priority message status,         					Address offset: 0x88          */
   uint32_t              RESERVED3;            /*!< reserved,       												Address offset: 0x8C          */
   uint32_t              RXF0S;                /*!< CAN Rx FIFO 0 status,             							Address offset: 0x90          */
   uint32_t              RXF0A;                /*!< CAN Rx FIFO 0 acknowledge,         							Address offset: 0x94          */
   uint32_t              RXF1S;                /*!< CAN Rx FIFO 1 status,         								Address offset: 0x98          */
   uint32_t              RXF1A;                /*!< CAN Rx FIFO 1 acknowledge,         							Address offset: 0x9C          */
   uint32_t              RESERVED4[8];         /*!< reserved,         											Address offset: 0xa0          */
   uint32_t              TXBC;                 /*!< CAN Tx buffer configuration,           						Address offset: 0xC0          */
   uint32_t              TXFQS;                /*!< CAN Tx FIFO/queue status,             						Address offset: 0xC4          */
   uint32_t              TXBRP;                /*!< CAN Tx buffer request pending,          						Address offset: 0xC8          */
   uint32_t              TXBAR;                /*!< CAN Tx buffer add request,        							Address offset: 0xCC          */
   uint32_t              TXBCR;                /*!< CAN Tx buffer cancellation request,         					Address offset: 0xD0          */
   uint32_t              TXBTO;                /*!< CAN Tx buffer transmission occurred,         					Address offset: 0xD4          */
   uint32_t              TXBCF;                /*!< CAN Tx buffer cancellation finished,       					Address offset: 0xD8          */
   uint32_t              TXBTIE;               /*!< CAN Tx buffer transmission interrupt enable,           		Address offset: 0xDC          */
   uint32_t              TXBCIE;               /*!< CAN Tx buffer cancellation finished interrupt enable,			Address offset: 0xE0          */
   uint32_t              TXEFS;                /*!< CAN Tx event FIFO status,        								Address offset: 0xE4          */
   uint32_t              TXEFA;                /*!< CAN Tx event FIFO acknowledge,         						Address offset: 0xE8          */
   uint32_t              RESERVED5[5];         /*!< reserved,         											Address offset: 0xEC          */
   uint32_t              CKDIV;                /*!< FD Clock divider,					         					Address offset: 0x100          */
} CODRV_MCANFD_T;

# else /* defined(CODRV_MCAN_CFG_VER_2) || defined(CODRV_MCAN_STM32_L5) || defined(CODRV_MCAN_STM32_G4) */

/* CODRV_MCAN_CFG_VER_1 */
typedef struct
{
   uint32_t              CREL;                 /*!< CAN master control register,         							Address offset: 0x00          */
   uint32_t              ENDN;                 /*!< CAN master status register,          							Address offset: 0x04          */
   uint32_t              CUST;                 /*!< CAN transmit status register,        							Address offset: 0x08          */
   uint32_t              DBTP;                 /*!< CAN Data bit timing and prescaler,							Address offset: 0x0C          */
   uint32_t              TEST;                 /*!< CAN Test register,         									Address offset: 0x10          */
   uint32_t              RWD;                  /*!< CAN interrupt enable register,       							Address offset: 0x14          */
   uint32_t              CCCR;                 /*!< CAN CC control,           									Address offset: 0x18          */
   uint32_t              NBTP;                 /*!< CAN Nominal bit timing and prescaler,							Address offset: 0x1C          */
   uint32_t              TSCC;                 /*!< CAN Timestamp counter configuration,  						Address offset: 0x20          */
   uint32_t              TSCV;                 /*!< CAN Timestamp counter value,          						Address offset: 0x24          */
   uint32_t              TOCC;                 /*!< CAN Timeout counter configuration,    						Address offset: 0x28          */
   uint32_t              TOCV;                 /*!< CAN Timeout counter value,         							Address offset: 0x2C          */
   uint32_t              RESERVED0[4];         /*!< reserved,         											Address offset: 0x30          */
   uint32_t              ECR;                  /*!< CAN Error counter,       										Address offset: 0x40          */
   uint32_t              PSR;                  /*!< CAN Protocol status,           								Address offset: 0x44          */
   uint32_t              TDCR;                 /*!< CAN Transmitter delay compensator,    						Address offset: 0x48          */
   uint32_t              RESERVED1;            /*!< reserved,         											Address offset: 0x4C          */
   uint32_t              IR;                   /*!< CAN Interrupt,          										Address offset: 0x50          */
   uint32_t              IE;                   /*!< CAN Interrupt enable,        									Address offset: 0x54          */
   uint32_t              ILS;                  /*!< CAN Interrupt line select,         							Address offset: 0x58          */
   uint32_t              ILE;                  /*!< CAN Interrupt line enable,         							Address offset: 0x5C          */
   uint32_t              RESERVED2[8];         /*!< reserved,       												Address offset: 0x60          */
   uint32_t              GFC;                  /*!< CAN Global filter configuration,      						Address offset: 0x80          */
   uint32_t              SIDFC;                /*!< CAN Standard ID filter configuration,							Address offset: 0x84          */
   uint32_t              XIDFC;                /*!< CAN Extended ID filter configuration,     					Address offset: 0x88          */
   uint32_t              RESERVED3;            /*!< reserved,          											Address offset: 0x8C          */
   uint32_t              XIDAM;                /*!< CAN Extended ID and mask,        								Address offset: 0x90          */
   uint32_t              HPMS;                 /*!< CAN High priority message status,         					Address offset: 0x94          */
   uint32_t              NDAT1;                /*!< CAN New data 1,         										Address offset: 0x98          */
   uint32_t              NDAT2;                /*!< CAN New data 2,       										Address offset: 0x9C          */
   uint32_t              RXF0C;                /*!< CAN Rx FIFO 0 configuration,           						Address offset: 0xA0          */
   uint32_t              RXF0S;                /*!< CAN Rx FIFO 0 status,             							Address offset: 0xA4          */
   uint32_t              RXF0A;                /*!< CAN Rx FIFO 0 acknowledge,         							Address offset: 0xA8          */
   uint32_t              RXBC;                 /*!< CAN Rx buffer configuration,          						Address offset: 0xAC          */
   uint32_t              RXF1C;                /*!< CAN Rx FIFO 1 configuration,        							Address offset: 0xB0          */
   uint32_t              RXF1S;                /*!< CAN Rx FIFO 1 status,         								Address offset: 0xB4          */
   uint32_t              RXF1A;                /*!< CAN Rx FIFO 1 acknowledge,         							Address offset: 0xB8          */
   uint32_t              RXESC;                /*!< CAN Rx buffer and FIFO element size configuration,       		Address offset: 0xBC          */
   uint32_t              TXBC;                 /*!< CAN Tx buffer configuration,           						Address offset: 0xC0          */
   uint32_t              TXFQS;                /*!< CAN Tx FIFO/queue status,             						Address offset: 0xC4          */
   uint32_t              TXESC;                /*!< CAN Tx buffer element size configuration,         			Address offset: 0xC8          */
   uint32_t              TXBRP;                /*!< CAN Tx buffer request pending,          						Address offset: 0xCC          */
   uint32_t              TXBAR;                /*!< CAN Tx buffer add request,        							Address offset: 0xD0          */
   uint32_t              TXBCR;                /*!< CAN Tx buffer cancellation request,         					Address offset: 0xD4          */
   uint32_t              TXBTO;                /*!< CAN Tx buffer transmission occurred,         					Address offset: 0xD8          */
   uint32_t              TXBCF;                /*!< CAN Tx buffer cancellation finished,       					Address offset: 0xDC          */
   uint32_t              TXBTIE;               /*!< CAN Tx buffer transmission interrupt enable,           		Address offset: 0xE0          */
   uint32_t              TXBCIE;               /*!< CAN Tx buffer cancellation finished interrupt enable,         Address offset: 0xE4          */
   uint32_t              RESERVED4[2];         /*!< reserved,         											Address offset: 0xE8          */
   uint32_t              TXEFC;                /*!< CAN Tx event FIFO configuration,          					Address offset: 0xF0          */
   uint32_t              TXEFS;                /*!< CAN Tx event FIFO status,        								Address offset: 0xF4          */
   uint32_t              TXEFA;                /*!< CAN Tx event FIFO acknowledge,         						Address offset: 0xF8          */
   uint32_t              RESERVED5;            /*!< reserved,					         							Address offset: 0xFC          */
} CODRV_MCANFD_T;

# endif /*  defined(CODRV_MCAN_CFG_VER_2) || defined(CODRV_MCAN_STM32_L5) || defined(CODRV_MCAN_STM32_G4) */
#endif /* define(CODRV_MCAN_CFG_VER_3) || defined(CODRV_MCAN_STM32_MP1) */


/* global prototypes, that not in co_drv.h */
void codrvCanReceiveInterrupt(void);
RET_T codrvCanTransmitInterrupt(void);
void codrvCanErrorInterrupt(void);

/* extern required functions */
extern void codrvCanSetTxInterrupt(void);


#if defined(CONFIG_MCAN0) || defined(CONFIG_MCAN1)
# else
	/* default FDCAN1 */
#	define CONFIG_MCAN0 1     //$klg> FDCAN1 * this is the one used by the  but with different pin selection
/* #define CONFIG_MCAN1 1 */  //$klg> FDCAN2
# endif

#endif /* CODRV_MCAN_H */
