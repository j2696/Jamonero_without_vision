/*
* ico_cobhandler.h - contains internal defines for timer handling
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/**
* \file
* \brief dataty type header
*/

#ifndef ICO_TIMER_H
#define ICO_TIMER_H 1


/* datatypes */



/* function prototypes */

void	icoTimerCheck(void);

#endif /* ICO_TIMER_H */

