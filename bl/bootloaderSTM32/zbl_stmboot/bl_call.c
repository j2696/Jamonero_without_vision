/*
* bl_call.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_call.c - check application and decide to stay in the bootloader
*
* Currently this file contains the communication over a RAM section with
* the application. The Customer can implement additional functionality 
* to stay in the bootloader with a corrupt application, but with correct 
* checksum. E.g. usage of additional GPIO Pins.

*
*
* Bootloader dont find 'BOOT' command
*  - check application CRC
*  - call application
*
* Application send 'BOOT' command and start Bootloader
*  - Bootloader find 'BOOT' command
*  - Bootloader wait for CANopen commands
*
* With Backdoor (not generally supported!)
*  Bootloader dont find 'BOOT' command
*   - activate backdoor
*   in case the backdoor is not used
*   - send 'START' command and restart bootloader
*
*   Bootloader find 'START' command
*   - check application
*   - call application
*
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <string.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>
#include <bl_type.h>

#include <bl_call.h>
#include <bl_application.h>

#include <bl_crc.h>


/* constant definitions
---------------------------------------------------------------------------*/

/* local defined data types
---------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/
/**
* \bl_command is shared memory between bootloader and application
*
* bl_command[0..3] call bootloader or application
*/
#if defined ( __CC_ARM )
uint8_t bl_command[16] __attribute__((at(0x20000100)));		/* Keil */
#elif defined ( __GNUC__ )
//uint8_t bl_command[16] __attribute__((section(".noinit")));	/* GCC */
uint8_t bl_command[16] __attribute__((section("command")));	/* GCC */
#elif defined (__ICCARM__)
#pragma location = "command_ram"
__no_init uint8_t bl_command[16];								/* IAR */
#elif defined (WIN32)
uint8_t bl_command[16];										/* VS */
#elif defined(__CCRX__)
#pragma address bl_command=0x00000004
uint8_t bl_command[16];								/*  */
#elif defined (__E2STUDIO__)
#pragma address bl_command=0xfb300
uint8_t bl_command[16];								/*  */
#else
# error "Compiler not supported yet"
#endif

/* local defined variables
---------------------------------------------------------------------------*/

/********************************************************************/
/**
 * \brief check to stay in the bootloader
 * 
* Within this function it is possible to add manufacturer
 * backup solutions in case of wrong application, that cannot
 * call back the bootloader.
 *
 * \retval BL_COMMAND_START
 * application should called
 * \retval BL_COMMAND_BL
 * stay in bootloader
 * \retval BL_COMMAND_BACKDOOR
 * activate backdoor
 */
Flag_t stayInBootloader(void)
{
#ifdef BL_CHECK_PRODUCTID
BlRet_t ret;
#endif

	if (memcmp(&bl_command[0], COMMAND_BOOT, COMMAND_SIZE) == 0)  {
		/* stay in bootloader - 'BOOT' command from the application */
		return (BL_COMMAND_BL);
	}

	if (memcmp(&bl_command[0], COMMAND_DISWD, COMMAND_SIZE) == 0)  {

		return (BL_COMMAND_DISWD);
	}

#ifdef BL_ECC_FLASH
	if (bl_command[ECC_ERROR_IDX] == ECC_ERROR_VAL)  {
		return (BL_COMMAND_BL);
	}
#endif /* BL_ECC_FLASH */

#ifdef BL_CHECK_PRODUCTID
	ret = applCheckImage();
	if (ret != BL_RET_OK)  {
		/* wrong image */
		return (BL_COMMAND_BL);
	}
#endif /* BL_CHECK_PRODUCTID */

#ifdef CFG_CAN_BACKDOOR
	if (memcmp(&bl_command[0], COMMAND_START, COMMAND_SIZE) != 0)  {
		/* stay in bootloader - activate backdoor */
		return (BL_COMMAND_BACKDOOR);
	}
#endif /* CFG_CAN_BACKDOOR */

#ifdef BL_WAIT_FOR_START
	if (memcmp(&bl_command[0], COMMAND_START, COMMAND_SIZE) != 0)  {
		/* stay in bootloader - wait for start command */
		return (BL_COMMAND_BL);
	}
#endif /* BL_WAIT_FOR_START */

    if (memcmp(&bl_command[0], COMMAND_NOFAR, COMMAND_SIZE) == 0)  {

        return (BL_COMMAND_NOFAR);
    }

    if (memcmp(&bl_command[0], COMMAND_START, COMMAND_SIZE) == 0)  {

        return (BL_COMMAND_START);
    }

	/* call application, if possible */
	return (BL_COMMAND_AWAKE);
}

#ifdef CFG_LOCK_USED
void lockException(void)
{
	/* set bootloader exception marker */
	bl_command[4] = EXCEPTION_BL_1;
	bl_command[5] = EXCEPTION_BL_2;
}

void unlockException(void)
{
	/* reset bootloader exception marker */
	bl_command[4] = 0x00;
	bl_command[5] = 0xFF;
}
#endif

/********************************************************************/
/**
 * \brief reset bootloader stay command
 * 
 */
void setNoneCommand(void)
{
	/* reset marker */
	memset( &bl_command[0], 0, sizeof(bl_command));
	memcpy( &bl_command[0], COMMAND_NONE, COMMAND_SIZE);

#ifdef CFG_LOCK_USED
	/* init this area */
	lockException();
#endif /* CFG_LOCK_USED */

#ifdef CFG_CAN_BACKDOOR
	/* init backdoor parameter */
	setBackdoorCommand(BL_BACKDOOR_DEACTIVATE);
#endif /* CFG_CAN_BACKDOOR */

}

/********************************************************************/
/**
 * \brief reset unknown bl_command[] sequences
 * Initialize the memory like after a power on reset.
 * Note, that some settings also set, if the memory
 * was initialized (COMMAND_NONE).
 *
 */

void resetUnknownCommand(void)
{
    if (
            (memcmp(&bl_command[0], COMMAND_BOOT, COMMAND_SIZE) != 0)
        &&  (memcmp(&bl_command[0], COMMAND_NOFAR, COMMAND_SIZE) != 0)
        &&  (memcmp(&bl_command[0], COMMAND_START, COMMAND_SIZE) != 0)
        &&  (memcmp(&bl_command[0], COMMAND_NONE, COMMAND_SIZE) != 0)
        &&  (memcmp(&bl_command[0], COMMAND_DISWD, COMMAND_SIZE) != 0)
        )
    {
        setNoneCommand();
    }

}

/********************************************************************/
/**
 * \brief set command for entering WLP Low Power Mode
 *
 */
void setUSLECommand(void)
{
    bl_command[0U] = 'U';
    bl_command[1U] = 'S';
    bl_command[2U] = 'L';
    bl_command[3U] = 'E';
}

/********************************************************************/
/**
 * \brief set command to call application
 *
 */
void setNoFARCommand(void)
{
    memcpy( &bl_command[0], COMMAND_NOFAR, COMMAND_SIZE);
}

/********************************************************************/
/**
 * \brief set command to call application
 *
 */
void setStartCommand(void)
{
    memcpy( &bl_command[0], COMMAND_START, COMMAND_SIZE);
}

/********************************************************************/
/**
 * \brief set command to stay in bootloader
 *
 */
void setBootCommand(void)
{
	/* reset marker */
	memcpy( &bl_command[0], COMMAND_BOOT, COMMAND_SIZE);

}

#ifdef CFG_CAN_BACKDOOR
/********************************************************************/
/**
 * \brief set backdoor state command
 *
 */
void setBackdoorCommand(
		BlBackdoor_t state
	)
{
	bl_command[COMMAND_IDX_BACKDOOR_STATE] = (U8)state;
}

/********************************************************************/
/**
 * \brief get current backdoor command
 *
 */
BlBackdoor_t getBackdoorCommand(void)
{
	return ((BlBackdoor_t)bl_command[COMMAND_IDX_BACKDOOR_STATE]);
}
#endif /* CFG_CAN_BACKDOOR */
