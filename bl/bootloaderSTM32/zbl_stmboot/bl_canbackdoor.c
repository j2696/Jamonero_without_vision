/*
* bl_canbackdoor.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief bl_canbackdoor.c - Bootloader backdoor using CAN
*
* The Bootloader activate the CAN during startup and wait a short time
* for a backdoor message. Without this message the application is calling.
*
* 
*
*/



/* header of standard C - libraries
---------------------------------------------------------------------------*/
#include <string.h>

/* header of project specific types
---------------------------------------------------------------------------*/
#include <bl_config.h>

#ifdef CFG_CAN_BACKDOOR
#include <bl_type.h>
#include <bl_hardware.h>
#include <bl_can.h>
#include <bl_timer.h>
#include <bl_call.h>

#include <bl_canbackdoor.h>

/* constant definitions
---------------------------------------------------------------------------*/
//#define BACKDOOR_SDO_WRITE_STOP 1
#define BACKDOOR_SDO_WRITE_AB 1
//#define BACKDOOR_SPECIAL_MESSAGE 1


/* local defined data types
---------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/
void softwareReset(void);

/* list of global defined functions
---------------------------------------------------------------------------*/

/* list of local defined functions
---------------------------------------------------------------------------*/

/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/

/********************************************************************/
/**
 * \brief backdoorCheck - wait a short time and check all received messages
 * 
 * \returns
 * never
 * 
 */
void backdoorCheck(void)
{
	/* initialize bootloader hardware */
	initHardware();

	setBackdoorCommand(BL_BACKDOOR_ACTIVATE);

	(void)coblCanInit();
	coblCanListenOnlyMode();
	coblCanConfigureFilter(0);
	coblCanEnable();

	//no EMCY Debug possible -> no nodeId !
	//SEND_EMCY(0xFFFF, 0, 0, 0);
	timerInit();

	/* in case of reset */
	setBackdoorCommand(BL_BACKDOOR_USED);
	while (timerTimeExpired(CFG_BACKDOOR_TIME) == 0)  {
	CanMsg_t canMsg;

		if (coblCanReceive(&canMsg) != CAN_EMPTY)  {

#ifdef BACKDOOR_SDO_WRITE_STOP
			// example SDO Stop message 0x1F51:1 = 0
			if (
					(canMsg.cobId.id == (0x600 + GET_NODEID()))
				&&	 (canMsg.dlc == 8)
				&& (canMsg.msg.u32Data[0] == 0x011F512Ful)
				&& (canMsg.msg.u32Data[1] == 0x00000000ul)
			)
			{
				setBootCommand();
				softwareReset();
			}
#endif
#ifdef BACKDOOR_SDO_WRITE_AB
			// example SDO Stop message with unused value 0x1F51:1 = 0xAB
			if (
					(canMsg.cobId.id == (0x600 + GET_NODEID()))
				&&	 (canMsg.dlc == 8)
				&& (BL_REVERSE_U32(canMsg.msg.u32Data[0]) == 0x011F512Ful)
				&& (BL_REVERSE_U32(canMsg.msg.u32Data[1]) == 0x000000ABul)
			)
			{
				setBootCommand();
				softwareReset();
			}
#endif
#ifdef BACKDOOR_SPECIAL_MESSAGE
			// special message
			if (canMsg.cobId.id == 0x001)
			{
				setBootCommand();
				softwareReset();
			}
#endif
		}
	}

	setBackdoorCommand(BL_BACKDOOR_DEACTIVATE);
	setStartCommand();
	softwareReset();

}

#endif /* CFG_CAN_BACKDOOR */
