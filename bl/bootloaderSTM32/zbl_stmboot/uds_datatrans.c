﻿/*
* uds_datatrans.c - contains uds data transfer functions
*
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
*
*/

/******************************************************************************
* \file uds_datatrans.c
* \brief uds data transfer routines
*/

/* header of standard C - libraries
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gen_define.h>
#include <co_datatype.h>
#include <co_timer.h>
#include <iso_tp.h>

#include <uds_datatype.h>
#include <uds_uds.h>
#include <uds_indication.h>

#include "iuds_datatrans.h"
#include "iuds_core.h"
#include "iuds_comm.h"

#include <bl_user.h>

/* header of project specific types
-----------------------------------------------------------------------------*/

/* constant definitions
-----------------------------------------------------------------------------*/

/* local defined data types
-----------------------------------------------------------------------------*/

#define MAX_RBI_CNT 10

/* list of external used functions, if not in headers
-----------------------------------------------------------------------------*/

/* list of global defined functions
-----------------------------------------------------------------------------*/

/* list of local defined functions
-----------------------------------------------------------------------------*/

static RET_T readByIdentifier(uint8_t addr, uint8_t *pData, uint16_t *dataSize);

static RET_T writeByIdentifier(uint8_t addr, uint8_t *pData, uint16_t dataSize);

/* external variables
-----------------------------------------------------------------------------*/

/* global variables
-----------------------------------------------------------------------------*/

/* local defined variables
-----------------------------------------------------------------------------*/

static UDS_EVENT_DATA_TRANSMIT_T pDataTransmitFct;


/******************************************************************************
* \brief iudsDataTransMsgHandler -
*
*/
RET_T iudsDataTransMsgHandler (
        uint8_t   addr,
        uint8_t  *pData,
        uint16_t  dataSize       )
{
    RET_T retVal;
    const uint8_t reqSessionTypes[4] = { UDS_SUB_SESSION_DS, UDS_SUB_SESSION_PRGS, UDS_SUB_SESSION_EXTDS, UDS_SUB_SESSION_SSDS};

    if (iudsCheckRequiredSession(UDS_REQ_READ_BY_ID, &reqSessionTypes[0], 4) == CO_FALSE) {
        iudsSendAbort(addr, pData, UDS_NRC_CNC, 0);
        return (RET_OK);
    }

    switch (pData[0u]) {
    /* read data by identifier */
    case UDS_REQ_READ_BY_ID:
        if (dataSize < 3) {
            iudsSendAbort(addr, pData, UDS_NRC_IMLOIF, 0);
        }
        retVal = readByIdentifier(addr, pData, &dataSize);
        if (retVal != RET_OK) {
            iudsSendRetAbort(addr, pData, retVal, 0);
            return (RET_ERROR_PRESENT_DEVICE_STATE);
        }
        pData[0] = UDS_RESP_READ_BY_ID;
        break;

    case UDS_REQ_WRITE_BY_ID:
        if (dataSize < 4) {
            iudsSendAbort(addr, pData, UDS_NRC_IMLOIF, 0);
        }
        retVal = writeByIdentifier(addr, pData, dataSize);
        if (retVal != RET_OK) {
            iudsSendRetAbort(addr, pData, retVal, 0);
            return (RET_ERROR_PRESENT_DEVICE_STATE);
        }
        pData[0] = UDS_RESP_WRITE_BY_ID;
        dataSize = 3;
        break;

    default:
        break;
    }

    iudsSendResponse(addr, pData, dataSize, 0u);
    /*iudsSendResponseWA(addr, pData, dataSize, 0u);*/

    return (RET_OK);
}


/******************************************************************************
* \brief iudsDataTransMsgHandler -
*
*/
static RET_T readByIdentifier (
		uint8_t   addr,
		uint8_t  *pData,
		uint16_t *dataSize      )
{
    RET_T     retVal = RET_OK;
    uint16_t  ident[MAX_RBI_CNT];
    uint16_t  idCnt;
    uint16_t  bufOffset;
    int       i;

    // call user indication
    if (pDataTransmitFct == NULL) {
        return (RET_UDS_NRC_ROOR);   // out of range
    }
    
    //DBG_RAM_Trace_2 ( 0xB0, *dataSize );

    // max MAX_RBI_CNT entries possible
    idCnt = (*dataSize - 1u) >> 1;
    if (idCnt > MAX_RBI_CNT) {
        return (RET_UDS_NRC_RTL);
    }
    /* save all identifier before buffer is overwritten */
    for (i = 0; i < idCnt; i++) {
        ident[i] = (uint16_t)((pData[(i * 2) + 1] << 8) | pData[(i * 2) + 2]);
    }
    /* now call indication */
    bufOffset = 1; /* uds cmd */
    for (i = 0; i < idCnt; i++) {
        /* check free buffer size (min 2byte ident and 1byte data */
        if ((UDS_BUFFER_SIZE - bufOffset) < 3) {
            return (RET_UDS_NRC_RTL);
        }
        /* save identifier at buffer */
        pData[bufOffset] = ident[i] >> 8;
        pData[bufOffset + 1] = ident[i] & 0xff;
        bufOffset += 2;
        /* calculate buffer size */
        *dataSize = UDS_BUFFER_SIZE - bufOffset;

        //DBG_RAM_Trace_3 ( 0xB1, ((*dataSize) >> 8U), *dataSize );

        retVal = pDataTransmitFct(addr, 0, ident[i], &pData[bufOffset], dataSize);

        //DBG_RAM_Trace_2 ( 0xB2, *dataSize );

        if (retVal != RET_OK) {
            return (retVal);
        }
        bufOffset += *dataSize;
    }
    *dataSize = bufOffset;

    //DBG_RAM_Trace_2 ( 0xB3, *dataSize );

    return (retVal);
}


/******************************************************************************
* \brief writeByIdentifier -
*
*/
static RET_T writeByIdentifier (
		uint8_t   addr,
		uint8_t  *pData,
		uint16_t  dataSize    )
{
    RET_T     retVal;
    uint16_t  ident = 0;

    // call user indication
    if (pDataTransmitFct == NULL) {
        return (RET_UDS_NRC_ROOR);   // out of range
    }

    /* now call indication */
    ident = pData[2];
    ident |= (uint16_t)(pData[1] << 8);

    dataSize -= 3u;
    retVal = pDataTransmitFct(addr, 1, ident, &pData[3], &dataSize);

    return (retVal);
}


/******************************************************************************
* \brief udsEventRegister_UDS_READ_WRITE_ID -
*
*/
RET_T udsEventRegister_UDS_READ_WRITE_ID (
		UDS_EVENT_DATA_TRANSMIT_T pFct      )
{
    pDataTransmitFct = pFct;
    return (RET_OK);
}
