/*
* iso_tp.c - contains emcy routines
*
*-------------------------------------------------------------------------------
*
*-------------------------------------------------------------------------------
*/

/******************************************************************************
* \brief iso-tp implementation - iso 15765-2
*
* \file iso_tp.c
* contains iso-tp routines
*
* \section i Introduction
* The ISO-TP Stack is a software library providing all
* the communication services of the
* "CANopen Application Layer and Communication Profile"
* ISO 15765-2,
*
*
* The main features are:
*   - well-defined interface between driver and iso-tp stack
*   - ANSI-C conform
*   - MISRA checked
*   - easy-to-handle Application Programming Interface
*   - configurable and scalable
*
* This reference manual describes the functions
* for the API to evaluate the received data
* and to use the iso-tp services in the network.
*
* Configuration and features settings are supported
* by the graphical configuration tool ISO-TP DeviceDesigner.
*
* \section g General
* The iso-tp stack uses only data pointers from the application, so the
* application has to ensure that all the data pointers are valid and protected
* during runtime.
*
* \section u Using ISO TP in an application
* At startup, some initialization functions are necessary:
*   - codrvHardwareInit()   - generic, CAN related hardware initialization
*   - codrvCanInit()    - initialize CAN driver (Classical Mode)
*   - codrvCanfdInit()  - initialize CAN driver (FD-Mode)
*   - coCanOpenStackInit()  - initialize CANopen and ISO TP functionality
*   - codrvTimerSetup() - initialize hardware timer
*   - codrvCanEnable()  - start CAN communication
*
* For the CANopen functionality,
* the central function
* coCommTask()
* has to be called in case of
*   - new CAN message was received
*   - timer period has been ellapsed.
*
* Therefore signal handlers should be used
* or a cyclic call of the function coCommTask() is necessary.
* For operating systems (like LINUX) the function
* codrvWaitForEvent()
* can be used to wait for events.
* <br>All CANopen functionality is handled inside this function.
*
* The start of CANopen services are also possible.
*
* \section c Indication functions
* Indication functions inform application about CAN and ISO-TP service events.
* <br>To receive an indication,
* the application has to register a function
* by the apropriate service register function like isoTpEventRegister_CLIENT().
* <br>Every time the event occures,
* the registered indication function is called.
*/

/* header of standard C - libraries
------------------------------------------------------------------------------*/
#include <stddef.h>
#include <string.h>

/* header of project specific types
------------------------------------------------------------------------------*/
#include <gen_define.h>

#ifdef ISOTP_SUPPORTED

#include <co_datatype.h>
#include <co_drv.h>
#include <co_timer.h>
#include <iso_tp.h>
#include "ico_cobhandler.h"
#include "ico_queue.h"
#include "ico_event.h"
#include "iiso_tp.h"
#include <bl_user.h>

/* constant definitions
------------------------------------------------------------------------------*/
#ifndef ISOTP_MAX_DATA_SIZE
#define ISOTP_MAX_DATA_SIZE (4u * 1024u)
#endif /* ISOTP_MAX_DATA_SIZE */

#ifndef ISOTP_MAX_BLOCK_SIZE
# define ISOTP_MAX_BLOCK_SIZE   20u
#endif /* ISOTP_MAX_BLOCK_SIZE */

#ifndef ISOTP_BLOCK_TIMEOUT
# define ISOTP_BLOCK_TIMEOUT    2u
#endif /* ISOTP_BLOCK_TIMEOUT */

#define ISOTP_FRAME_MASK            0xf0u
#define ISOTP_FRAME_SINGLE          0x00u
#define ISOTP_FRAME_FIRST           0x10u
#define ISOTP_FRAME_CONSECUTIVE     0x20u
#define ISOTP_FRAME_FLOW_CONTROL    0x30u


/* local defined data types
------------------------------------------------------------------------------*/
typedef struct  {
    CO_EVENT_T              event;
    CO_TIMER_T              timer;
    COB_REFERENZ_T          trCob;
    COB_REFERENZ_T          recCob;
    ISOTP_SERVER_STATE_T    state;
    uint16_t                expectedSize;
    uint16_t                transferdSize;
#ifdef ISOTP_EVENT_SERVER_DATA_SPLIT
    uint16_t                splitTransSize;
#endif /* ISOTP_EVENT_SERVER_DATA_SPLIT */
    uint16_t                recMaxSize;
    uint8_t                *pRecData;
    uint8_t                *pData;
    uint8_t                 seqNbr;
    uint8_t                 blockCnt;
//  uint8_t                 destAddr;
    uint8_t                 srcAddr;
    uint8_t                 timeout;
    ISO_TP_ADDRESS_T        type;
} ISO_TP_CONN_T;


/* list of external used functions, if not in headers
------------------------------------------------------------------------------*/

/* list of global defined functions
------------------------------------------------------------------------------*/

/* list of local defined functions
------------------------------------------------------------------------------*/
static void reqFlowControl(ISO_TP_CONN_T *pIsoTp);
static void recDataMsg(ISO_TP_CONN_T *pIsoTp, const CO_REC_DATA_T *pRecData);
static void sendDataMsg(void *pData);

static void recFlowControl(ISO_TP_CONN_T *pIsoTp, const CO_REC_DATA_T *pRecData);
static void recFlowControlWait(void *pData);

static RET_T serverUserHandler(ISO_TP_CONN_T *pIsoTp);
static RET_T clientUserHandler(ISO_TP_CONN_T *pIsoTp, RET_T result);
static RET_T clientTxAckUserHandler(ISO_TP_CONN_T* pIsoTp, uint8_t* pData, uint16_t size);

static ISO_TP_ADDRESS_T currentType = {ISOTP_ADDRESS_UNKNOWN};

/* external variables
------------------------------------------------------------------------------*/

/* global variables
------------------------------------------------------------------------------*/
#define GWA_ENABLED     1U
#define GWA_DSABLED     0U
#define GWA_NUMPDU_TH   10U
static uint8_t       flag_gwa_En = GWA_DSABLED;
static ISO_TP_CONN_T isotpwaChan = {0};

/* local defined variables
------------------------------------------------------------------------------*/

#ifdef ISOTP_SERVER_CONNECTION_CNT

static ISO_TP_CONN_T isotpServerChan[ISOTP_SERVER_CONNECTION_CNT];
static uint16_t isotpServerChanCnt = 0u;

# ifdef ISOTP_EVENT_DYNAMIC_SERVER
static ISOTP_EVENT_SERVER_T isotpServerTable[ISOTP_EVENT_DYNAMIC_SERVER];
static uint16_t isotpServerTableCnt = 0u;
# endif /* ISOTP_EVENT_DYNAMIC_SERVER */

# ifdef ISOTP_EVENT_DYNAMIC_SSPLIT
static ISOTP_EVENT_SERVER_SPLIT_T isotpServerSplitTable[ISOTP_EVENT_DYNAMIC_SSPLIT];
static uint16_t isotpServSplitTableCnt = 0u;
# endif /* ISOTP_EVENT_DYNAMIC_SSPLIT */

#endif /* ISOTP_SERVER_CONNECTION_CNT */


#ifdef ISOTP_CLIENT_CONNECTION_CNT

static ISO_TP_CONN_T isotpClientChan[ISOTP_CLIENT_CONNECTION_CNT];
static uint16_t isoTpClientChanCnt = 0u;

# ifdef ISOTP_EVENT_DYNAMIC_CLIENT
static ISOTP_EVENT_CLIENT_T isotpClientTable[ISOTP_EVENT_DYNAMIC_CLIENT];
static uint16_t isotpClientTableCnt = 0u;
# endif /* ISOTP_EVENT_DYNAMIC_CLIENT */

# ifdef ISOTP_EVENT_DYNAMIC_CLIENT_TXACK
static ISOTP_EVENT_CLIENT_TXACK_T   isotpClientTxAckTable[ISOTP_EVENT_DYNAMIC_CLIENT_TXACK];
static uint16_t isotpClientTxAckTableCnt = 0u;
# endif /* ISOTP_EVENT_DYNAMIC_CLIENT_TXACK */

#endif /* ISOTP_CLIENT_CONNECTION_CNT */



/* implementation
------------------------------------------------------------------------------*/

#ifdef ISOTP_CLIENT_CONNECTION_CNT
/***************************************************************************
* \brief isoTpSendReq - send isotp request
*
* This function transmit the given data to the destAddr.
* The isotp channel has to be initialized before.
*
* Please note - the data has to be valid until transfer was finished.
*
* \return RET_T
*
*/
RET_T isoTpSendReq (
        uint8_t   destAddr,         /**< destination address */
        uint8_t  *pData,            /**< pointer to transmit data */
        uint16_t  dataLen,          /**< length of transmit data */
        uint8_t   flags             /**< flags for inhibit/indicaton */ )
{
    RET_T           retVal;
    uint16_t        cnt;
    ISO_TP_CONN_T  *pIsoTp = NULL;
    uint8_t         trData[CO_CAN_MAX_DATA_LEN];

    if (pData == NULL) {
        return (RET_HARDWARE_ERROR);
    }
    if (dataLen > (4u * 1024u)) {
        return (RET_INVALID_PARAMETER);
    }
    for (cnt = 0u; cnt < isoTpClientChanCnt; cnt++) {
        if (isotpClientChan[cnt].srcAddr == destAddr) {
            pIsoTp = &isotpClientChan[cnt];
            break;
        }
    }
    if (pIsoTp == NULL) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    if (pIsoTp->state != ISOTP_STATE_CLIENT_FREE) {
        return (RET_SERVICE_BUSY);
    }

    if (pIsoTp->type == ISOTP_ADDRESS_FUNC) {
        if (dataLen > (CO_CAN_MAX_DATA_LEN - 1u)) {
            return (RET_DATA_TYPE_MISMATCH);
        }
    }

    memset(trData, 0x55u, CO_CAN_MAX_DATA_LEN);

    if (dataLen > (CO_CAN_MAX_DATA_LEN - 1u)) {
        pIsoTp->expectedSize = dataLen;
        pIsoTp->transferdSize = 6u;
        pIsoTp->pData = pData;
        pIsoTp->seqNbr = 0u;

        trData[1u] = (uint8_t)(dataLen & 0xffu);
        dataLen >>= 8u;
        trData[0u] = (uint8_t)(dataLen & 0xffu);
        trData[0u] |= 0x10u;
        memcpy(&trData[2], pData, 6u);

        pIsoTp->state = ISOTP_STATE_CLIENT_WAIT_CTS;
        retVal = icoTransmitMessage(pIsoTp->trCob, &trData[0], 0u);
    } else {
        trData[0u] = (uint8_t)dataLen;
        memcpy(&trData[1], pData, dataLen);
        pIsoTp->state = ISOTP_STATE_CLIENT_FREE;
        retVal = icoTransmitMessage(pIsoTp->trCob, &trData[0], flags);
        clientUserHandler(pIsoTp, retVal);
    }

    //DBG_RAM_Trace_3 ( 0xCA, pIsoTp->expectedSize, pIsoTp->transferdSize );

    return (retVal);
}


/******************************************************************************
* \brief isoTpSendReqWA -
*
*/
RET_T isoTpSendReqWA (
        uint8_t   destAddr,
        uint8_t  *pData,
        uint16_t  dataLen,
        uint8_t   flags       )
{
    RET_T           retVal;
    uint16_t        cnt;
    ISO_TP_CONN_T  *pIsoTp = NULL;
    uint8_t         trData[CO_CAN_MAX_DATA_LEN];

    if (pData == NULL) {
        return (RET_HARDWARE_ERROR);
    }
    if (dataLen > (4u * 1024u)) {
        return (RET_INVALID_PARAMETER);
    }
    for (cnt = 0u; cnt < isoTpClientChanCnt; cnt++) {
        if (isotpClientChan[cnt].srcAddr == destAddr) {
            pIsoTp = &isotpClientChan[cnt];
            break;
        }
    }
    if (pIsoTp == NULL) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    if (pIsoTp->state != ISOTP_STATE_CLIENT_FREE) {
        return (RET_SERVICE_BUSY);
    }

    if (pIsoTp->type == ISOTP_ADDRESS_FUNC) {
        if (dataLen > (CO_CAN_MAX_DATA_LEN - 1u)) {
            return (RET_DATA_TYPE_MISMATCH);
        }
    }

    if (dataLen > (CO_CAN_MAX_DATA_LEN - 1u)) {
        flag_gwa_En = GWA_ENABLED;
        isotpwaChan.expectedSize = dataLen;
        isotpwaChan.transferdSize = 6u;
        isotpwaChan.pData = pData;
        isotpwaChan.seqNbr = 0u;

        pIsoTp->expectedSize = dataLen;
        pIsoTp->transferdSize = 6u;
        pIsoTp->pData = pData;
        pIsoTp->seqNbr = 0u;

        trData[1u] = (uint8_t)(dataLen & 0xffu);
        dataLen >>= 8u;
        trData[0u] = (uint8_t)(dataLen & 0xffu);
        trData[0u] |= 0x10u;
        memcpy(&trData[2], pData, 6u);

        pIsoTp->state = ISOTP_STATE_CLIENT_FREE;
        retVal = icoTransmitMessage(pIsoTp->trCob, &trData[0], 0u);
    } else {
        trData[0u] = (uint8_t)dataLen;
        memcpy(&trData[1], pData, dataLen);
        pIsoTp->state = ISOTP_STATE_CLIENT_FREE;
        retVal = icoTransmitMessage(pIsoTp->trCob, &trData[0], flags);
        clientUserHandler(pIsoTp, retVal);
    }

    return (retVal);
}
#endif /* ISOTP_CLIENT_CONNECTION_CNT */


/***************************************************************************
* \brief isoTpSetRecDataPtr - set ISO-TP receive pointer
*
* This function sets the adress and the size of a receive connection.<br>
* At the initialization time,
* there are no receive buffers.
* This has to be done by this function.
*
* \return RET_T
*
*/
RET_T isoTpSetRecDataPtr (
        ISO_TP_DATAPTR_T  direction,
        uint8_t           destAddr,
        uint8_t          *pRecData,
        uint16_t          size            )
{
#ifdef ISOTP_SERVER_CONNECTION_CNT
    uint16_t        cnt;
#endif /* ISOTP_SERVER_CONNECTION_CNT */
    ISO_TP_CONN_T  *pIsoTp = NULL;

    if (size > ISOTP_MAX_DATA_SIZE) {
        return (RET_INVALID_PARAMETER);
    }

    if (direction == ISOTP_DATAPTR_SERVER) {
#ifdef ISOTP_SERVER_CONNECTION_CNT
        for (cnt = 0u; cnt < ISOTP_SERVER_CONNECTION_CNT; cnt++) {
            if (isotpServerChan[cnt].srcAddr == destAddr) {
                pIsoTp = &isotpServerChan[cnt];
                break;
            }
        }
    } else {
#else /* ISOTP_SERVER_CONNECTION_CNT */
        return (RET_INVALID_PARAMETER);
#endif /* ISOTP_SERVER_CONNECTION_CNT */
    }

    if (pIsoTp == NULL) {
        return (RET_EVENT_NO_RESSOURCE);
    }
    if (size > ISOTP_MAX_DATA_SIZE) {
        return (RET_INVALID_PARAMETER);
    }

    pIsoTp->pRecData = pRecData;
    pIsoTp->recMaxSize = size;

    return (RET_OK);
}


#ifdef ISOTP_CLIENT_CONNECTION_CNT
/******************************************************************************
* \brief iisoTpMessageHandler - handles all iso-tp messages
*
*/
void iisoTpClientMessageHandler (
        const CO_REC_DATA_T *pRecData       /* pointer to received data */ )
{
    ISO_TP_CONN_T *pIsoTp;
    uint8_t        command;

    /* check max service index */
    if (pRecData->spec >= isoTpClientChanCnt) {
        return;
    }
    pIsoTp = &isotpClientChan[pRecData->spec];

    /* the command is the most left nibble */
    command = pRecData->msg.data[0u] & ISOTP_FRAME_MASK;

    if (command == ISOTP_FRAME_FLOW_CONTROL) {
        recFlowControl(pIsoTp, pRecData);
    }
}
#endif /* ISOTP_CLIENT_CONNECTION_CNT */


#ifdef ISOTP_SERVER_CONNECTION_CNT
/******************************************************************************
* \brief iisoTpMessageHandler - handles all iso-tp messages
*
*/
void iisoTpServerMessageHandler (
        const CO_REC_DATA_T *pRecData       /* pointer to received data */ )
{
    ISO_TP_CONN_T *pIsoTp;
    uint8_t        command;

    /* check max service index */
    if (pRecData->spec >= isotpServerChanCnt) {
        return;
    }
    pIsoTp = &isotpServerChan[pRecData->spec];

    //DBG_RAM_Trace_4 ( bl_dbg_cnt, 0xEE, pRecData->service, pRecData->msg.data[0u] );

    //DBG_RAM_Trace_4 ( pIsoTp->pRecData[0u], pIsoTp->pRecData[1u], pIsoTp->pRecData[2u], pIsoTp->pRecData[3u] );
    
    //DBG_RAM_Trace_4 ( pIsoTp->pRecData[4u], pIsoTp->pRecData[5u], pIsoTp->pRecData[6u], pIsoTp->pRecData[7u] );

    //DBG_RAM_Trace_2 ( pIsoTp->pRecData[8u], pIsoTp->pRecData[9u] );

    //DBG_RAM_Trace_3 ( 0xF3, pIsoTp->transferdSize, pIsoTp->expectedSize );

    /* the command is the most left nibble */
    command = pRecData->msg.data[0u] & ISOTP_FRAME_MASK;

    switch (command) {
    case ISOTP_FRAME_SINGLE:
        if (pIsoTp->pRecData == NULL) {
            return;
        }
        if (pRecData->msg.data[0u] > 7u) {
            return;
        }
        if (pIsoTp->recMaxSize >= pRecData->msg.data[0u]) {
            memcpy(pIsoTp->pRecData, &(pRecData->msg.data[1u]), pRecData->msg.data[0u]);
            pIsoTp->transferdSize = pRecData->msg.data[0u];
            pIsoTp->expectedSize = pRecData->msg.data[0u];
            /* call user indication */
            serverUserHandler(pIsoTp);

            //DBG_RAM_Trace_1 ( 0xE4 );
            
            //DBG_RAM_Trace_4 ( pIsoTp->pRecData[0u], pIsoTp->pRecData[1u], pIsoTp->pRecData[2u], pIsoTp->pRecData[3u] );
            
            //DBG_RAM_Trace_4 ( pIsoTp->pRecData[4u], pIsoTp->pRecData[5u], pIsoTp->pRecData[6u], pIsoTp->pRecData[7u] );

            //DBG_RAM_Trace_2 ( pIsoTp->pRecData[8u], pIsoTp->pRecData[9u] );

            //DBG_RAM_Trace_3 ( 0xF4, pIsoTp->transferdSize, pIsoTp->expectedSize );

        }
        break;

    case ISOTP_FRAME_FIRST:
        if (pIsoTp->type == ISOTP_ADDRESS_FUNC) {
            return;
        }
        memcpy(pIsoTp->pRecData, &(pRecData->msg.data[2u]), 6u);
        pIsoTp->transferdSize = 6u;

        pIsoTp->expectedSize = pRecData->msg.data[0u] & 0x0fu;
        pIsoTp->expectedSize = pIsoTp->expectedSize << 8u;
        pIsoTp->expectedSize |= pRecData->msg.data[1u];

        if (pIsoTp->expectedSize > pIsoTp->recMaxSize) {
            /* ignore in case we cannot handle the size */
            return;
        }

        pIsoTp->seqNbr = 1u;
        pIsoTp->blockCnt = 0u;

#ifdef ISOTP_EVENT_SERVER_DATA_SPLIT
        pIsoTp->splitTransSize = 6u;
#endif /* ISOTP_EVENT_SERVER_DATA_SPLIT */

        pIsoTp->state = ISOTP_STATE_SERVER_WAIT_DATA;

        /* call flow control request */
        reqFlowControl(pIsoTp);
        break;

    case ISOTP_FRAME_CONSECUTIVE:
        recDataMsg(pIsoTp, pRecData);
        break;

    case ISOTP_FRAME_FLOW_CONTROL:
#if 0
        if (flag_gwa_En == GWA_ENABLED) {
            uint8_t wa_trData[CO_CAN_MAX_DATA_LEN] = {0};
            while ((isotpwaChan.transferdSize < isotpwaChan.expectedSize) && (isotpwaChan.seqNbr < GWA_NUMPDU_TH))
            {
                isotpwaChan.seqNbr++;
                wa_trData[0U] = ISOTP_FRAME_CONSECUTIVE + isotpwaChan.seqNbr;

                memcpy(&wa_trData[1U], &isotpwaChan.pData[isotpwaChan.transferdSize], CO_CAN_MAX_DATA_LEN - 1U);

                isotpClientChan[0U].state = ISOTP_STATE_CLIENT_FREE;
                RET_T retVal = icoTransmitMessage(isotpClientChan[0U].trCob, &wa_trData[0], 0U);
                clientUserHandler(&isotpClientChan[0U], retVal);

                if (isotpwaChan.expectedSize < (isotpwaChan.transferdSize + CO_CAN_MAX_DATA_LEN))
                    isotpwaChan.transferdSize = isotpwaChan.expectedSize;
                else
                    isotpwaChan.transferdSize += (CO_CAN_MAX_DATA_LEN - 1U);
            }
        }
        flag_gwa_En = GWA_DSABLED;
#endif
        recFlowControl(pIsoTp, pRecData);
        break;

    default:
        break;
    }
}
#endif /* ISOTP_SERVER_CONNECTION_CNT */


#ifdef ISOTP_CLIENT_CONNECTION_CNT
/******************************************************************************
* \brief iisoTpClientTxAck -
*
*/
RET_T iisoTpClientTxAck (
        COB_REFERENZ_T   cobRef,
        CO_CAN_TR_MSG_T *pMsg         )
{
    uint16_t       cnt;
    ISO_TP_CONN_T *pIsoTp = NULL;

    for (cnt = 0u; cnt < isoTpClientChanCnt; cnt++) {
        if (isotpClientChan[cnt].trCob == cobRef) {
            pIsoTp = &isotpClientChan[cnt];
            break;
        }
    }
    if (pIsoTp == NULL) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    if (pMsg->data[0u] > 7u) {
        return (RET_DATA_TYPE_MISMATCH);
    }

    (void)clientTxAckUserHandler(pIsoTp, &pMsg->data[1], pMsg->data[0]);

    return (RET_OK);
}
#endif /* ISOTP_CLIENT_CONNECTION_CNT */


/******************************************************************************
* \brief recDataMsg - handles all received data messages
*
*/
static void recDataMsg (
        ISO_TP_CONN_T       *pIsoTp,
        const CO_REC_DATA_T *pRecData       )
{
#ifdef ISOTP_EVENT_SERVER_DATA_SPLIT
    uint16_t splitCnt;
#endif /* ISOTP_EVENT_SERVER_DATA_SPLIT */

    if (pIsoTp->state != ISOTP_STATE_SERVER_WAIT_DATA) {
        // TODO abort
        return;
    }

    if ((pRecData->msg.data[0u] & 0x0fu) != pIsoTp->seqNbr) {
        pIsoTp->state = ISOTP_STATE_SERVER_FREE;
        return;
    }

    pIsoTp->seqNbr++;
    pIsoTp->seqNbr &= 0x0fu;

    pIsoTp->blockCnt++;

#ifdef ISOTP_EVENT_SERVER_DATA_SPLIT
    if ((pIsoTp->splitTransSize + 7u) >= ISOTP_SERVER_DATA_SPLIT_CNT) {

        splitCnt = (pIsoTp->splitTransSize + 7u) - ISOTP_SERVER_DATA_SPLIT_CNT;
        splitCnt = (CO_CAN_MAX_DATA_LEN - 1u) - splitCnt;

        memcpy(&pIsoTp->pRecData[pIsoTp->splitTransSize], &pRecData->msg.data[1u], splitCnt);
        pIsoTp->splitTransSize += splitCnt;
        pIsoTp->transferdSize += splitCnt;
        (void)serverUserHandler(pIsoTp);
        pIsoTp->splitTransSize = 0u;

        splitCnt = (CO_CAN_MAX_DATA_LEN - 1u) - splitCnt;
        memcpy(&pIsoTp->pRecData[pIsoTp->splitTransSize], &pRecData->msg.data[CO_CAN_MAX_DATA_LEN - splitCnt], splitCnt);
        pIsoTp->splitTransSize += splitCnt;
        pIsoTp->transferdSize += splitCnt;

    } else {
        memcpy(&pIsoTp->pRecData[pIsoTp->splitTransSize], &pRecData->msg.data[1u], CO_CAN_MAX_DATA_LEN - 1u);
        pIsoTp->transferdSize += (CO_CAN_MAX_DATA_LEN - 1u);
        pIsoTp->splitTransSize += (CO_CAN_MAX_DATA_LEN - 1u);
    }

    if (pIsoTp->transferdSize >= pIsoTp->expectedSize) {
        pIsoTp->splitTransSize = pIsoTp->expectedSize % ISOTP_SERVER_DATA_SPLIT_CNT;
    }
#else /* ISOTP_EVENT_SERVER_DATA_SPLIT */

    memcpy(&pIsoTp->pRecData[pIsoTp->transferdSize], &pRecData->msg.data[1u], CO_CAN_MAX_DATA_LEN - 1u);
    pIsoTp->transferdSize += (CO_CAN_MAX_DATA_LEN - 1u);
#endif /* ISOTP_EVENT_SERVER_DATA_SPLIT */

    if (pIsoTp->transferdSize >= pIsoTp->expectedSize) {

        pIsoTp->state = ISOTP_STATE_SERVER_FREE;

        pIsoTp->transferdSize = pIsoTp->expectedSize;
        /* call user indication */
        (void)serverUserHandler(pIsoTp);
        return;
    }
    if (pIsoTp->blockCnt >= ISOTP_MAX_BLOCK_SIZE) {
        reqFlowControl(pIsoTp);
        pIsoTp->blockCnt = 0u;
    }
}


/******************************************************************************
* \brief reqFlowControl - sends flow control message
*
*/
static void reqFlowControl (
        ISO_TP_CONN_T *pIsoTp       )
{
    uint8_t trData[CO_CAN_MAX_DATA_LEN];

    memset(trData, 0x55u, CO_CAN_MAX_DATA_LEN);

    trData[0u] = 0x30u;
    if (pIsoTp->expectedSize > pIsoTp->recMaxSize) {
        trData[0u] |= (uint8_t)2u;
    }

    trData[1u] = ISOTP_MAX_BLOCK_SIZE;
    trData[2u] = ISOTP_BLOCK_TIMEOUT;

    (void)icoTransmitMessage(pIsoTp->trCob, &trData[0], 0u);
}


/******************************************************************************
* \brief recFlowControl - handles received flow control
*
*/
static void recFlowControl (
        ISO_TP_CONN_T       *pIsoTp,
        const CO_REC_DATA_T *pRecData      )
{
    uint8_t command;

    pIsoTp->timeout = pRecData->msg.data[2u];
    pIsoTp->blockCnt = pRecData->msg.data[1u];

    if (pIsoTp->blockCnt == 0u) {
        pIsoTp->blockCnt = 0xffu;
    }

    command = pRecData->msg.data[0u];
    command = command & 0x0fu;

    switch(command) {
    case 0x00u: /* Clear To Send */
        sendDataMsg(pIsoTp);
        if(pIsoTp->timeout > 0u) {
            coTimerStart( &pIsoTp->timer, (uint32_t)pIsoTp->timeout * 1000ul,
                          sendDataMsg, (void *)pIsoTp, CO_TIMER_ATTR_ROUNDUP_CYCLIC );
        }
        break;
    case 0x01u: /* wait for next frame */
        /* we are supposed to wait, so we wait one second */
        coTimerStart( &pIsoTp->timer, 1000ul * 1000ul,
                      recFlowControlWait, (void *)pIsoTp, CO_TIMER_ATTR_ROUNDDOWN );
        break;
    /* case 0x02: *//* overflow at receiver side */
    default:
        break;
    }
}


/******************************************************************************
* \brief recFlowControlWait - timer callback for waiting after flow control
*
*/
static void recFlowControlWait (
        void *pData    )
{
    ISO_TP_CONN_T   *pIsoTp = pData;

    sendDataMsg(pIsoTp);
    coTimerStart( &pIsoTp->timer, (uint32_t)pIsoTp->timeout * 1000ul,
                  sendDataMsg, pIsoTp, CO_TIMER_ATTR_ROUNDUP_CYCLIC );
}


/******************************************************************************
* \brief sendDataMsg - sends data message
*
*/
static void sendDataMsg (
        void *pData    )
{
    uint8_t        trData[CO_CAN_MAX_DATA_LEN];
    uint16_t       size;
    ISO_TP_CONN_T *pIsoTp = (ISO_TP_CONN_T *)pData;

    size = (uint16_t)(pIsoTp->expectedSize - pIsoTp->transferdSize);
    if (size >= 7u) {
        size = 7u;
    }

    //DBG_RAM_Trace_4 ( 0xAB, pIsoTp->expectedSize, pIsoTp->transferdSize, size );

    if ((size == 0u) || (pIsoTp->blockCnt == 0u))  {
        /* nothing to send anymore */
        pIsoTp->state = ISOTP_STATE_CLIENT_FREE;
        coTimerStop(&pIsoTp->timer);
        clientUserHandler(pIsoTp, RET_OK);
        return;
    }
    memset(trData, 0x55u, CO_CAN_MAX_DATA_LEN);

    pIsoTp->seqNbr++;
    pIsoTp->seqNbr &= 0x0fu;

    trData[0u] = 0x20u;
    trData[0u] |= pIsoTp->seqNbr;

    memcpy(&trData[1], &(pIsoTp->pData[pIsoTp->transferdSize]), size);
    pIsoTp->transferdSize += size;
    pIsoTp->blockCnt--;

    (void)icoTransmitMessage(pIsoTp->trCob, &trData[0], 0u);

    if (pIsoTp->timeout == 0u)  {
        icoEventStart(&pIsoTp->event, sendDataMsg, pIsoTp);
    }

}


/******************************************************************************
* \brief serverUserHandler - call iso-tp server user indication
*
*/
static RET_T serverUserHandler (
        ISO_TP_CONN_T *pIsoTp      )
{
    uint16_t cnt;

    /* save connection type for callback */
    currentType = pIsoTp->type;

    //DBG_RAM_Trace_3 ( 0xF5, pIsoTp->expectedSize, pIsoTp->transferdSize );

#ifdef ISOTP_EVENT_SERVER_DATA_SPLIT
    cnt = isotpServSplitTableCnt;
    while(cnt--) {
        isotpServerSplitTable[cnt]( pIsoTp->srcAddr, &pIsoTp->pRecData[0u],
                                    pIsoTp->splitTransSize, pIsoTp->transferdSize );
    }
#endif /* ISOTP_EVENT_SERVER_DATA_SPLIT */

    //DBG_RAM_Trace_2 ( pIsoTp->expectedSize, pIsoTp->transferdSize );

#ifdef ISOTP_EVENT_DYNAMIC_SERVER
    if (pIsoTp->transferdSize == pIsoTp->expectedSize) {
        cnt = isotpServerTableCnt;
        while (cnt--) {
            isotpServerTable[cnt]( pIsoTp->srcAddr, &pIsoTp->pRecData[0u],
                                   pIsoTp->transferdSize );
        }
    }
#endif /* ISOTP_EVENT_DYNAMIC_SERVER */

    //DBG_RAM_Trace_2 ( pIsoTp->expectedSize, pIsoTp->transferdSize );

    currentType = ISOTP_ADDRESS_UNKNOWN;

    return (RET_OK);
}


/******************************************************************************
* \brief clientUserHandler - call iso-tp client user indication
*
*/
static RET_T clientUserHandler (
        ISO_TP_CONN_T *pIsoTp,
        RET_T          result       )
{
#ifdef ISOTP_EVENT_DYNAMIC_CLIENT
    uint16_t cnt;

    cnt = isotpClientTableCnt;
    while(cnt--)  {
        isotpClientTable[cnt]( pIsoTp->srcAddr, &pIsoTp->pRecData[0u], result );
    }
#else /* ISOTP_EVENT_DYNAMIC_CLIENT */
    (void)pIsoTp;
    (void)result;
#endif /* ISOTP_EVENT_DYNAMIC_CLIENT */

    return (RET_OK);
}


/******************************************************************************
* \brief clientTxAckUserHandler - call iso-tp client user indication 
*
*/
static RET_T clientTxAckUserHandler (
        ISO_TP_CONN_T *pIsoTp,
        uint8_t       *pData,
        uint16_t       size       )
{
#ifdef ISOTP_EVENT_DYNAMIC_CLIENT_TXACK
    uint16_t cnt;

    cnt = isotpClientTxAckTableCnt;
    while (cnt--) {
        isotpClientTxAckTable[cnt]( pIsoTp->srcAddr, pData, size );
    }
#else /* ISOTP_EVENT_DYNAMIC_CLIENT_TXACK */
    (void)pIsoTp;
#endif /* ISOTP_EVENT_DYNAMIC_CLIENT_TXACK */

    return (RET_OK);
}


#ifdef ISOTP_EVENT_DYNAMIC_SERVER
/******************************************************************************
* \brief isoTpEventRegister_SERVER - register iso-tp server event function
*
* This function registers an iso-tp write indication function.
*
* \return RET_T
*
*/
RET_T isoTpEventRegister_SERVER (
        ISOTP_EVENT_SERVER_T  pFunction   /**< pointer to function */ )
{
    if (isotpServerTableCnt >= ISOTP_EVENT_DYNAMIC_SERVER) {
        return (RET_EVENT_NO_RESSOURCE);
    }
    /* save function pointer */
    isotpServerTable[isotpServerTableCnt] = pFunction;
    isotpServerTableCnt++;

    return (RET_OK);
}
#endif /* ISOTP_EVENT_DYNAMIC_SERVER */


#ifdef ISOTP_EVENT_DYNAMIC_SSPLIT
/******************************************************************************
* \brief isoTpEventRegister_SERVER - register iso-tp server event function
*
* This function registers an iso-tp write indication function.
*
* \return RET_T
*
*/
RET_T isoTpEventRegister_SERVER_SPLIT (
        ISOTP_EVENT_SERVER_SPLIT_T  pFunction   /**< pointer to function */ )
{
    if (isotpServSplitTableCnt >= ISOTP_EVENT_DYNAMIC_SSPLIT) {
        return (RET_EVENT_NO_RESSOURCE);
    }
    /* save function pointer */
    isotpServerSplitTable[isotpServSplitTableCnt] = pFunction;
    isotpServSplitTableCnt++;

    return (RET_OK);
}
#endif /* ISOTP_EVENT_DYNAMIC_SSPLIT */


#ifdef ISOTP_EVENT_DYNAMIC_CLIENT
/******************************************************************************
* \brief isoTpEventRegister_CLIENT - register iso-tp client event function
*
* This function registers an iso-tp client indication function.
*
* \return RET_T
*
*/
RET_T isoTpEventRegister_CLIENT (
        ISOTP_EVENT_CLIENT_T  pFunction   /**< pointer to function */ )
{
    if (isotpClientTableCnt >= ISOTP_EVENT_DYNAMIC_CLIENT) {
        return (RET_EVENT_NO_RESSOURCE);
    }
    /* save function pointer */
    isotpClientTable[isotpClientTableCnt] = pFunction;
    isotpClientTableCnt++;

    return (RET_OK);
}
#endif /* ISOTP_EVENT_DYNAMIC_CLIENT */


#ifdef ISOTP_EVENT_DYNAMIC_CLIENT_TXACK
/******************************************************************************
* \brief isoTpEventRegister_CLIENT - register iso-tp client event function
*
* This function registers an iso-tp client indication function.
*
* \return RET_T
*
*/
RET_T isoTpEventRegister_CLIENT_TXACK (
        ISOTP_EVENT_CLIENT_TXACK_T  pFunction   /**< pointer to function */ )
{
    if (isotpClientTxAckTableCnt >= ISOTP_EVENT_DYNAMIC_CLIENT_TXACK) {
        return (RET_EVENT_NO_RESSOURCE);
    }
    /* save function pointer */
    isotpClientTxAckTable[isotpClientTxAckTableCnt] = pFunction;
    isotpClientTxAckTableCnt++;

    return (RET_OK);
}
#endif /* ISOTP_EVENT_DYNAMIC_CLIENT_TXACK */


#ifdef ISOTP_CLIENT_CONNECTION_CNT
/******************************************************************************
* \brief isoTpInitClientChannel - initialize iso tp client channel
*
* This function initialize an iso tp client channel.
* It can be used for transmit data.
*
* \return RET_T
*
*/
RET_T isoTpInitClientChannel (
        uint32_t          reqId,            /**< request CAN id */
        uint32_t          respId,           /**< response CAN id */
        uint8_t           srcAddr,          /**< source address */
        uint8_t           destAddr,         /**< destination address */
        ISO_TP_ADDRESS_T  addrType,         /**< address type */
        ISO_TP_FORMAT_T   formatType,       /**< format type */
        uint8_t          *pRecDataBuffer,   /**< receive data buffer */
        uint16_t          recBufSize        /**< receive data buffer size */ )
{
    ISO_TP_CONN_T *pIsoTp;

    if (isoTpClientChanCnt >= ISOTP_CLIENT_CONNECTION_CNT) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    pIsoTp = &isotpClientChan[isoTpClientChanCnt];

    pIsoTp->recCob = icoCobCreate(CO_COB_TYPE_RECEIVE, CO_SERVICE_ISOTP_CLIENT, isoTpClientChanCnt);
    if (pIsoTp->recCob == 0xffffu) {
        return (RET_NO_COB_AVAILABLE);
    }

    pIsoTp->trCob = icoCobCreate(CO_COB_TYPE_TRANSMIT, CO_SERVICE_ISOTP_CLIENT, isoTpClientChanCnt);
    if (pIsoTp->trCob == 0xffffu) {
        return (RET_NO_COB_AVAILABLE);
    }

    (void)icoCobSet(pIsoTp->recCob, respId, CO_COB_RTR_NONE, 8u);
    (void)icoCobSet(pIsoTp->trCob, reqId, CO_COB_RTR_NONE, 8u);

    pIsoTp->srcAddr = destAddr;
    pIsoTp->type = addrType;

    pIsoTp->pRecData = pRecDataBuffer;
    pIsoTp->recMaxSize = recBufSize;

    isoTpClientChanCnt++;

    (void)srcAddr;
    (void)formatType;

    return (RET_OK);
}
#endif /* ISOTP_CLIENT_CONNECTION_CNT */


#ifdef ISOTP_SERVER_CONNECTION_CNT
/******************************************************************************
* \brief isoTpInitChannel - initialize iso tp channel
*
* This function initialize an iso tp channel.
* It can be used for transmit and for receive of data.
*
* \return RET_T
*
*/
RET_T isoTpInitServerChannel(
        uint32_t          reqId,            /**< request CAN id */
        uint32_t          respId,           /**< response CAN id */
        uint8_t           srcAddr,          /**< source address */
        uint8_t           destAddr,         /**< destination address */
        ISO_TP_ADDRESS_T  addrType,         /**< address type */
        ISO_TP_FORMAT_T   formatType,       /**< format type */
        uint8_t          *pRecDataBuffer,   /**< receive data buffer */
        uint16_t          recBufSize        /**< receive data buffer size */ )
{
    ISO_TP_CONN_T *pIsoTp;

    if (isotpServerChanCnt >= ISOTP_SERVER_CONNECTION_CNT) {
        return (RET_EVENT_NO_RESSOURCE);
    }

    pIsoTp = &isotpServerChan[isotpServerChanCnt];

    pIsoTp->recCob = icoCobCreate(CO_COB_TYPE_RECEIVE, CO_SERVICE_ISOTP_SERVER, isotpServerChanCnt);
    if (pIsoTp->recCob == 0xffffu) {
        return (RET_NO_COB_AVAILABLE);
    }

    pIsoTp->trCob = icoCobCreate(CO_COB_TYPE_TRANSMIT, CO_SERVICE_ISOTP_SERVER, isotpServerChanCnt);
    if (pIsoTp->trCob == 0xffffu) {
        return (RET_NO_COB_AVAILABLE);
    }

    (void)icoCobSet(pIsoTp->recCob, reqId, CO_COB_RTR_NONE, 8u);
    (void)icoCobSet(pIsoTp->trCob, respId, CO_COB_RTR_NONE, 8u);

    pIsoTp->srcAddr = srcAddr;
    pIsoTp->type = addrType;

    pIsoTp->pRecData = pRecDataBuffer;
    pIsoTp->recMaxSize = recBufSize;

    isotpServerChanCnt++;

    (void)destAddr;
    (void)formatType;

    return (RET_OK);
}
#endif /* ISOTP_SERVER_CONNECTION_CNT */


/******************************************************************************
* \brief isoTpInit - init iso tp module
*
*/
RET_T isoTpInit (
        void      )
{
    uint16_t cnt;

#ifdef ISOTP_EVENT_DYNAMIC_SERVER
    isotpServerTableCnt = 0u;

    for (cnt = 0u; cnt < ISOTP_EVENT_DYNAMIC_SERVER; cnt++) {
        isotpServerTable[cnt] = NULL;
    }
#endif /* ISOTP_EVENT_DYNAMIC_SERVER */

#ifdef ISOTP_EVENT_DYNAMIC_SSPLIT
    for (isotpServSplitTableCnt = 0u; isotpServSplitTableCnt < ISOTP_EVENT_DYNAMIC_SSPLIT; isotpServSplitTableCnt++) {
        isotpServerSplitTable[isotpServSplitTableCnt] = NULL;
    }
    isotpServSplitTableCnt = 0u;
#endif /* ISOTP_EVENT_DYNAMIC_SSPLIT */

#ifdef ISOTP_EVENT_DYNAMIC_CLIENT
    isotpClientTableCnt = 0u;

    for (cnt = 0u; cnt < ISOTP_EVENT_DYNAMIC_CLIENT; cnt++) {
        isotpClientTable[cnt] = NULL;
    }
#endif /* ISOTP_EVENT_DYNAMIC_CLIENT */

#ifdef ISOTP_CLIENT_CONNECTION_CNT
    for (isoTpClientChanCnt = 0u; isoTpClientChanCnt < ISOTP_CLIENT_CONNECTION_CNT; isoTpClientChanCnt++) {
        isotpClientChan[isoTpClientChanCnt].state = ISOTP_STATE_CLIENT_FREE;

    }
    isoTpClientChanCnt = 0u;
#endif /* ISOTP_CLIENT_CONNECTION_CNT */

#ifdef ISOTP_SERVER_CONNECTION_CNT
    for (isotpServerChanCnt = 0u; isotpServerChanCnt < ISOTP_SERVER_CONNECTION_CNT; isotpServerChanCnt++) {
        isotpServerChan[isotpServerChanCnt].state = ISOTP_STATE_SERVER_FREE;
    }
    isotpServerChanCnt = 0u;
#endif /* ISOTP_SERVER_CONNECTION_CNT */

    return (RET_OK);
}


ISO_TP_ADDRESS_T isoTpGetCurrectConType (
        void       )
{
    return (currentType);
}

#endif /* ISOTP_SUPPORTED */

