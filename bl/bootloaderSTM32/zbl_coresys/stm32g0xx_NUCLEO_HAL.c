/*
* cpu_stm32g0xx_NUCLEO_HAL.c
*
*-------------------------------------------------------------------
*
*-------------------------------------------------------------------
*
*
*/

/********************************************************************/
/**
* \file
* \brief CPU driver for Arm Cortex-M0+ from STM32 with MCAN (e.g. STM32G0B1)
*
* This module contains the CPU specific routines for initialization
* and timer handling,
* as well as the GPIO initialization for the CAN controller
*
* Note: Adapt this initialization according to your requirements!
*
*
*/

/* header of standard C - libraries
---------------------------------------------------------------------------*/

/* header of project specific types
---------------------------------------------------------------------------*/
#include <gen_define.h>

#include <co_datatype.h>
#include <co_timer.h>
#include <co_drv.h>

/* hardware header
---------------------------------------------------------------------------*/
#include <stm32g0xx.h>
#include <stm32g0xx_hal.h>
#include <codrv_mcan.h>

/* constant definitions
---------------------------------------------------------------------------*/
/* OS related default definition */
#ifdef CO_OS_SIGNAL_TIMER
#else /* CO_OS_SIGNAL_TIMER */
#  define CO_OS_SIGNAL_TIMER
#endif /* CO_OS_SIGNAL_TIMER */

/* local defined data types
---------------------------------------------------------------------------*/

/* list of external used functions, if not in headers
---------------------------------------------------------------------------*/

/* list of global defined functions
---------------------------------------------------------------------------*/
void codrvTimerISR( void);
void NVIC_Configuration(void);

/* list of local defined functions
---------------------------------------------------------------------------*/


/* external variables
---------------------------------------------------------------------------*/

/* global variables
---------------------------------------------------------------------------*/

/* local defined variables
---------------------------------------------------------------------------*/


/***************************************************************************/
/**
* \brief codrvInitHardware - general customer hardware initialization
*
*/
void codrvHardwareInit(void	/* no parameter */)
{
	/* initialize NVIC controller */
	NVIC_Configuration();
}


/***************************************************************************/
/**
* \brief codrvCanEnableInterrupt - enable CAN interrupts
*
*/
void codrvCanEnableInterrupt(
		void	/* no parameter */
	)
{

	/* enable CAN interrupts */
	HAL_NVIC_EnableIRQ(TIM16_FDCAN_IT0_IRQn);

}


/***************************************************************************/
/**
* \brief codrvCanDisableInterrupt - disable CAN interrupts
*
*/
void codrvCanDisableInterrupt(
		void	/* no parameter */
	)
{

	/* disable CAN interrupts */
	HAL_NVIC_DisableIRQ(TIM16_FDCAN_IT0_IRQn);
}


/***************************************************************************/
/**
* \brief codrvSetCanTxInterrupt - set pending bit of the Transmit interrupt
*
* This function set the interrupt pending bit. In case of the NVIC
* enable interrupt and the CAN specific enable TX Interrupt mask
* the CAN interrupt handler is calling.
*
*/
void codrvCanSetTxInterrupt(
		void	/* no parameter */
	)
{

	/* set pending bit of the CAN transmit interrupt */
	HAL_NVIC_SetPendingIRQ(TIM16_FDCAN_IT0_IRQn);
}


/***************************************************************************/
/**
* \brief TIM16_FDCAN_IT0_IRQHandler - FDCAN1 and FDCAN2 interrupt line 0
*
*/
void TIM16_FDCAN_IT0_IRQHandler(
		void	/* no parameter */
	)
{
	codrvCanTransmitInterrupt();
	codrvCanReceiveInterrupt();
	codrvCanErrorInterrupt();
}


/***************************************************************************/
/**
* \brief codrvTimerISR - Timer interrupt service routine
*
* is normally called from timer interrupt or from an other system timer
*
* \param
*	none
* \results
*	none
*/
void codrvTimerISR(
		void
	)
{
	/* inform stack about new timer event */
	coTimerTick();

	/* signal in case of use of an OS */
	CO_OS_SIGNAL_TIMER
}


#ifdef CODRV_USE_OS
#else /* CODRV_USE_OS */
/***************************************************************************/
/**
* \brief codrvTimerSetup - init Timer
*
* Start a cyclic hardware timer to provide timing interval.
* Alternatively it can be derived from an other system timer
* with the interval given from the DeviceDesigner.
*
*/
RET_T codrvTimerSetup(
		uint32_t timerInterval
	)
{
uint32_t clockFreqHz;

	/* Configure the Systick */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* get the HCLK frequency */
	clockFreqHz = HAL_RCC_GetHCLKFreq();

	/* Configure the Systick interrupt time */
	if (clockFreqHz >= (1000ul*1000ul))  {
		if (HAL_SYSTICK_Config(
			((clockFreqHz/(1000ul*1000ul)))*timerInterval) == 0u)  {
			return (RET_OK);
		}
	}

	/* HCLK frequency under 1Mhz not supported */
	return (RET_HARDWARE_ERROR);
}


/***************************************************************************/
/**
* HAL_SYSTICK_Callback - SYSTICK interrupt callback
*
*/
void HAL_SYSTICK_Callback(
		void	/* no parameter */
	)
{
	/* call timer interrupt service routine */
	codrvTimerISR();
}
#endif /* CODRV_USE_OS */


/***************************************************************************/
/**
* NVIC_Configuration - initialize NVIC controller
*
* Possible, that also the interrupt table is moving in the RAM
* (required for Boot Loader)
*
*/
void NVIC_Configuration(void/* no parameter */)
{
#ifdef CODRV_USE_OS
#else /* CODRV_USE_OS */
const uint32_t canPriority = 1u;
const uint32_t canSubPriority = 1u;

	/* CAN-FD interrupts
	 *-------------------------------------------------------*/
	HAL_NVIC_SetPriority(TIM16_FDCAN_IT0_IRQn, canPriority, canSubPriority);
#endif /* CODRV_USE_OS */

	/* enable CAN-FD interrupts */
	codrvCanEnableInterrupt();

#if defined(BOOT)
	/*
	 * configure the Vector table
	 * At this point very late. Better place is system_stm32fxxx.c.
	 * Correct VECT_TAB_OFFSET.
	 *
	 * NOTE: There are some reserved bits. Means, not all addresses are allowed!
	 */

#if   defined ( __CC_ARM )
/*------------------ RealView Compiler -----------------*/
	extern uint32_t __Vectors;
	SCB->VTOR = (uint32_t)&__Vectors;
#endif /* defined ( __CC_ARM ) */

#if defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
/*------------------ ARM Compiler V6 -------------------*/
#warning not implemented yet!
#endif /* defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050) */

#if defined ( __GNUC__ )
/*------------------ GNU Compiler ----------------------*/
	extern uint32_t g_pfnVectors;
	SCB->VTOR = (uint32_t)&g_pfnVectors;
#endif /* defined ( __GNUC__ ) */

#if defined ( __ICCARM__ )
/*------------------ ICC Compiler ----------------------*/
#warning not implemented yet!
#endif /* defined ( __ICCARM__ ) */

#if defined ( __TMS470__ )
/*------------------ TI CCS Compiler -------------------*/
#warning not implemented yet!
#endif /* defined ( __TMS470__ ) */

#if defined ( __TASKING__ )
/*------------------ TASKING Compiler ------------------*/
#warning not implemented yet!
#endif /* defined ( __TASKING__ ) */

#if defined ( __CSMC__ )
/*------------------ COSMIC Compiler -------------------*/
#warning not implemented yet!
#endif /* defined ( __CSMC__ ) */
#endif /* defined(BOOT) */
}
